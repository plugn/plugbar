(function($){ 

    var auth = {
      cookieManager: Components.classes["@mozilla.org/cookiemanager;1"].getService(Components.interfaces.nsICookieManager2),
      tmpLogin:     '',
      currentLogin: '',
      currentRSID:  '-1',
      login: function(cfgIn, callback){
        try {
          if ('function' != typeof callback) 
            callback = function(){};
            
          $.util.auth.tmpLogin = cfgIn.login? cfgIn.login : '';
          
          var cfgR = {
            login    : (cfgIn.login || '') + '@' + (cfgIn.domain || 'rambler.ru'),
            password : (cfgIn.password || ''),
            remember : (cfgIn.remember || 0), 
            version  : 4
          };

          var xhrArgs = {
            dataType : 'xml', 
            type: 'POST',
            url:  'http://informers.rambler.ru/login/',
            headers: $.util.getHttpHeaders('xml'),
            data: cfgR, 
            error: (function(xhr, aCase, error){
              // $.log('login ajax error. xhr.status:'+xhr.status+'; aCase: ' + aCase);
              if (401 == xhr.status){
                $.loginMsg = $.util.getMsg('accessDenied');
                ('function' == typeof callback) && callback(xhr);
              }
            }),
            success  : (function(docR, aCase, xhr){
              // $.log('login ajax success. xhr.status: ' + xhr.status +'; aCase: '+aCase+ '; docR: ' + docR);
              try {
                // EventCaster.publish('checkAuth');
                ('function' == typeof callback) && callback(xhr);
              } catch (e) {
                $.log('auth.login xhrGet :: load(), exception : ' + e.message);
              }
            })
          };
          
          Zepto.ajax( xhrArgs );
          
        } catch(e){
          $.log('auth.login() exception: ' + e.message);
        }
      },
      logout: function(callback) {
        // $.log('auth.logout()');
        try {
          $.util.cookie.remove('rsid',   '.rambler.ru');
          // EventCaster.publish('checkAuth');
          ('function' == typeof callback) && callback();
          
          var xhrArgs = {
            type     : 'POST', 
            url      : 'http://informers.rambler.ru/logout/',
            data     : { version  : 4 },
            headers  : $.util.getHttpHeaders('xml'),
            dataType : 'xml', 
            error    : (function(xhr, aCase, error){}),
            success  : (function(docR, aCase, xhr) {})
          }    
          Zepto.ajax( xhrArgs );
        } catch(e) {
          $.log('auth.logout() exception: ' + e.message);
        }
      },
      // host should be 'rambler.ru' without leading dot, if not
      pick: function(host) {
        // $.log('auth.pick()');
        try {
          var cookies = {}, nowMs = Date.now();
          host = host || 'rambler.ru';
          // var cookEnum = Components.classes["@mozilla.org/cookieService;1"].getService(Components.interfaces.nsICookieManager).enumerator;
          var cookEnum = $.util.auth.cookieManager.getCookiesFromHost(host);
          while (cookEnum.hasMoreElements()) {
            var cookie = cookEnum.getNext();
            if (cookie && cookie instanceof Components.interfaces.nsICookie && 
              ('.'+host)==cookie.host && (0===cookie.expires || nowMs < cookie.expires * 1000) ) {
              cookies[ cookie.name ] = cookie.value;
            }
          }
          return cookies;
        } catch (e) {
          $.log('pick exception: '  + e.message);
        }
      },
      check : function() {
        // $.log('auth.check()');
        try {
          var cookies = $.util.auth.pick('rambler.ru');
          
          if ('undefined' == typeof cookies.rsid)  
            cookies.rsid = '';
          if ($.util.auth.currentRSID == cookies.rsid) 
            return;
          
          var btnLabel = $.util.getMsg('enter');
          var logged = false;
          if (cookies.rsid && cookies.rlogin) {
            $.util.auth.currentRSID = cookies.rsid;
            var rlogin = (-1==cookies.rlogin.indexOf('@')? cookies.rlogin : (cookies.rlogin.split('@'))[0]);
            $.util.auth.currentLogin = btnLabel = rlogin;
            logged = true;
          } else {
            $.util.auth.currentLogin = '';
            $.util.auth.currentRSID = '';
          }
          Zepto('#ramblerBar-btnLogin').attr('label', btnLabel)
            .attr('tooltiptext', logged ? $.util.getMsg('you') : $.util.getMsg('auth') );
              
          EventCaster.publish(logged? 'onLogin' : 'onLogout');
        } catch (e) {
          $.log('auth::check() exception: '  + e.message);
        }
      }
    };
    
    $.util.auth = auth;
    
})( this['rambler_plugbar'] );
