Components.utils.import('resource://rambler_plugbar-mod/Preferences.js');

var $wo =  window.opener,
    $ = $wo['rambler_plugbar'],
    Zepto = $wo.Zepto,
    EventCaster = $wo.EventCaster;
    
var prefs = {
    promptSvc : Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService),
    cookies: {},
    liveWgtHash : {},
    docPresets : null,
    defBDay: '0000-00-00',
    _inputs: {'0': {prefname:'options.smth.kindof', prefindex : 0}},
    init: function() {
      $.util.camp.notify('usage', 'a3c48374-354c-43ae-8401-b869f684ad41'); // Preferences widget guid
      if (!$.config.prefPane) 
        return;
      var pane = document.getElementById($.APPNAME+'-'+$.config.prefPane);
      if (!pane) return;
      if (document.documentElement.currentPane != pane)
        document.documentElement.showPane(pane);
    },
    quit: function() {
      // $.log('quit');    
      if (prefs.syncBDay)
        $.sync.storeBirthday(prefs.syncBDay);
      $.config.prefPane = null;
    },
    initWidgets: function(){
      try {
        // $.log(' prefs.initWidgets()');
        prefs.prefMgr = new Preferences($.config.prefBranchName); //'extensions.brand_toolbar.'
        prefs.docPresets = $.sync.getXmlDoc('chrome://'+$.APPNAME+'/content/tpl/presets.xml');
        prefs.fillWidgetList();
        
      } catch(e) {
        $.log('prefs.initWidgets() exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    },
    initSettings: function(){
      try {
        // $.log(' prefs.initSettings()');
        prefs.syncBDay = '';
        prefs.prefMgr = new Preferences($.config.prefBranchName);
        var a = document.getElementById('account'),
            e = document.getElementById('denial'),
            l = document.getElementById('login'),
            d = document.getElementById('domain'),
            cookies = prefs.cookies = $.util.auth.pick();
        e.hidden = true;    
        var mode = cookies.rsid? 'login' : 'logout';
        if (cookies.rdomain) 
          d.value = cookies.rdomain;
        if (cookies.rlogin) {
          var s = cookies.rlogin.split('@');
          a.value = (s.length > 1)? cookies.rlogin : (s[0] + (cookies.rdomain? '@'+cookies.rdomain : ''));
        }
        prefs.switchLoginArea(mode);
        prefs.BDay.init();
        
        var loc = document.getElementById('location'); 
        if (loc) { loc.value = prefs.prefMgr.get('options.city.list', ''); }
        
        prefs.txtLogin = document.getElementById('login');
        prefs.txtPassword = document.getElementById('password');
        prefs.btnLogin = document.getElementById('btnlogin'); 
        
      } catch (e) {
        $.log('prefs.initSettings() exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    },
    
    switchLoginArea: function(toCase) {
      try {
        if (-1 == ['login','logout'].indexOf(toCase)) return;
        var rows = $wo.Zepto('row[case]', self.document);
        for (var i=0; i<rows.length; i++) {
            var item = rows[i];
            item.hidden = (toCase==item.getAttribute('case')); 
        };
      } catch(e) {
        $.log('prefs.switchLoginArea() exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    },
    
    onLogin: function(xhr) {
      try {
        // $.log('prefs.onLogin() xhr.status : ' + xhr.status);
        var a = document.getElementById('account'),
            e = document.getElementById('denial'),
            cookies = prefs.cookies = $.util.auth.pick(),
            rlogin = cookies.rlogin;
        if (xhr.status == 200) {
          e.hidden = true;
          var s = rlogin.split('@');
          a.value = (s.length > 1)? rlogin : (s[0] + (cookies.rdomain? '@'+cookies.rdomain : ''));
          prefs.switchLoginArea('login');
        } else if (xhr.status == 401) {
          e.hidden = false;
        }
        
        prefs.BDay.setup();

      } catch (e) {
        $.log('prefs.onLogin() exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    },
    onLogout: function() {
      try {
        // $.log('prefs.onLogout()');
        document.getElementById('account').value='';
        document.getElementById('password').value='';
        document.getElementById('denial').hidden = true;
        prefs.switchLoginArea('logout');
        prefs.cookies = $.util.auth.pick();
        
        prefs.BDay.setup();
        
      } catch (e) {
        $.log('prefs.onLogin() exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    },
    doLogin: function(){
      try {
        // $.log('prefs.doLogin()');
        var l = document.getElementById('login'),
            d = document.getElementById('domain'),
            p = document.getElementById('password'),
            m = document.getElementById('remember'),
            r = {
                login: (l && l.value || ''),
                domain: (d && d.value || ''),
                password: (p && p.value || ''),
                remember : (m && m.checked? 1 : 0)
            };
        var notEmptyData = Object.keys(r).every(function(el,idx,arr){ return '' !== r[el] });
        if (notEmptyData)
            $.util.auth.login(r, prefs.onLogin);
      
      } catch (e) {
        $.log('prefs.doLogin() exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    },
    doLogout: function() {
      try {
        // $.log('prefs.doLogout()');
        $.util.auth.logout(prefs.onLogout);
      } catch(e) {
        $.log('prefs.doLogout() exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    },
    onKeyUp: function(event) {
      if (prefs.txtLogin.value && prefs.txtPassword.value)
        prefs.btnLogin.disabled = false;
      else
        prefs.btnLogin.disabled = true;
    },
    onKeyDown: function(event){
      if (event.keyCode == event.DOM_VK_RETURN || event.keyCode == event.DOM_VK_ENTER){
        event.stopPropagation();
        event.preventDefault();
        prefs.doLogin();
      }
    },
    onSuggestComplete: function(subject) {
      try {
        var geoDump = {};
        for (var i=0; i<subject.controller.matchCount; i++) {
          var fLabel = subject.controller.getValueAt(i);
          var fGeoID = subject.controller.getCommentAt(i);
          if (fGeoID && fLabel)
            geoDump[ fLabel ] = fGeoID;
        }
        if (Object.keys(geoDump).length) 
          subject.setUserData('geodata', geoDump, null);
        else  
          subject.setUserData('geodata', null, null);
        // $.log( 'subject.getUserData(geodata) : {' + subject.getUserData('geodata') + '}');
      } catch (e) {
        $.log(' * onSuggestComplete() exception: '  + e.message + '@ ' + e.lineNumber + '; stack: ' + e.stack);
      }
    },
    onSuggestEntered: function(subject) {
      try {
        var geoDump = subject.getUserData('geodata'),
            geoid = geoDump && geoDump[subject.value];
        if (geoid)
          $.util.cookie.setValue('geoid', geoid, {expires: 800000000});
      } catch (e) {
        $.log(' * onSuggestEntered() exception: '  + e.message);
      }
    },
    
    BDay: {
      elmY: null,
      elmM: null,
      elmD: null,
      monthLength: {
        0:  31, // jan
        1:  28, // feb
        2:  31, // mar
        3:  30, // apr
        4:  31, // may
        5:  30, // jun
        6:  31, // jul
        7:  31, // aug
        8:  30, // sep
        9:  31, // oct
        10: 30, // nov
        11: 31  // dec
      },
      isLeapYear: function (year) {
        year = parseInt(year, 10);
        if (isNaN(year))
          return undefined;
          
        var leap;
        if (year % 400 == 0)
          leap = true;
        else if (year % 100 == 0)
          leap = false;
        else if (year % 4 == 0)
          leap = true;
        else
          leap = false;
        return leap;
      },      
      init: function(){
        try {
          // $.log('BDay.init()');
          var y = prefs.BDay.elmY = document.getElementById('birth-year'),
              m = prefs.BDay.elmM = document.getElementById('birth-month'),
              d = prefs.BDay.elmD = document.getElementById('birth-date');
          var yPs = Zepto('menupopup', y),
              dPs = Zepto('menupopup', d);
          if (!yPs.length || !dPs.length) 
            return $.log('BDay.init() halt: no such element: menupopup');
            
          var now = new Date(Date.now());
          for (var day=1; day<=31; day++){
            var dmi = document.createElement('menuitem');
            dmi.setAttribute('label', day);
            dmi.setAttribute('value', day);
            dPs[0].appendChild(dmi);
          }
          for (var year = now.getFullYear(); year > 1900; year--){
            var ymi = document.createElement('menuitem');
            ymi.setAttribute('label', year);
            ymi.setAttribute('value', year);
            yPs[0].appendChild(ymi);
          }
          
          prefs.BDay.setup();
          
        } catch(e) {
          $.log('BDay.init() exception: ' + e.message + '@' + e.lineNumber );
        }
      },
    
      setup: function(){
        try {
          // $.log('BDay.setup()');
          if (prefs.cookies && prefs.cookies.rsid) {
            $.sync.retrieveBirthday(prefs.BDay.display);
          } else {
            var prefVal = prefs.prefMgr.get('options.birthday', prefs.defBDay);
            if (prefs.defBDay == prefVal) {
              // $.log('(prefs.defBDay==prefVal): '+(prefs.defBDay==prefVal));
              prefs.BDay.display();
              return;
            }
            var parts = prefVal.split('-');
            if (3 != parts.length)
              return $.log('bday prefs broken'); 
              
            var aDate = prefs.BDay.display([parts[0], (parts[1]-1), parts[2]]);
          }
        } catch(e) {
          $.log('BDay.setup() exception: ' + e.message + '@' + e.lineNumber );
        }
      },
  
      // @subj 'date|month|year'
      check: function(subj) {
        try {
          // $.log('BDay.check() ');
          if (!(prefs.BDay.elmY && prefs.BDay.elmM && prefs.BDay.elmD))
            return $.log(' (!) BDay.check(): no data elms defined ');
          if (0 > 'year-month-date'.split('-').indexOf(subj))
            return $.log(' (!) BDay.check(): no subj specified ');
            
          var aDate = [prefs.BDay.elmY.value, prefs.BDay.elmM.value, prefs.BDay.elmD.value],
              isEmpty = aDate.every(function(item) {
                return 'none'==item;
              });
          if (isEmpty)
            return prefs.BDay.save(prefs.defBDay);
            
          aDate = prefs.BDay.display(aDate);
          var hasNaN = aDate.some( function(item) {
            return isNaN(parseInt(item, 10)); 
          });
          // $.log('BDay.check() '+aDate+'; subj: '+subj+'; hasNaN: ' + hasNaN+'; isEmpty: '+isEmpty);
          if (!hasNaN)
            prefs.BDay.save(aDate);
        
        } catch(e) {
          $.log('BDay.check() exception: ' + e.message + '@' + e.lineNumber );
        }      
      },
      
      correct: function(aDate) {
        try {
          // $.log('prefs.BDay.correct()');
          var curMonth = aDate[1];
          if (!aDate || 3 != aDate.length || 'none'==curMonth) {
            return aDate;
          }  
          var monthLastDay = prefs.BDay.monthLength[ curMonth ] + ( (1==curMonth && prefs.BDay.isLeapYear(aDate[0]))? 1 : 0 );
          if (monthLastDay < aDate[2]){
            prefs.BDay.elmD.value = 'none';
            aDate[2] = 'none';
          }
          
          for (var i=31; i>28; i--) {
            var mni = $.util.query('menuitem[value="'+i+'"]', prefs.BDay.elmD);
            if (mni)
              mni.collapsed = i > monthLastDay;
          }
          
          return aDate;
          
        } catch (e) {
          $.log('prefs.BDay.correct() exception: '+e.message+' @ '+e.lineNumber);
        }
      },          
    
      display: function(aDate) {
        try {
          var vDate = (!aDate || 3!=aDate.length)? ['none','none','none'] : prefs.BDay.correct(aDate);
          if (!(prefs.BDay.elmY && prefs.BDay.elmM && prefs.BDay.elmD)){
            $.log(' (!) BDay.display(): no data elms defined ');
            return vDate;
          }
          
          var rDate = vDate.map(function(item){
            var num = parseInt(item, 10);
            return isNaN(num)? 'none' : num;
          });
          // $.log('display() incoming aDate:{'+aDate+'}; vDate:{'+vDate+'}; rDate:'+rDate);
          prefs.BDay.elmY.value = rDate[0];
          prefs.BDay.elmM.value = rDate[1];
          prefs.BDay.elmD.value = rDate[2];
          
          return rDate;
          
        } catch(e) {
          $.log('BDay.display() exception: ' + e.message + '@' + e.lineNumber );
        }
      }, 
      
      // aDate : [yyyy,mm,dd] || '0000-00-00'
      save: function(aDate) {
        try {
          // $.log('BDay.save()');
          if (!Array.isArray(aDate)) {
            if (prefs.defBDay===aDate && !prefs.cookies.rsid) {
              prefs.prefMgr.set('options.birthday', prefs.defBDay);
              return $.scheme.updateWidget('53d7265c-9c1e-4ec1-99d8-b9cbc46b2140'); // update horo
            } else { 
              return; 
            }
          }    
          
          var vDate = aDate.map(function(item, index, array) {
            item = parseInt(item, 10);
            item = String((1==index)? 1+item : item);
            return (item.length < 2? '0'+item : item);
          });
          
          var bday = vDate.join('-');
          if (prefs.cookies.rsid) {
            prefs.syncBDay = bday;
          } else {
            prefs.prefMgr.set('options.birthday', bday);
            $.scheme.updateWidget('53d7265c-9c1e-4ec1-99d8-b9cbc46b2140'); // update horo            
          }  
            
        } catch(e) {
          $.log('BDay.save() exception: ' + e.message + '@' + e.lineNumber );
        }
      }
    },
    goHref: function(event) {
      try {
        $wo.openUILink(event.target.getAttribute('href'));
        event.stopPropagation();
        event.preventDefault();
      } catch(e) {
        $.log('goHref() exception: ' + e.message + ' @ ' + e.lineNumber);
      }  
    },
    _wgtListOnChangeFn : function(event) {
      try {
        var target = event.target, 
            appear = target.getAttribute("isEnabled"),
            guid = target.getAttribute("guid") || '';
        if (!guid) return;
        // $.loggo(target, '_wgtListOnChangeFn() event.type: '+event.type+'; event.target');            
        var wgts = $wo.Zepto('[guid="'+guid+'"]', $.appNode); 
        for (var i=0; i<wgts.length; i++) {
            var item = wgts[i];
            item.setAttribute('visible', appear); 
            item.hidden = ('true' != appear); 
        };
        var wgtx = $wo.Zepto('[guid="'+guid+'"]', $.scheme.cfgDoc); 
        for (var i=0; i<wgtx.length; i++) {
            var itmx = wgtx[i];
            itmx.setAttribute('visible', appear); 
        };
        
        $.scheme.setup($.scheme.cfgDoc);
        $.scheme.saveConfig();
        
      } catch(e) {
        $.log('_wgtListOnChangeFn() exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    },
    fillWidgetList: function() {
        try {
            // $.log(' fillWidgetList()');
            var wgtList = document.getElementById('widgetList');
            if (!wgtList || !$.scheme.widgets) return;
            
            if (prefs._wgtListOnChangeHandle) {
                wgtList.removeEventListener("change", prefs._wgtListOnChangeFn, false);
                prefs._wgtListOnChangeHandle = null;
            }
            
            while (wgtList.firstChild) 
              wgtList.removeChild(wgtList.firstChild);
            var wgts = $.scheme.widgets;
            prefs.liveWgtHash = {};
            for (var i=0; i<wgts.length; i++){
                var wgt = wgts[i];
                if ('true'==wgt.deleted){
                  // $.log(wgt.guid + ' deleted: '+ wgt.deleted);
                  continue; 
                }
                var item = document.createElement('richlistitem');
                item.setAttribute('label', (wgt.nfoBody.title || wgt.nfoBody.text || wgt.guid || ''));
                item.setAttribute('guid',  (wgt.guid || ''));
                item.setAttribute('image', (wgt.nfoBody.image || 'chrome://'+$.APPNAME+'/skin/empty.png'));
                item.setAttribute('disabled',  ((wgt.visible && -1 < ['always', 'always-right'].indexOf(wgt.visible))?'true':'false') );
                item.setAttribute('isEnabled', ((wgt.visible && -1 < ['always', 'always-right', 'true'].indexOf(wgt.visible))?'true':'false') );
                
                if ('false'==item.getAttribute('disabled')) {
                    if (wgt.guid) { prefs.liveWgtHash[ wgt.guid ] = wgt; }
                    wgtList.appendChild(item);
                }
            }

            $wo.Zepto('.wgt-info', document).each(function(index, item){
              $wo.Zepto(item)[ wgtList.getRowCount()? 'removeClass' : 'addClass' ]('hidden-content');
            }); 

            wgtList.selectedIndex = 0;
            if (!prefs._wgtListOnChangeHandle)
                prefs._wgtListOnChangeHandle = wgtList.addEventListener("change", prefs._wgtListOnChangeFn, false );
        
        } catch(e) {
            $.log('fillWidgetList() exception: ' + e.message + ' ; wgt.guid : ' +  wgt.guid );
        }
    },
    
    onWidgetSelect: function(event){
        try {
            // $.log('onWidgetSelect()');
            let eoTarget = event.explicitOriginalTarget, 
                target = event.target,
                listitem = target.selectedItem,
                guid = (listitem.getAttribute('guid') || '');
            let wgt = prefs.liveWgtHash[ guid ] || null;
            
            if (!wgt) return;
            prefs.currentWgtGuid = guid;
            
            prefs.selectWidget(wgt);
            prefs.drawOptions(wgt);
            
        } catch (e) {
            $.log('onWidgetSelect() exception: ' + e.message + ' @ ' + e.lineNumber);
        }
    },
    selectWidget: function(wgt){
        try {
            // $.log(' selectWidget()');
            let elms = {}; 
            ['title','image','description','author'].forEach(function(item){
                elms[item] = document.getElementById('wTop-'+item);
            });  
            ['title','description'].forEach(function(item){
                if (elms[item]) elms[item].textContent = wgt.nfoBody[item]? wgt.nfoBody[item] : '';
            });
            if (elms.image)
                elms.image.setAttribute('src', (wgt.nfoBody.image? wgt.nfoBody.image :''));
            if (elms.author) {
                elms.author.textContent = wgt.nfoBody.authorName? wgt.nfoBody.authorName : '';
                elms.author.setAttributeNS('http://www.w3.org/1999/xhtml','href', (wgt.nfoBody.authorURL? wgt.nfoBody.authorURL : ''));
            }
          
        } catch (e) {
            $.log('selectWidget() exception: ' + e.message + ' @ ' + e.lineNumber);
        }
    },
    optionsRedraw: function( guid ){
        guid = ('undefined' == typeof guid? prefs.currentWgtGuid : guid);
        let wgt = prefs.liveWgtHash[ guid ] || null;
        if (!wgt) return;
        prefs.drawOptions(wgt);
    },
    drawOptions: function(wgt){
        try {
            // $.log(wgt.options, 'drawOptions() options ');
            prefs._inputs = {};
            var render = null;
            var wrapper = document.getElementById('wgt-pref-wrapper') || null;
            if (wrapper) { wrapper.textContent = ''; } else return;
            if (!(wgt.options && wgt.options.length && prefs.docPresets)) return;
            
            for (var i=0; i<wgt.options.length; i++ ) {
                var option = wgt.options[i],
                    colPre = $wo.Zepto('[name="'+option.preset+'"]', prefs.docPresets),
                    prefValRaw = prefs.prefMgr.get(option.prefname, null),
                    prefVal = prefValRaw? String(prefValRaw) : '';
                prefVal = (prefVal.indexOf(',') > 0)? prefVal.split(',') : [ prefVal.replace(',','') ];
                    
                for (var j=0; j<colPre.length; j++ ) {
                    var tplSet = $.scheme.getTplSet( colPre[j] );
                    var aObj = $.util.unlink(option);
                    aObj.list = [];
                    for (var k=0; k<prefVal.length; k++){
                        var prefIdx = Object.keys(prefs._inputs).length;
                        aObj.list.push({ '_IDX': ':'+prefIdx, '_VALUE': prefVal[k]});
                        prefs._inputs[ String(prefIdx) ] = { prefname: option.prefname, prefindex: (tplSet.templateitem? k : -1) };
                    }
                    
                    render = $.util.jxt.render(tplSet, aObj);
                    if (render) 
                        wrapper.appendChild(render);
                }
            }
            
        } catch(e) {
            $.log('drawOptions() exception: ' + e.message + ' @ ' + e.lineNumber );
        }
    },
    UI: {
      dlgConfirm: function(message, title) {
        try {
          var result = prefs.promptSvc.confirm(null, title, message);
          return result;
        } catch(e) {
          $.log('drawOptions() exception: ' + e.message + ' @ ' + e.lineNumber );
        }
      },
      addWidget: function() {
        try {
          // $.log('UI.addWidget');
          $wo.openUILink($.config.galleryURI);
          self.close();
      
        } catch (e) {
          $.log('UI.addWidget() exception: ' + e.message + ' @ ' + e.lineNumber);
        }
      },
      removeWidget: function() {
        try {
          var guid = prefs.currentWgtGuid,
              w = $.scheme.widgetsByGuid[guid] || null;        
          if (!w || !w.nfoBody) { $.log('UI.removeWidget(): widget ' + guid+ ' not found.'); return; }
          var msg = ($.util.getMsg('widgetRemoveConfirm', 'Please confirm you want to remove this widget:')),
              title = ($.util.getMsg('widgetRemoveConfirmTitle', 'Removing a Widget'));
          var confirmed = prefs.UI.dlgConfirm(msg + ' ' + w.nfoBody.title, title);
          $.log('UI.removeWidget() guid: ' + prefs.currentWgtGuid + ' confirmed : ' + confirmed);
          if (!confirmed) return;
            
          $.scheme.removeWidget(guid);
          prefs.fillWidgetList();          
      
        } catch (e) {
          $.log('UI.removeWidget() exception: ' + e.message + ' @ ' + e.lineNumber);
        }
      },
      editPref: function( elm ) {
        try {
          if ('string' == typeof elm) 
              elm = document.getElementById(elm);
          if (!elm) return;
          // $.loggo(elm, 'UI.prefSet() elm');
          var elmID = elm.getAttribute('id') || '',
              elmP = elmID.split(':'),
              elmIdx = parseInt(elmP[elmP.length-1],10),
              prefCfg = (!isNaN(elmIdx)?prefs._inputs[elmIdx]:null),
              prefValRaw = prefs.prefMgr.get(prefCfg.prefname, ''), 
              prefValue;
          if (-1 != prefCfg.prefindex) {
              var v = String(prefValRaw).split(',');
              v[prefCfg.prefindex] = elm.value;
              prefValue = v.join(',');
          } else 
              prefValue = String(elm.value);
          
          prefs.prefMgr.set(prefCfg.prefname, prefValue);
          $.scheme.updateWidget(prefs.currentWgtGuid);
        
        } catch (e) {
          $.log('UI.prefSet() exception: ' + e.message + ' @ ' + e.lineNumber);
        }
      },    
      removePref: function( elm ) {
        try {
          if ('string' == typeof elm) 
              elm = document.getElementById(elm);
          if (!elm) return;
          var elmID = elm.getAttribute('id') || '',
              elmP = elmID.split(':'),
              elmIdx = parseInt(elmP[elmP.length-1],10),
              prefCfg = (!isNaN(elmIdx)?prefs._inputs[elmIdx]:null),
              prefValRaw = prefs.prefMgr.get(prefCfg.prefname, ''), 
              prefValue;
          if (-1 == prefCfg.prefindex) { return; }
          var v = String(prefValRaw).split(',');
          if (v.length < 2) { return; }
          v.splice(prefCfg.prefindex, 1);
          prefValue = v.join(',');
              
          prefs.prefMgr.set(prefCfg.prefname, prefValue);
          prefs.optionsRedraw();
          $.scheme.updateWidget(prefs.currentWgtGuid);
        
        } catch (e) {
          $.log('UI.prefSet() exception: ' + e.message + ' @ ' + e.lineNumber);
        }
      },
      addPref: function( prefname, value ) {
        try {
          var prefValRaw = prefs.prefMgr.get(prefname, ''), 
              v = String(prefValRaw).split(',');
          v.push(value);
          prefValue = v.join(',');

          prefs.prefMgr.set(prefname, prefValue);
          prefs.optionsRedraw();
          $.scheme.updateWidget(prefs.currentWgtGuid);
          
        } catch (e) {
          $.log('UI.prefSet() exception: ' + e.message + ' @ ' + e.lineNumber);
        }
      },
      setSocialPrefs: function(mode, alias) {
        alias = 'facebook'; // only FB supported now
        prefs.prefMgr.set('options.' + alias + '.alerts', !!mode);
      }
    }
}
