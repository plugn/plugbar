/**
 * @copyright 2010-2012, Rambler Internet Holding 
 * @author Max L Dolgov, bananafishbone at gmail dot com
**/ 

Components.utils.import("resource://gre/modules/AddonManager.jsm");
Components.utils.import('resource://rambler_plugbar-mod/Preferences.js');
Components.utils.import('resource://rambler_plugbar-mod/Watcher.js');
Components.utils.import('resource://rambler_plugbar-mod/io.js');
Components.utils.import('resource://rambler_plugbar-mod/md5.js');
Components.utils.import('resource://rambler_plugbar-mod/EventCaster.js');
Components.utils.import('resource://rambler_plugbar-mod/Queue.js');
Components.utils.import('resource://rambler_plugbar-mod/Database.js');
Components.utils.import('resource://rambler_plugbar-mod/jsuri.min.js');

(function($){
    var app = {
        config: {
            RamblerGUID: '27d8774f-75e6-43fa-8e52-6d00bc10c917',
            prefBranchName: 'extensions.'+$.APPNAME+'.',
            appNodeID: $.APPNAME+'-Toolbar',
            cfgLocalPath: 'ProfD/'+$.APPNAME+'.cfg.xml',
            cfgDistribURI: 'chrome://'+$.APPNAME+'/content/tpl/scheme.xml',
            galleryURI: 'http://assist.rambler.ru/gallery/',
            galleryRE : /^https?:\/\/[\w\.]+rambler\.ru\/gallery\//gi,
            schemeUpdateURI: 'http://assist.rambler.ru/scheme/update/',
            schemeVersion: 1,
            updateDelay: 1000*60, // 60 sec
            appData : {
              'APPNAME':  $.APPNAME,
              'APPGUID':  $.APPGUID,
              'ADDONID':  $.ADDONID,
              'APPTITLE': $.APPTITLE,
              'APPVERSION': '4.4' // will be overwritten from init()
            },
            prefPane: null,
            xchWProp:  $.APPNAME+'_nfoDomF'
        },
        getCfg: function(key, defVal){ return ('undefined' == typeof $.config[key]? defVal : $.config[key]); },
        logSvc: Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService),
        promptSvc : Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService),
        xmlSerializer: new XMLSerializer(),
        log: function(){
          try {
            var args = Array.prototype.slice.call(arguments);
            if (!args.length) 
              return;
            $.logSvc.logStringMessage((1 < args.length)? $.util.dumpObject.apply($, args) : args[0]);
          } catch (e) {
            $.logSvc.logStringMessage(' (!) $.log() exception: ' + e.message + ' @' + e.lineNumber);
          }
        },
        loggo: function(obj, cmt, echotypes) {
          return $.log.apply($, Array.prototype.slice.call(arguments));
        },
        quit: function(){
          try {
            // $.log('$.quit()');
            $.sync.stopTimer();
            $.unsetEvents();
          } catch (e) {
            $.log('$.quit() exception: ' + e.message);
          } 
        },
      
        init: function(){
          window.removeEventListener("load", $.init, false);
          $.msgSet = document.getElementById($.APPNAME+'-stringbundle') || null;
          $.appNode = document.getElementById($.config.appNodeID);
          $.prefMgr = new Preferences($.config.prefBranchName); //'extensions.brand_toolbar.'
          if ('r02' != $.PARTNER.code)
            $.prefMgr.set('partner.code', $.PARTNER.code);
            
          $.blockDisability();
          if (!$.util.camp.checkLicense())
            $.appDecline();
            
          $.setup();
          $.setupEvents();
        },
        
        appDecline: function(){
          try {
            // $.util.camp.disableAddonByID($.ADDONID, $.util.camp.restartApp);
            $.appNode.collapsed = true;
          } catch (e) {
            $.log('$.appDecline() exception: ' + e.message);
          }
        },
        blockDisability: function() {
          var addonListener = { 
            onDisabling: function addonListener_onDisabling(addon, needsRestart){
              if (addon.id === $.ADDONID){
              var aud = addon.userDisabled;
                var cbfn = $.util.bind(function(addon){ 
                  addon.userDisabled = false; 
                  this.appNode.collapsed = true;
                }, $, [addon]);
                setTimeout(cbfn, 50);
              }  
            } 
          }
          AddonManager.addAddonListener(addonListener);
        },   
        
        setup: function(){
          try {
            // $.log('$.setup()');
            $.util.jxt.baseData = $.config.appData;
            AddonManager.getAddonByID($.ADDONID, function(addon) {
              $.config.appData.APPVERSION = $.APPVERSION = addon.version;
              $.util.jxt.baseData = $.config.appData;
              $.sync.setRBarCookie();
            });        

            $.scheme.initTemplates();
            $.sync.setupGeo();
            $.sync.bootScheme();
            
          } catch (e) {
            $.log('$.setup() exception: ' + e.message + '@' + e.lineNumber);
          }
        },
        setupEvents: function() {
          // gBrowser.addEventListener("load", $.util.sniff.messenger.onPageLoad, true);
          $.util.sniff.messenger.listenRequest($.util.sniff.messenger.callback);
          $.util.sniff.cookieWatcher.register();
          $.util.sniff.watcher.register();
          // $.util.sniff.geoWatcher.register();
          $.util.sniff.addonWatcher.init();
          $.util.sniff.appQuit.init();
          
          $.evtHandlers = {};
          $.evtHandlers['moveWidget'] = EventCaster.subscribe($.APPNAME+':'+'moveWidget', $.scheme.moveWidget, this);
        },
        unsetEvents: function() {
          // gBrowser.removeEventListener("load", $.util.sniff.messenger.onPageLoad, true);
          $.util.sniff.cookieWatcher.unregister();
          $.util.sniff.watcher.unregister();
          // $.util.sniff.geoWatcher.unregister();
          
          for (var handler in $.evtHandlers)
            EventCaster.unsubscribe( $.evtHandlers[ handler ] );
        },
        
        util: {
            xhr: null,
            utf8_to_b64 : function( str ) {
                return window.btoa(unescape(encodeURIComponent( str )));
            },
            b64_to_utf8: function( str ) {
                return decodeURIComponent(escape(window.atob( str )));
            },
            bind: function(fn, context, params) {
              return function(){
                fn.apply(context, params);
              };
            },            
            dumpObject: function(obj, cmt, echotypes) {
              try {
                var is = [];
                var types = ['String', 'Number', 'Boolean', 'Error', 'Date', 'Function', 'RegExp', 'Object', 'Array', 'NodeList', 
                  'DocumentFragment', 'Document', 'Node', 'Element', 'XULElement', 'Attr', 'CharacterData', 'ProcessingInstruction'];
                for (var t=0; t<types.length; t++)
                  if ( obj instanceof window[ types[t] ] ) is.push( types[t] );
                var val = (-1 != ['Node', 'Document', 'DocumentFragment', 'Element', 'XULElement'].indexOf(is[is.length-1]) )?
                ($.xmlSerializer.serializeToString(obj)) : JSON.stringify(obj);
                return (cmt? cmt+' :\n' : '') + val + (echotypes? '. * types: {'+is.join(',')+'}' : '');
              } catch(e) { return (' (!) util.dumpObject() exception: ' +types[t]+ ' : ' +e.message+ '@' +e.lineNumber); }
            },
            copy: function(o){
              var copy = Object.create( Object.getPrototypeOf(o) );
              var propNames = Object.getOwnPropertyNames(o);
             
              propNames.forEach(function(name){
                var desc = Object.getOwnPropertyDescriptor(o, name);
                Object.defineProperty(copy, name, desc);
              });
             
              return copy;
            },            
            unlink: function(object){
                var unlinked;
                if (typeof object=='object'){
                    if (object.length){
                        unlinked = [];
                        for (var i = 0, l = object.length; i < l; i++) unlinked[i] = arguments.callee(object[i]);
                    } else { 
                        unlinked = {};
                        for (var p in object) unlinked[p] = arguments.callee(object[p]);
                    }
                } else { 
                    return object;
                }    
                return unlinked;
            },
            query: function(selector, elm) {
              if (!elm) { elm = document; }
              return elm.querySelector(selector);              
            },
            queryAll: function(selector, elm) {
              if (!elm) { elm = document; }
              return Array.prototype.slice.call(elm.querySelectorAll(selector));
            },
            getToday: function() {
              var now = new Date(Date.now());
              return ([now.getFullYear(),(1+now.getMonth()),now.getDate()].join('-'));
            },
            getTime: function() {
              var now = new Date(Date.now());
              return ([now.getHours(),now.getMinutes(),now.getSeconds()].join(':'));
            },
            fileInfo: function(f){
              return ' fileSize: '+f.fileSize+'; , lastModifiedTime: '+(new Date(f.lastModifiedTime))+' ('+f.lastModifiedTime+') ';
            },
            getCompStyle: function(element, prop) {
              return window.getComputedStyle(element, null).getPropertyValue(prop); 
            },
            getMsg : function(msg, def) {
              return ($.msgSet? $.msgSet.getString(msg) : (def? def : ''));
            },  
            isGhost: function(){
              return !!($.appNode && $.appNode.collapsed);
            },
            loadURL: function( uri, event ) {
              try {
                uri = $.util.camp.modify( uri );
                openUILink( uri, event );
                
                if (!event) return;
                var selector = '[plugtype=widget][guid]',
                    zt = Zepto(event.target),
                    pwt = zt.parents(selector);
                var guid = zt.is(selector)? zt.attr('guid') : (pwt.length && pwt.eq(0).attr('guid') || '');
                if (guid)
                  $.util.camp.notify('usage', guid);
                
              } catch (e) {
                $.log('$.util.loadURL() exception: ' + e.message + '@' + e.lineNumber);
              }
            },
            getHttpHeaders : function( dataType ) {
              dataType = ('undefined' == typeof dataType) ? '' : dataType;
              var xhrHeaders = {
                'X-Requested-With': 'RamblerBarFF',
                'Accept-Encoding': 'gzip,deflate',
                'Accept': (('xml'==dataType)? 'application/xml, text/xml' : '*'+'/'+'*')
              }
              return xhrHeaders;
            },
            getArgs: function( cfg ) {
              try {
                var xhrArgs = {
                  type     : (cfg.type? cfg.type : null),
                  url      : cfg.uri,
                  headers  : $.util.getHttpHeaders(cfg.format? cfg.format : 'xml'),
                  dataType : (cfg.format? cfg.format : 'any'), 
                  error    : cfg.error || (function( xhr ){
                      $.log(' * util.getArgs():xhrArgs.error() xhr: ' + xhr);
                  }),
                  success  : cfg.success || cfg.load || (function( result ){
                      $.loggo(result, ' * util.getArgs():xhrArgs.load() result' );
                  })
                };
                return xhrArgs;
              } catch (e) {
                  $.log('*** util.getArgs() exception: ' + e.message);
              }
            }
        }
    };
    
    for (var key in app)
        $[key] = app[key];    
    
})( this['rambler_plugbar'] );

window.addEventListener("load",   this['rambler_plugbar'].init, false);
window.addEventListener("unload", this['rambler_plugbar'].quit, false);
