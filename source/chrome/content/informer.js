var $wo =  window.opener,
    $ = $wo['rambler_plugbar'];
    
var informer = {
    _stylesheets : [],
    quit: function() {
      try {
        var ssURI;
        while(ssURI = informer._stylesheets.pop())
          $.sync.removeStylesheet(ssURI);
      } catch (e) {
        $.log('quit() exception: ' + e.message + ' @ '+e.lineNumber);
      }
    },
    onLoad: function(event) {
        try {
            // $.log('informer.onLoad()');
            if (!window.opener) return;
            var iW, 
                xchKey = $.getCfg('xchWProp', null),
                current = $[xchKey]['xchCurrent'];
            if ('object'!= typeof $[xchKey] || !current || !$[xchKey][current])
              return;
            var pocket = $[xchKey][ $[xchKey]['xchCurrent'] ];
            var domF = (pocket.docFrag instanceof window['DocumentFragment']? (pocket.docFrag).cloneNode(true) : null);
            document.title = ('undefined'==typeof pocket.docTitle? '' : pocket.docTitle);
            if (iW = document.getElementById('informerWrap')) {
              var ssURI = (pocket.docCSS || 'chrome://'+$.APPNAME+'/skin/informer.css'),
                  ssR = $.sync.loadStylesheet(ssURI);
              if (ssR)
                informer._stylesheets.push(ssURI);
                  
              iW.innerHTML = '';
              if (domF) { iW.appendChild(domF); }
            }
            event.preventDefault(); 
            event.stopPropagation();
            self.sizeToContent();
        } catch(e) {
            $.log('informer.onLoad() exception: ' + e.message);
        }
    },
    onClick: function(event) {
        try {
            var target = event.originalTarget;
            if (target) {
                for (var node = target; node != null; node = node.parentNode){
                    if(node.nodeType == Node.ELEMENT_NODE) {
                        var className = node.getAttribute('class');
                        var tag = node.nodeName.toLowerCase();
                        
                        if (className && -1 != className.indexOf('btnClose')) {
                            self.close();
                            return;
                        }
                        if ('a' == tag) {
                            var href = node.getAttribute('href');
                            if (href && -1 != href.indexOf('http://')) {
                                $.util.loadURL(href, event);
                                event.preventDefault(); 
                                event.stopPropagation();
                                self.close();
                                return;
                            }
                        } 
                    }
                }
            }
        } catch (e) {
            $.log('informer.onClick() exception: ' + e.message);
        }
    }
};
