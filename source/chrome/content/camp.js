(function($){

    var camp = {
    
      modify: function(uri) {
        try {
          if ('r07' == $.util.camp.getPartnerCode()) {
            var oUri = new jsUri( uri );
            var re = /^(?:[\w\d_-]+\.)?(rambler|r0)\.ru$/gi;
            if (-1 != String(oUri.host()).search(re)) {
              var mozbarQuery = 'utm_source=r07&utm_medium=distribution&utm_content=e01&utm_campaign=a05';
              var f = mozbarQuery.split('&')
                .map(function(item){
                  var o={},
                      [k,v] = item.split('=');
                  o[k]=v;
                  return k? o : null;
                })
                .filter(function(item){
                  return !!item;
                });  
                
              for (var j=0; j<f.length; j++) {
                for (var jkey in f[j]) {
                  oUri.addQueryParam(jkey, f[j][jkey] )
                } 
              }
              uri = oUri.toString();
            }
          }
          return uri;
        } catch (e) {
          $.log('camp.correct() exception: '+e.message+' @ '+e.lineNumber);
        }
      },
      setupData: {
        util: $.util,
        productName: $.APPTITLE,
        License: {
          checked: false,
          url: 'chrome://' + $.APPNAME + '/content/license.xhtml'
        },
        HomePage: {
          checked: true,
          display: true
        }
      },
      checkRbar: function(){
        var appCookies = $.util.auth.pick();
        if ('assist' in appCookies || 'rbar' in appCookies) {
          $.prefMgr.set('license.accepted', true);
          return true;
        }  
        return false;
      },
      checkLicense: function() {
        try {
          if ($.util.camp.checkRbar())
            return true;
            
          var accepted = $.prefMgr.get('license.accepted', null);
          if (false === accepted){
            var choices = $.util.camp.showLicense();
            if (!choices.License || !choices.HomePage) 
              return false;
            
            $.util.camp.notify( choices.License.checked? 'license_accepted' : 'license_declined' );
            
            if (choices.License.checked) {
              $.prefMgr.set('license.accepted', true);
              if (choices.HomePage.checked) {
                Preferences.set("browser.startup.homepage", $.util.camp.getHomePageURI());
                $.util.camp.notify('sethome');
              }
            }  
            return choices.License.checked;
          }
          return true;
        } catch (e) {
          $.log(' $.util.camp.checkLicense() exception:' + e.message + ' @ ' + e.lineNumber);
        }
      },
      showLicense: function() {
        try {
          var windowWatcher = Components.classes["@mozilla.org/embedcomp/window-watcher;1"].getService(Components.interfaces.nsIWindowWatcher);
          
          var args = this.setupData;
          args.wrappedJSObject = args;
          
          var setupWin = windowWatcher.openWindow(null, 'chrome://' + $.APPNAME + '/content/license.xul',
                                                  null, "centerscreen,modal,resizable", args);
          return args;
        } catch (e) {
          $.log(' $.util.camp.showLicense() exception:' + e.message + ' @ ' + e.lineNumber);
        }
        return true;
      },
      disableAddonByID: function(addonID, callback) {
        try {
          AddonManager.getAddonByID(addonID, function(addon) {
            if (addon)
              addon.userDisabled = true;
            callback();
          });
        } catch (e) {
          $.log(' (!) disableAddonByID() exception: '+e.message+' @ '+e.lineNumber);
        }
      },
      restartApp: function () {
        const nsIAppStartup = Components.interfaces.nsIAppStartup;
        // Notify all windows that an application quit has been requested.
        var os = Components.classes["@mozilla.org/observer-service;1"]
                 .getService(Components.interfaces.nsIObserverService);
        var cancelQuit = Components.classes["@mozilla.org/supports-PRBool;1"]
                     .createInstance(Components.interfaces.nsISupportsPRBool);
        os.notifyObservers(cancelQuit, "quit-application-requested", null);
        // Something aborted the quit process. 
        if (cancelQuit.data)
        return;
        // Notify all windows that an application quit has been granted.
        os.notifyObservers(null, "quit-application-granted", null);
        // Enumerate all windows and call shutdown handlers
        var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
                 .getService(Components.interfaces.nsIWindowMediator);
        var windows = wm.getEnumerator(null);
        while (windows.hasMoreElements()) {
        var win = windows.getNext();
        if (("tryToClose" in win) && !win.tryToClose())
          return;
        }
        Components.classes["@mozilla.org/toolkit/app-startup;1"].getService(nsIAppStartup)
            .quit(nsIAppStartup.eRestart | nsIAppStartup.eAttemptQuit);
      },
      getHomePageURI: function() {
        return 'http://rambler.ru/?utm_source='+$.util.camp.getPartnerCode()+'&utm_medium=distribution&utm_content=e08&utm_campaign=a05';
      },
      getPartnerCode: function() {
        return $.prefMgr.get('partner.code', $.PARTNER.code);
      },
      getAppEventURI: function(evt, widgetGuid) {
        // evt : install|usage|sethome|license_accepted|license_declined
        if ('undefined' == typeof evt)
          evt = 'usage';
        if (widgetGuid)
          return 'http://assist.rambler.ru/stat/widget?guid=' + widgetGuid + '&event=' + evt;
        return 'http://www.rambler.ru/r/p?event='+evt+'&rpid='+($.util.camp.getPartnerCode())+'&appid=assist_ff&guid='+($.sync.getSchemeGUID());
      },
      notify: function(evtName, widgetGuid) {
        try {
          var cfg = {
            url      : $.util.camp.getAppEventURI(evtName, widgetGuid),
            nocache  : true, 
            headers  : $.util.getHttpHeaders(),
            error    : function(){},
            success  : function(r,s,xhr){}
          };
          Zepto.ajax( cfg );        
        } catch (e) {
          $.log(' (!) camp.notify() exception: '+e.message+' @ '+e.lineNumber);
        }
      },
      sendUsage: function() {
        try {
          var today = $.util.getToday(),
              evtName = $.util.isGhost()? 'invisible' : 'usage',
              lastUsg = $.prefMgr.get('stat.lastusage', '1970-01-01');
          if ('1970-01-01' == lastUsg && 'usage' == evtName) 
            $.util.camp.notify('install');
          if (lastUsg == today) 
            return; 
          
          var cfg = {
            url      : $.util.camp.getAppEventURI( evtName ),
            nocache  : true, // FF XPCOM hack + Zepto patched = supported
            headers  : $.util.getHttpHeaders(),
            error    : function(){},
            success  : function(r,s,xhr){
              try {
                $.prefMgr.set('stat.lastusage', today);
              } catch(e) {
                $.log('sendUsage() xhr.success() exception: ' + e.message);
              }
            }
          };
          Zepto.ajax( cfg );
        } catch(e) {
          $.log('sendUsage() exception: ' + e.message);
        }
      }
    };
    
    $.util.camp = camp;
    
})( this['rambler_plugbar'] );