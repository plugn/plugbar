(function($){ 

    var cookie = {
        cookieString: function(name, value, props) {
            props = props || {}
            var exp = props.expires
            if (typeof exp == "number" && exp) {
                var d = new Date()
                d.setTime(d.getTime() + exp*1000)
                exp = props.expires = d
            }
            if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }

            value = encodeURIComponent(value)
            var updatedCookie = name + "=" + value
            for(var propName in props){
                updatedCookie += ";" + propName;
                var propValue = props[propName];
                if(propValue !== true){ updatedCookie += "=" + propValue; }
            }
            return updatedCookie;
        },
        remove: function(name, host) {
          try {
            host = String(host || '.rambler.ru');
            if ('.' !== String(host).charAt(0)) 
              host = '.' + host;
            // $.log('cookie.remove() ' + name + '@' + host);
            var cookieManager = Components.classes["@mozilla.org/cookiemanager;1"].getService(Components.interfaces.nsICookieManager2);
            var iter = cookieManager.getCookiesFromHost(host);            
            while (iter.hasMoreElements()) {
              var cookie = iter.getNext();
              if (cookie instanceof Components.interfaces.nsICookie) {
                if (name===cookie.name && host.indexOf(cookie.host.toLowerCase()) != -1) {
                  cookieManager.remove(cookie.host, cookie.name, cookie.path, cookie.blocked);
                }
              }
            }
            cookieManager.remove(cookie.host, cookie.name, cookie.path, false);
          
          } catch (e) {
            $.log('cookie.remove() exception + ' + e.message);
          }
        },          
        setValue: function(name, value, props) {
            try {
                // $.log('util.Cookir.setValue('+name+', '+ value+')');            
                if (!name || ''==name) { return; }
                props = props || {};
                var cookieCfg = {};
                cookieCfg[name] = (value?  value  : "");
                cookieCfg.domain = (props.domain? props.domain : ".rambler.ru");
                cookieCfg.path =   (props.path? props.path : "/");
                
                var exp = props.expires;
                
                if (typeof exp == "number" && exp) {
                    var d = new Date();
                    d.setTime(d.getTime() + exp*1000);
                    exp = props.expires = d;
                }
                if (exp && exp.toUTCString) { props.expires = exp.toUTCString(); }
                if (props.expires) {
                    cookieCfg.expires = props.expires;
                }
                   
                var cookieURI = Components.classes["@mozilla.org/network/standard-url;1"].createInstance(Components.interfaces.nsIURI);
                cookieURI.spec = "http://" + cookieCfg.domain.replace(/^\./, "");
                    
                var cookieStr = '';
                for (var pName in cookieCfg) {
                    var pValue = cookieCfg[pName];
                    if(pValue !== true){ 
                        cookieStr += (pName + "=" + pValue + ';'); 
                    }
                }
                // $.log('util.cookie.setValue() cookieStr: ' + cookieStr);
                Components.classes["@mozilla.org/cookieService;1"]
                    .getService(Components.interfaces.nsICookieService)
                    .setCookieString(cookieURI, null, cookieStr, null); 
            
            } catch (e) {
                $.log('util.cookie.setValue() exception: ' + e.message);
            }
        }

    };
    
    $.util.cookie = cookie;
    
})( this['rambler_plugbar'] );
