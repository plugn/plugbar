(function($){

    var api = {
        safeEscape: function(v) {
          v = v.replace(/^\s+|\s+$/mg,'')
               .replace(/&\w+;/mg,'')
               .replace(/[\u00A0\u2028\u2029\u0000-\u001F]/mg, '')
               .replace(/[\f\n\r\t\v]+/mg,' ')
               .replace(/\\/mg,'');
          return v;
        },
        parser: function( doc, query ) {
            try {
                if (!doc) { return null; }

                query = ('undefined' == typeof query)? 'results > result' : query;
                var nfoData = [], 
                    r = (doc instanceof Document)? Zepto(query, doc) : [ doc ];
                
                for (var k=0; k<r.length; k++){
                    var h = {};
                    h.value = r[k].getAttribute('value') || '';
                    
                    if (r[k].hasChildNodes()) {
                        var cN=r[k].childNodes;
                        for (var i = 0; i < cN.length; i++) {
                            var item = r[k].childNodes[i];
                            if (Node.ELEMENT_NODE !== item.nodeType) continue;
                            if (Node.ELEMENT_NODE == item.nodeType && item.hasAttributes()) {
                                for (var idx=0; idx < item.attributes.length; idx++) {
                                    var iAttr = item.attributes.item(idx);
                                    var aKey = iAttr.nodeName.toLowerCase();
                                    var aValue = iAttr.value.replace(/^\s+|\s+$/g, "");
                                    h[item.nodeName+'.'+aKey] = aValue;
                                }
                            }
                            if (Node.ELEMENT_NODE == item.nodeType && item.nodeName && item.textContent) {
                                var txt = (item.textContent).replace(/^\s+|\s+$/mg,'').replace(/\n+/mg, '\n');
                                var spl = txt.split('\n');
                                for (var j=0; 1<spl.length && j<spl.length; j++) {
                                    h[(item.nodeName)+j] = $.util.api.safeEscape(spl[j]);
                                }
                                // h[item.nodeName] = txt.replace(/^\s+|\s+$/mg,'').replace(/\s+/mg,' ').replace(/\\/mg,'');
                                h[item.nodeName] = txt.replace(/^\s+|\s+$/mg,'').replace(/\n/mg,'(plugmagicbr)').replace(/\s+/mg,' ').replace(/\\/mg,'');
                            }
                            
                            if ('menus' == item.nodeName) {
                                if (item.hasChildNodes() && item.childNodes.length>0) {
                                    var helper = $.util.apiHelper.factory( item.nodeName );
                                    h[item.nodeName] = this.parseTree( item, helper );
                                }
                            }
                        }
                        nfoData.push(h);
                    }
                }
                return (nfoData.length? nfoData : null);  
            } catch (e) {
                $.log('*** util.api.parser() exception : ' + e.message + '; e.lineNumber: ' + e.lineNumber );
            }
        },
        
        parseTree: function(oXML, helper) {
            try {
                // $.log('parseTree() oXML.nodeName: ' + oXML.nodeName);
                switch(oXML.nodeType) {
                case Node.ELEMENT_NODE: // 1
                    var h = new helper(oXML);
                    var elmEdge = h.getEdgeElm();
                    var elmTop  = h.getTopElm();
                    if (oXML.hasChildNodes()) {
                        var iXMLChild, oChild, oChilds = [];
                        for (var iChildId = 0; iChildId < oXML.childNodes.length; iChildId++) {
                            iXMLChild = oXML.childNodes.item(iChildId);
                            oChild = this.parseTree(iXMLChild, helper);
                            if (null !== oChild) 
                                elmEdge.appendChild( oChild );
                        }
                    }
                    return elmTop;
                    break;
                case Node.TEXT_NODE: // 3
                    var result = document.createTextNode(oXML.nodeValue);
                    return (result || null);
                    break;
                case Node.CDATA_SECTION_NODE: // 4
                    var result = document.createCDATASection(oXML.nodeValue);
                    return (result || null);
                    break;
                default: 
                    return null;
                }                    
                        
            } catch(e) {
                $.log('(!) parseTree() exception: ' + e.message );
            }
        },
        
        processFeed: function(txt, uri, wgt){
          try {
            var parser = Components.classes["@mozilla.org/feed-processor;1"]
                                            .createInstance(Components.interfaces.nsIFeedProcessor);
            parser.listener = {
              handleResult: function(result) {
                try {
                  var feed = result.doc; // nsIFeedContainer
                  if (!feed) {
                    var dataDef = wgt.nfoBody? [wgt.nfoBody] : ($.util.api.parser( wgt.informer ));
                    dataDef.isRss = true;
                    $.scheme.createWidget(dataDef, wgt.guid, wgt.informer);
                    return;
                  } 
                  feed.QueryInterface(Components.interfaces.nsIFeed);
                  var list = [];
                  for (var i=0; i<feed.items.length && i<15; i++) {
                    var entry = feed.items.queryElementAt(i, Components.interfaces.nsIFeedEntry);
                    if (entry) {
                      list.push({
                        title: entry.title.plainText(),
                        link: entry.link.resolve('')
                      });
                    }
                  }
                  var dataSet = {
                    text : (feed.title && feed.title.plainText() || ''),
                    title : (feed.title && feed.title.plainText() || ''),
                    tooltip : (feed.subtitle && feed.subtitle.plainText() || ''),
                    link: (feed.link && feed.link.spec || ''),
                    list: list,
                    isRss: true
                  }
                  // $.loggo(dataSet, ' ============ feed result: ============= ');
                  wgt.lastUpdated = Date.now();
                  $.scheme.createWidget(dataSet, wgt.guid);
                } catch(e) {
                  $.log('(!) handleResult() exception: ' + e.message + ' @ ' + e.lineNumber );
                }
                  
              }
            };
            parser.parseFromString(txt, uri);
          } catch(e) {
            $.log('(!) processFeed() exception: ' + e.message + ' @ ' + e.lineNumber );
          }
        },
        
        
        rss: function( doc ) {
            try {    
                if (!doc) { return null; }
                
                var fields = ['title', 'link', 'pubDate'];// 'description',
                var nfoData = [];
                var r = Zepto('rss', doc); 
                var gtext = Zepto('text', r[0]);
                var gt = Zepto('title', r[0]);
                var gtitle = gt.length? gt[0].textContent : ( gtext.length? gtext[0].textContent :'');
                var gd = Zepto('description', r[0]);
                var gdesc = gd.length? gd[0].textContent : gtitle;
                
                var gl = Zepto('link', r[0]);
                var glink = gl.length? gl[0].textContent : '';
                var s = Zepto('channel item', r[0]);

                s.forEach(function(item, idx) {
                    var h = {};
                    fields.forEach((function(field){
                        var w = Zepto(field,item);
                        if (w.length) {
                            var v=w[0].textContent;
                            h[field] = $.util.api.safeEscape(v);
                        }    
                    }) );
                    
                    if (nfoData.length < 25) { 
                        nfoData.push(h); 
                    }
                });                        

                return { text: gtitle, title: gtitle,  tooltip: gdesc, link: glink, list: nfoData, isRss: true };
                
            } catch (e) {
                $.log('*** util.api.rss() exception : ' + e.message + '@'+e.lineNumber);
            }
        }
    };
    
    $.util.api = api;
    
})( this['rambler_plugbar'] );
