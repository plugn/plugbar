var EXPORTED_SYMBOLS = ['unit'];

const FORCELOGIN = false;

var XMLHttpRequest = function () {
  return Components.classes["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance(Components.interfaces.nsIXMLHttpRequest);
}

var FB_APP_ID = 450825244985622, // 219468064849803,
    OAuthRedirectURI = 'https://www.facebook.com/connect/login_success.html',
    OAuthRedirectURITest = /facebook\.com\/connect\/login_success\.html/i,
    OAuthPermissions = [""
    , "manage_notifications"
    , "offline_access"
    , "read_insights"
    , "read_mailbox"
    , "read_requests"
    , "read_stream"
    , "user_activities"
    , "user_status"
    ].join(','),
    OAuthWindowFeatures = [
    "centerscreen",
    "resizable",
    "width=640",
    "height=480"
    ].join(",");
    
var FBdomain = {
  api: 'https://api.facebook.com/',
  graph: 'https://graph.facebook.com/',
};

function unit($){
  
  var api = {
  
    proceedLogin : function facebook__proceedLogin(callbacks) {
      var OAuthWindow = null;
      var oauthLocationListener = $.utils.createLocationListener(
        function (callbacks, aWebprogress, aRequest, aLocation) {
          var spec = aLocation.spec;
          try {
            var url = aLocation.QueryInterface(Components.interfaces.nsIURL);
          } catch (e) {
            return;
          }
          
          if (OAuthRedirectURITest.test(spec)) {
            var param = $.utils.urlParams2obj(url.query);
            if ("error" in param) {
              return callbacks.error(param);
            }
            var ref = $.utils.urlParams2obj(url.ref);
            if ("access_token" in ref) {
              return callbacks.success(ref);
            }
            callbacks.error("PARSE_ERROR");
          }
        }
      );
      
      var authCB = new $.utils.CallbackObject(
        function (reqs) {
          OAuthWindow.close();
          $.FB._setReqs(reqs);
          callbacks.success();
        },
        function (e) {
          OAuthWindow.close();
          callbacks.error(e);
        }
      );
      
      var oauthParams = {
        "client_id": FB_APP_ID, 
        "redirect_uri": OAuthRedirectURI, 
        "type": "user_agent",
        "display" : "popup",
        "scope": OAuthPermissions
      };
      
      var OAuthWindowArguments = {
        url: 'https://graph.facebook.com/oauth/authorize?' + $.utils.obj2UrlParams(oauthParams),
        progressListener: new oauthLocationListener(authCB),
        notifyMask: Components.interfaces.nsIWebProgress.NOTIFY_LOCATION,				
        onclose: function () {
          callbacks.error("CANCELLED");
        }
      };
      
      OAuthWindow = $.ui.openMicrobrowser(OAuthWindowFeatures, OAuthWindowArguments);
    },

    connect: function facebook__connect() {
      try {
        // $.log(' * ' + arguments.callee.name + '()');
        
        var uri = 'https://www.facebook.com/dialog/oauth?client_id=' + FB_APP_ID + 
            '&redirect_uri=' + encodeURIComponent(OAuthRedirectURI) + '&scope=' + OAuthPermissions + '&response_type=token';

        var xhrArgs = {
            url      : uri,
            error    : (function(xhr, aCase, error){
              $.log('FB.connect(), xhr error() '+aCase+'; error: '+ error + '; xhr: ' + xhr);
              $.onLogout();
              return FORCELOGIN && $.login();
            }),
            success  : (function(txr, aCase, xhr) {
              var uri = xhr.channel.URI.spec;  
              var url = $.api.makeURI(uri);
              if (OAuthRedirectURITest.test(url.spec)) {
                var ref = $.utils.urlParams2obj(url.ref);
                if ("access_token" in ref) {
                  return $.FB._setReqs(ref);
                }
              }
              
              return FORCELOGIN && $.login();
            })
        };    
        $.api.Zepto.ajax( xhrArgs ); 

      } catch (e)  {
        $.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    },
    
    _setReqs: function facebook__setReqs(reqs) {
      try {
        // $.log(' * ' + arguments.callee.name + '()');
        $.log(reqs, 'reqs');
        $.reqs = reqs || {};
        if ($.connected()) {
          $.update();
        }  
      } catch (e) {
        $.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    },
    
    hasUserCookie: function() {
      return !!($.utils.getCookie('.facebook.com', 'c_user') && $.utils.getCookie('.facebook.com', 'xs'));
    },
    
    deleteUserCookie: function() {
      $.utils.deleteCookie('.facebook.com', 'c_user');
    },
    
    _createSignedRequest: function (httpMethod, url) {
      var request = new XMLHttpRequest;

      request.open(httpMethod, url, true);
      
      var headers = {
        "Authorization": "OAuth " + $.reqs.access_token, 
        "Accept-Charset": null,
        "Accept-Language": null,
        "Accept": null,
        "Connection": null,
        "Keep-Alive": null,
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
      };
      
      for (var i in headers) 
        request.setRequestHeader(i, headers[i]);
      request.channel.loadFlags |= Components.interfaces.nsIRequest.LOAD_ANONYMOUS;
      
      return request;
    },
    execFQL: function (callbacks, query) {
      var method = 'fql.query',
        queryField = 'query';

      if (typeof query == 'object') {
        method = 'fql.multiquery';
        query = JSON.stringify(query);
        queryField = 'queries';
      }
      // $.log(' * execFQL, query: ' + query);			
      var params = {};
      params[queryField] = query;
      
      var url = FBdomain.api + 'method/' + method + '?format=json';
      var req = this._createSignedRequest('POST', url);
      req.onreadystatechange = function (e) {
        var readyState = req.readyState;
        if (readyState == 4) {
          var responseText = req.responseText;
          try {
            var response = JSON.parse(responseText);
          } catch (e) {
            return callbacks.error('PARSE_ERROR');
          }
          callbacks.success(response);
        }
      }
      
      req.send($.utils.obj2UrlParams(params));
      
      return new $.utils.CallbackWatcher(function () {
        req.abort();
      });
    },
    
    requestGraph: function (callbacks, id, postData) {
      var method = 'GET';
      if (postData) {
        method = 'POST';
      }
      var req = this._createSignedRequest(method, FBdomain.graph + id);
      req.onreadystatechange = function (e) {
        var readyState = req.readyState;
        if (readyState == 4) {
          var responseText = req.responseText;
          try {
            var response = JSON.parse(responseText);
          } catch (e) {
            return callbacks.error('PARSE_ERROR');
          }
          callbacks.success(response);
        }
      }
      if (typeof postData !== 'string') {
        postData = $.utils.obj2UrlParams(postData);
      }
      req.send(postData);
    }
  };
  
  $.FB = api;
  
}; 
