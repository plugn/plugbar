var EXPORTED_SYMBOLS = ['unit'];

const {
    classes: Cc,
    interfaces: Ci,
    results: Cr,
    utils: Cu
} = Components;

Cu.import("resource://gre/modules/FileUtils.jsm");

function unit($){
  var storage = {
  
    _dbFile: null,
    _dbLink: null,
    _fileName: 'rambler_plugbar_fb.sqlite',    
    _tableName: 'alerts',  
    _tableScheme: 'alertid STRING UNIQUE, time INTEGER',
    _READ_DATA_QUERY: 'SELECT * FROM alerts',
    _READ_ITEM_QUERY: 'SELECT * FROM alerts WHERE alertid = :alertid',
    _KEEP_DATA_DAYS: 7, // 
    
    get _file() {
      if (this._dbFile)
        return this._dbFile;
      this._dbFile = FileUtils.getFile('ProfD', [this._fileName]);
      return this._dbFile;
    },
    
    get db() {
      if (this._dbLink)
        return this._dbLink;
      this._dbLink = new $.api.Database($.storage._file); 
      try {
        if (this._dbLink && this._dbLink._connection)
          this._dbLink._connection.createTable(this._tableName, this._tableScheme);
      } catch (e) {
        if (0x80004005 != e.result){
          $.log(' (!) get db(); unexpected exception #'+e.result+': ' + e.message + ' @' + e.lineNumber);   
          this._dbLInk = null; 
          return null;
        } // else { $.log(' get db(); table ' + this._tableName + ' already exists.'); }
      }
      return this._dbLink;
    },  
    start: function fb__storage__start(){
      try {
        this.eraseOldRecords();
      } catch(e) {
        $.log(' (!) storage__start() exception: ' + e.message + ' @' + e.lineNumber);
      }
    },
    
    readData: function fb__storage__readData(){
      try {
        var rows = this.db.execQuery(this._READ_DATA_QUERY);
        // $.log(rows, 'readData() '+this._READ_DATA_QUERY+', rows');
      } catch (e) {
        $.log(' (!) readData() exception: ' + e.message + ' @' + e.lineNumber);
      }
    },
    
    readItem: function fb__storage__readData(alertid){
      try {
        var rows = this.db.execQuery(this._READ_ITEM_QUERY, {alertid: alertid});
        // $.log(rows, 'readItem() '+this._READ_DATA_QUERY+', rows');
        return (rows.length? rows[rows.length-1] : null);
      } catch (e) {
        $.log(' (!) readData() exception: ' + e.message + ' @' + e.lineNumber);
      }
    },
    
    insertData: function fb__storage__insertData(alertid){
      try {
        if ('object' == typeof value)
          value = JSON.stringify(value);
        var timeSec = this._currentTimestampSecs;
        
        return this.db.execQuery('INSERT INTO '+this._tableName+' (alertid, time) VALUES (:alertid, :time)', {
          alertid: alertid,
          time: timeSec
        });
        
      } catch (e) {
        if (0x80630003 == e.result)
          return; // $.log(' (!) insertData() expected fallback: STORAGE_CONSTRAINT');
        $.log(' (!) insertData() exception: ' + e.message + ' @ ' + e.fileName + ':' + e.lineNumber);
      }  
    },  
    
    get _currentTimestampSecs() {
      return parseInt(Date.now() / 1000, 10);
    },
    
    eraseRecord: function _eraseRecord(alertid) {
      return this.db.execQueryAsync('DELETE FROM '+this._tableName+' WHERE (alertid = :alertid)', {alertid: alertid});
    },
    
    eraseOldRecords: function _eraseOldRecords() {
      return this.db.execQuery('DELETE FROM '+this._tableName+' WHERE (time < :time)', {
        time: (this._currentTimestampSecs - 3600 * 24 * this._KEEP_DATA_DAYS)
      });
    }
  }
  
  $.storage = storage;
}
  