var EXPORTED_SYMBOLS = ['unit'];

const DEBUG = false;
const LIMIT = 3;

function unit($){
  var query = {
  
    alertUIDs: [],
    
    fetch: function facebook__query__fetch() {
      try {
        if (!$.connected()) {
          // $.log(arguments.callee.name+'() not connected.');
          return $.display();
        }

        function parseResult (res) {
          try {
            var qResult = {};
            for (var i = 0, l = res.length; i < l; ++i) {
              var response = res[i],
                  name = response.name,
                  resultSet = response.fql_result_set;
              if (resultSet)
                qResult[name] = resultSet;
            }
            // $.log(qResult, ' fetch() : parseResult() : qResult \n');
            
            return qResult;
            
          } catch (e) {
            $.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber);
          }
        };        
        
        var cb = new $.utils.CallbackObject(
          function fb__fetch__success(result) {
            try {
              // $.log(result, 'FQL success cb, result: \n');
              var r = parseResult(result);
              
              if (r.notif && Array.isArray(r.notif)) {
                var me = !$.account && r.me && r.me.length && r.me[0];
                if (me)
                  $.account = $.api.Zepto.extend( {id: me.uid}, me );
                var summary = [].concat(r.notif, r.mail, r.friendship);
                var menuset = {
                  friendsURI: r.friendship.length? 'requests/' : '',
                  numFriendship: r.friendship.length? ' ('+r.friendship.length+')' : '',
                  numMail: r.mail.length? ' ('+r.mail.length+')' : '',
                  numNotif: r.notif.length? ' ('+r.notif.length+')' : ''
                };
                
                $.display( summary, menuset );
                
                if ($.api.util.isGhost())
                  return;
                
                var profiles = {},
                    tmpData = [],
                    popData = [];
                for (var pi=0; pi<r.peer.length; pi++){
                  var p = r.peer[pi];
                  if (p && p.uid)
                    profiles[p.uid] = p;
                }    
                
                for (var j=0; j<r.friendship.length; j++){
                  var fi = r.friendship[j];
                  var item = $.api.Zepto.extend({ 
                      title_text: $.api.util.getMsg('friendshipRequest','Friendship request'),
                      href: 'http://www.facebook.com/friends/requests/',
                      alrID: 'friend#' + fi.uid_from + '@' + fi.time
                    }, 
                    (fi || {}), 
                    (profiles[ fi.uid_from ] || {})
                  );
                  item && tmpData.push(item);
                }
                
                for (var j=0; j<r.mail.length; j++){
                  var mi = r.mail[j];
                  var item = $.api.Zepto.extend({
                      title_text: mi.snippet,
                      href: 'http://www.facebook.com/messages/' + (mi.snippet_author || ''),
                      alrID: 'mail#' + mi.thread_id + ':' + mi.snippet_author + '@' + mi.updated_time
                    }, 
                    (profiles[ r.mail[j].snippet_author ] || {})
                  );
                  item && tmpData.push(item);
                }

                for (var j=0; j<r.notif.length; j++){
                  var ni = r.notif[j];
                  var item = $.api.Zepto.extend(
                    (ni || {}), 
                    (profiles[ ni.sender_id ] || {}),
                    {alrID: 'notif#' + ni.notification_id + ':' + ni.sender_id + '@' + ni.updated_time}
                  );
                  item && tmpData.push(item);
                }
                
                var ftrData = tmpData.filter(function(item, index, array){
                  var ins = $.storage.insertData(item.alrID);
                  return ('undefined' !== typeof ins); 
                });
                
                var popData = ftrData.map(function(rec){
                  var name = rec.name || '', 
                      title = rec.title_text || '',
                      image = rec.pic_square || 'chrome://rambler_plugbar/skin/settings-logo.png',
                      href = rec.href || 'http://www.facebook.com/home.php?sk=nf',
                      listener = {
                        observe: function(subject, topic, data) {
                          if ('alertclickcallback'==topic && data)
                            $.api.util.loadURL(data);
                        }
                      };  
                  return ([name || '{from}', title || '{title}', image, listener, href]);
                });
                
                var finalize = function() {
                  var timer = Components.classes["@mozilla.org/timer;1"].createInstance(Components.interfaces.nsITimer);
                  var cbfn = $.api.util.bind($.api.sync.removeStylesheet, {}, ['chrome://rambler_plugbar/skin/alert.css']);
                  timer.initWithCallback( cbfn, 5000, timer.TYPE_ONE_SHOT );
                };
                
                $.api.sync.loadStylesheet('chrome://rambler_plugbar/skin/alert.css');
                
                var wgt = $.api.getWidget($.wgtGuid);
                if (wgt)
                  wgt.lastUpdated = Date.now();

                var alertsAllowed = $.api.prefMgr.get('options.facebook.alerts', true);
                if (!alertsAllowed) 
                  return;
                
                var popQ = new $.api.Queue(popData, 3500, $.tray.popup, {}, finalize);
                popQ.play();
                  
              }
          
            } catch(e) {
              $.log(' (!) query.getData() callback exception: \n' + e.stack);
            }
          },        
          function(err){
            $.log(' (!) FQL error, error: ' + err);
          }          
        );        
        
        var mQuery = {
          friendship: 'SELECT uid_from, message, time FROM friend_request WHERE unread!=0 AND uid_to = me()',
          
          mail:       'SELECT snippet, snippet_author, subject, thread_id, updated_time FROM thread WHERE folder_id = 0 AND unseen != 0',
          
          notif:  'SELECT sender_id, href, title_text, notification_id, updated_time FROM notification WHERE recipient_id = me() AND is_unread = ' + 
                      (DEBUG? 0 : 1) + ' LIMIT ' + (DEBUG? LIMIT : 25),
                            
          peer:   'SELECT uid, name, pic_square FROM user WHERE ' + 
                      'uid IN (SELECT sender_id FROM #notif) OR ' + 
                      'uid IN (SELECT snippet_author FROM #mail) OR ' +
                      'uid IN (SELECT uid_from FROM #friendship)'
        };
        
        if (!$.account)
          $.api.Zepto.extend( mQuery, {me: 'SELECT uid, name FROM user WHERE uid = me()'} );
          
        $.FB.execFQL(cb, mQuery);
        
      } catch (e) {
        $.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    } 
      
  };
  
  $.query = query;
  
}; 
 