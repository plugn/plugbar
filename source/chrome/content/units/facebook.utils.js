var EXPORTED_SYMBOLS = ['unit'];

function unit(scope){

  var utils = {
    createLocationListener: function (changeListener) {
      var progressListener = function (callbacks) {
        var $ = this, empty = function (){};
      
        $.QueryInterface = function (aIID) {
          if (aIID.equals(Components.interfaces.nsIWebProgressListener) ||
            aIID.equals(Components.interfaces.nsISupportsWeakReference) ||
            aIID.equals(Components.interfaces.nsISupports)) {
              return this;
          } else {
            throw Components.results.NS_NOINTERFACE;
          }
        }	
        
        $.onLocationChange = function (aWebProgress, aRequest, aLocation) {
          return changeListener.call(this, callbacks, aWebProgress, aRequest, aLocation);
        }
        $.onProgressChange = empty;
        $.onSecurityChange = empty;
        $.onStateChange = empty;
        $.onStatusChange = empty;
      };
      return progressListener;
    },
    
    md5: function (str) {
      function toHexString(charCode) {
        return ("0" + charCode.toString(16)).slice(-2);
      }
      var converter = Components.classes["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Components.interfaces.nsIScriptableUnicodeConverter);
      converter.charset = "UTF-8";
      var t = {};
      var data = converter.convertToByteArray(str, t);
      var ch = Components.classes["@mozilla.org/security/hash;1"].createInstance(Components.interfaces.nsICryptoHash);
        ch.init(ch.MD5);
        ch.update(data, data.length);
      var hash = ch.finish(false);
      var s = [];
      for (var i = 0, l = hash.length; i < l; ++i) {
        s.push(toHexString(hash.charCodeAt(i)));
      }
      
      return s.join("");
    },
    
    CallbackObject: function (success, error) {
      this.success = function () {
        try {
          return success.apply(this, arguments);
        } catch(e) {
          scope.log(' (!) CallbackObject::success() exception\n' + e.message +'@' +e.lineNumber); // + '; stack:' + e.stack);
          this.error();
        }
      }
      this.error = error || (function() {});
    },
    
    CallbackWatcher: function (abort) {
      this.abort = abort || function () {};
    },
    
    obj2UrlParams: function (obj, encode) {
      var value = "",
        res = [],
        noencode = (encode === false);
      
      var fixedEncodeURIComponent = function (str) {
        return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A');
      }
      
      for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
          value = noencode ? obj[key] : fixedEncodeURIComponent(obj[key]);
          key = noencode ? key : fixedEncodeURIComponent(key);
          res.push(key + "=" + value);
        }
      }
      return res.join("&");
    },
    
    urlParams2obj: function (urlParams, decode) {
      var res = {},
        nodecode = (decode === false);
      
      if (urlParams.length == 0) {
        return res;
      }

      var fixedDecodeURIComponent = function (str) {
        return decodeURIComponent(str.replace(/\+/g, '%20'));
      }
      
      var params = urlParams.split("&");
      for (var i = 0, l = params.length; i < l; ++i) {
        var [key, value] = params[i].split("=");
        key = nodecode ? key : fixedDecodeURIComponent(key);
        res[key] = nodecode ? value : fixedDecodeURIComponent(value);
      }
      return res;
    },
    
    mix: function F(a, b) {
      for (var i in b) {
        if (!b.hasOwnProperty(i)) {
          continue;
        }
        
        var data = b[i],
          mixin;
        
        if (i in a) {
          switch (typeof data) {
            case "object":
              mixin = F(a[i], data);
              break;
          
            default: 
              mixin = b[i];
          }
          
          a[i] = mixin;
        } else {
          a[i] = data;
        }
      }
      
      return a;
    },
    
    
    // ------ cookie auth ---------------------------------
    getCookie: function(host, name) {
      var cookieMgr = Components.classes["@mozilla.org/cookiemanager;1"].getService(Components.interfaces.nsICookieManager);
      for (var e = cookieMgr.enumerator; e.hasMoreElements();) {
        var cookie = e.getNext().QueryInterface(Components.interfaces.nsICookie);
        if(cookie.name == name && cookie.host == host && (0===cookie.expires || Date.now() < cookie.expires * 1000)) { return cookie.value || ''; }
      }
      return '';
    },
    deleteCookie: function(host, name, path) {
      var cookieMgr = Components.classes["@mozilla.org/cookiemanager;1"].getService(Components.interfaces.nsICookieManager);
      cookieMgr.remove(host, name, path || '/', false);
    },
    ajax: function(obj) {
      var xhr = Components.classes["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance(Components.interfaces.nsIXMLHttpRequest);
      
      var sync = !obj.callback;
      xhr.open("GET", obj.url, !sync);
      
      if(obj.background) {
        try {
          xhr.mozBackgroundRequest = true;
        } catch(exc) { }
      }
      
      if(!sync) {
        xhr.onreadystatechange = function() {
          if (xhr.readyState == 4) {
            var st = xhr.status, stt = '';
            if((st >= 200) && (st < 400)) {
              obj.callback(st, xhr);
            } else {
              try { stt = xhr.statusText; } catch(exc1) {}
              obj.errback && obj.errback(st, stt);
            }
            xhr = null;
          }
        };
      }
      xhr.send(null);
      return xhr;
    }    
  }
  
  var tray = {
    image: 'chrome://rambler_plugbar/skin/settings-logo.png',
    popup: function trayPopup(title, text, image, listener, croissant) {
      // scope.log(' * ' + arguments.callee.name+'('+title+','+text+')');
      try {
        image = image || tray.image || null;
        Components.classes['@mozilla.org/alerts-service;1'].
                  getService(Components.interfaces.nsIAlertsService).
                  showAlertNotification(image, title, text, ('object'==typeof listener? true : false), (croissant|| ''), ('object'==typeof listener? listener : null));
      } catch(e) {
        // prevents runtime error on platforms that don't implement nsIAlertsService
        // If you need to display a comparable alert on a platform that doesn't support nsIAlertsService, you can do this:
        image = image || tray.image || null;
        var win = Components.classes['@mozilla.org/embedcomp/window-watcher;1'].
                            getService(Components.interfaces.nsIWindowWatcher).
                            openWindow(null, 'chrome://global/content/alerts/alert.xul',
                                        '_blank', 'chrome,titlebar=no,popup=yes', null);
        win.arguments = [image, title, text, false, ''];        
      }
    }
  }  
    
  scope.utils = utils;  
  scope.tray = tray;
  
}
