var EXPORTED_SYMBOLS = ['unit'];

const DEBUG = true;

var Unit = function(){
  var $ = this;
  $.publicFuncs = ['update','login','logout'];
  
  $.init = function(api, wgtGuid){
    try {
      $.api = api;
      $.log = api.log;
      $.api.require('facebook.api', $);
      $.api.require('facebook.ui', $);
      $.api.require('facebook.utils', $);
      $.api.require('facebook.query', $);
      $.api.require('facebook.storage', $);
      $.wgtGuid = wgtGuid || '';
      $.widgetElement = $.api.getWidgetElement($.wgtGuid);
      $.reqs = {};
      $.account = null;
      $.onLogout();
      
      $.storage.start();
      
      $.FB.connect();
      
      return $;
      
    } catch (e) {
      $.log(' facebook Unit(): init() exception : ' + e.message + '@' + e.lineNumber);
    }
  };

  $.connected = function facebook__connnected() {
    return !!($.reqs && $.reqs.access_token);
  }; 
  
  $.update = function facebook__update() {
    $.query.fetch();
  },
  
  $.display = function facebook__display(data, menuset) {
    try {
      // $.log(' * ' + arguments.callee.name + '()');
      var wgt = $.api.getWidget($.wgtGuid);
      if (!$.wgtGuid || !wgt) {
        $.log('FB.display(): !$.wgtGuid || !wgt');
        return;
      }  
      // not connected
      if (!Array.isArray(data)) {
        var nfoData = $.api.Zepto.extend({}, (wgt.nfoBody || {}), {btnClass:'logout'}); 
        return $.api.createWidget([nfoData], wgt.guid);
      }      
      
      var nfoData = $.api.Zepto.extend(menuset || {}, wgt.nfoBody, { text: (data.length || ''), name: $.account.name, btnClass:'' });
      $.api.createWidget([nfoData], wgt.guid);  
      
    } catch (e) {
      $.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber + '.\nStack trace: ' + e.stack);
    }
  };
  
  $.setState = function(state){
    try {
      var wgt = $.api.getWidgetElement($.wgtGuid) || null;
      if (!wgt) return ;
      var tmp = $.api.Zepto(wgt).eq(0)[(state?'removeClass':'addClass')]('logout');
    } catch (e) {
      $.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber + '.\nStack trace: ' + e.stack);
    }
  };
  $.onLogin = function(){
    $.setState(true);
  };
  $.onLogout = function(){
    $.setState(false);
  };
  
  $.login = function facebook_login() {
    try {
      // $.log(' * ' + arguments.callee.name + '()');
      var callbacks = new $.utils.CallbackObject(
        function () {
          $.log('facebook_login(): connected');
        },
        function (error) {
          $.log("facebook_login(): not connected");
          $.onLogout();
        }
      );
      $.FB.proceedLogin(callbacks);      
      
    } catch (e) {
      $.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber);
    }
  };
  
	$.logout = function() {
		$.FB.deleteUserCookie();
    $.reqs = {};
    $.account = null;
    $.update();
	};  
  
  $.fetchQuery = function() {
    $.query.fetch();
  };
  
}

Unit.prototype = {
	log: function(f) {
		var cs = Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService),
			log = function (text) { return cs.logStringMessage('[FB.log]: ' + text) },
			empty = function () {};
		return f ? log : empty;
	}(DEBUG)
}

var unit = new Unit();