Components.utils.import("resource://gre/modules/NetUtil.jsm");
Components.utils.import("resource://gre/modules/FileUtils.jsm");

(function($){

    var sync = {
        // === section: Widgets Update ========================================
        initTimer: function() {
          try {
            $.sync.stopTimer();
            // $.log('sync.initTimer()')
            var timer = $.sync.timer = Components.classes["@mozilla.org/timer;1"].createInstance(Components.interfaces.nsITimer);
            timer.initWithCallback( $.sync.tickTimer, $.config.updateDelay, timer.TYPE_REPEATING_SLACK ); // timer.TYPE_ONE_SHOT
          } catch (e) {
            $.log('sync.initTimer() exception: ' + e.message);
          }        
        },
        stopTimer: function() {
          try {
            // $.log('sync.stopTimer()');
            if ($.sync.timer instanceof Components.interfaces.nsITimer){ $.sync.timer.cancel(); }
          } catch (e) {
            $.log('sync.stopTimer() exception: ' + e.message);
          } 
        },
        tickTimer: function() {
          $.util.camp.sendUsage();
          $.sync.setRBarCookie();
          if (!$.appNode.collapsed)
            $.sync.manageUpdate();          
        },
        manageUpdate: function() {
          try {
            // $.log('manageUpdate() ========================================');
            var widgets = $.scheme.widgets;
            if (!widgets.length) return;
            for (var i=0; i<widgets.length; i++) {
              if ($.scheme.isUpdateable(widgets[i].guid))
                $.scheme.updateWidget(widgets[i].guid);
            }
          } catch (e) {
            $.log('manageUpdate() exception: ' + e.message);
          }        
        },
        // === section: Boot Scheme config read/create/update  ================
        forceSUS: false,
        
        halt: function(errmsg) {
          $.log('Error: ' + errmsg);
          throw errmsg;
        },
        saveGUID: function(doc){
          var scm = Zepto('scheme', doc);
          var scmGuid  = scm && String(scm.attr('guid'));
          if (scmGuid)
            $.prefMgr.set('scheme.guid', scmGuid);
          // $.log(' * saveGuid() scm.attr(guid) : {' + scmGuid+'}');
        },
        getSchemeGUID: function(){
          var guid = $.prefMgr.get('scheme.guid', '');
          return $.config.RamblerGUID != $.APPGUID? $.APPGUID : (guid || $.APPGUID);
        },
        // scheme file             : ProfD/%appname%.cfg.xml
        // distributed scheme file : chrome://%appname%/content/tpl/scheme.xml
        bootScheme: function( doc ) {
          try {
            // $.log('$.sync.bootScheme()');
            var cF = $.sync.checkFileCfg($.config.cfgLocalPath);
            var dFTxt = $.sync.getTxtUtf8($.config.cfgDistribURI);
            // $.log(' * ' + $.config.cfgLocalPath +  $.util.fileInfo(cF));
            // current config unavailable
            if (!cF.fileSize) {
              $.sync.writeFile(cF, dFTxt, $.sync.onCfgCreated);
            } else {
              var assistCookies = $.util.auth.pick('assist.rambler.ru');
              if (
                (('install' in assistCookies) && assistCookies.install) 
                  ||
                ( ($.config.RamblerGUID != $.APPGUID) && ($.APPGUID != $.prefMgr.get('scheme.guid','')) )
              ) $.sync.forceSUS = true;
                
              var lastUpd = $.prefMgr.get('scheme.lastupdate', '1970-01-01');
              var today = $.util.getToday();
              $.log('bootScheme() * config lastUpd: ' + lastUpd + (lastUpd == today?' already up-to-date':' update needed') + ($.sync.forceSUS? ', forceSUS' : ''));
              if (!$.sync.forceSUS && lastUpd == today) {
                var docCfg = $.sync.getXmlDoc(cF);
                if (docCfg)
                  return $.scheme.onLoad(docCfg);
                else {
                  $.log('(!) sync: config '+cF.path+' corrupted. Restoring from defaults.');
                  return $.sync.writeFile(cF, dFTxt, $.sync.onCfgCreated);
                }  
              } else {
                return $.sync.onCfgCreated();
              }  
            }  
          } catch (e) {
            $.log('$.sync.bootScheme() exception: ' + e.message + '@'+ e.lineNumber);
          }
        },
        // when a config file in ProfD
        onCfgCreated: function() {
          var createdCfg = $.sync.checkFileCfg($.config.cfgLocalPath);
          // $.log('onCfgCreated() createdCfg.fileSize: '+createdCfg.fileSize);
          if (!createdCfg.fileSize)
            return $.sync.halt('(!) sync: config not created');
          var docCfg = $.sync.getXmlDoc(createdCfg);
          if (!docCfg) 
            return $.sync.halt('(!) sync: xml parser error');
          // tmpDoc should be used when update is impossible or not needed
          $.scheme.tmpDoc = docCfg;
          // schemeUpdate from a server
          $.sync.schemeUpdate(docCfg);
        },
        // schemeUpdate ajax callback
        resolveSchemeUpdate: function(doc, aCase) {
          try {
            // $.loggo(doc, '$.sync.resolveSchemeUpdate(); aCase: '+aCase+'; doc');

            if (!$.scheme.tmpDoc) {
              return $.sync.halt('tmpDoc is broken');
            }
            if (null===doc) {
              $.log('(!) server update unavailable');
              return $.scheme.onLoad($.scheme.tmpDoc);
            }
            if (doc) {
              var scm = Zepto('scheme', doc);
              // $.log('scm.length: '+scm.length+'; !scm[0]: '+scm[0]+'; scm[0].hasChildNodes(): '+scm[0].hasChildNodes());
              if (!scm.length || !scm[0]) {
                $.log('(!) server update XML corrupted');
                return $.scheme.onLoad($.scheme.tmpDoc);
              }
              if (!scm[0].hasChildNodes()) {
                $.log(' * config checked: ok');
                $.prefMgr.set('scheme.lastupdate', $.util.getToday()); 
                $.sync.forceSUS = false;
                return $.scheme.onLoad($.scheme.tmpDoc);
              }
              // server returned an update
              $.log(' * config updated');
              $.prefMgr.set('scheme.lastupdate', $.util.getToday());
              $.sync.forceSUS = false;
              return $.scheme.onLoad(doc);
            }
          } catch(e) {
            $.log('$.sync.resolveSchemeUpdate() exception: ' + e.message + '@'+ e.lineNumber);
          }
        },
        // update scheme from server
        schemeUpdate: function(doc) {
          try {
            // $.log('$.sync.schemeUpdate()');
            var reqBody = $.sync.makeSchemeRequest(doc);
            if (!reqBody)
              return $.sync.halt('sync.schemeUpdate() reqBody is empty');
            var xhrArgs = {
              dataType: 'xml', 
              type: 'POST',
              url:  $.config.schemeUpdateURI + ($.sync.getSchemeGUID()) + '/',
              headers: $.util.getHttpHeaders('xml'),
              data: {
                pretty: 1,
                xml: reqBody
              }, 
              error: (function(xhr, aCase, error) { // $.log('schemeUpdate() ajax error. xhr.status:'+xhr.status);
                $.sync.resolveSchemeUpdate(null, aCase);
              }),
              success: (function(doc, aCase, xhr) { // $.log('schemeUpdate ajax success. xhr.status: ' + xhr.status);
                $.sync.resolveSchemeUpdate(doc, aCase);
              })
            };
            var xhr = Zepto.ajax( xhrArgs );
            setTimeout(function(){
              if (4 != xhr.readyState && 'function'==typeof xhr.abort){ xhr.abort(); }
            }, 3000);
            
          } catch(e) {
            $.log('$.sync.schemeUpdate() exception: ' + e.message + '@'+ e.lineNumber);
          }
        },
        
        // returns (XMLDocument) scheme containing empty widget elements
        makeSchemeRequest: function(doc) {
          try {
            // $.loggo(doc, '$.sync.makeSchemeRequest() doc', 1);
            var dcn = ((doc instanceof window['DocumentFragment'] || doc instanceof window['Document'])? 
                        doc.cloneNode(true) : null);
            if (!dcn) 
              return;
            var scm = Zepto('scheme', dcn);
            if (!scm.length)
              return;
              
            // $.sync.updateGUID = $.config.RamblerGUID != $.APPGUID? $.APPGUID : (scm[0].getAttribute('guid') || $.APPGUID);
            scm[0].setAttribute('guid', $.sync.getSchemeGUID());
              
            var schemeVersion = scm[0].getAttribute('version') || 0;
            if (schemeVersion)
              $.config.schemeVersion = schemeVersion;
            else
              scm[0].setAttribute('version', $.config.schemeVersion);
            
            var chd = (scm[0].hasChildNodes())? scm[0].childNodes : [];
            for (var m=0; m<chd.length; m++){
              if (Node.ELEMENT_NODE == chd[m].nodeType && 'widget'==chd[m].nodeName) {
                chd[m].setAttribute('version', parseInt((chd[m].getAttribute('version') || 1), 10) - 0);
                while (chd[m].firstChild) 
                  chd[m].removeChild(chd[m].firstChild);              
              }
            }
            var cfgTxt = $.xmlSerializer.serializeToString(dcn); 
            cfgTxt = cfgTxt.replace(/[ ]+[\r\n]+/mg,'\n').replace(/[\r\n]+/mg,'\n');
            cfgTxt = cfgTxt.replace(/<\?[^>]+\?>/gm,'').replace(/^\s+|\s+$/g, '');
            
            // $.log(' * cfgTxt ====================================== \r\n'); $.log(cfgTxt);
            
            return cfgTxt;
          } catch(e) {
            $.log('$.sync.makeSchemeRequest() exception: ' + e.message + '@'+ e.lineNumber);
            return false;
          }
        },
        
        // === section: File I/O ==============================================
        // path = 'ProfD/%appname%.cfg.xml'
        checkFileCfg: function(path) {
          try {
            if ('string' != typeof path || !path) return false;
            var fh = null, 
                fhExPath = '';
            // $.log(' $.sync.checkFileCfg() path: ' + path);
            
            var a = path.replace(/\/+$/g, '').split('/'); 
            if (''==a[0] && a.length>1) {
              a.shift(0);
              a[0] = '/'+a[0];
            }
            for (var p=0; p<a.length; p++) {
              if (0==p) {
                var q = true;
                if (p==a.length-1) {
                  fh = FileIO.open(a[p]);
                  if (!fh.exists()) { q = FileIO.create(fh); }
                } else {
                  fh = DirIO.get(a[p]);
                  if (!fh.exists()) { q = DirIO.create(fh); }
                }    
                if (!q) return false;
                // $.loggo(fh, 'open : nsIFile '+a[p]);
                if (fh.exists()) { 
                  fhExPath = fh.path; 
                  continue; 
                } else return false;
              }
              if (fh instanceof Components.interfaces.nsIFile) {
                var q = true;
                fh.append(a[p]);
                if (fh.exists()) { 
                  fhExPath = fh.path; 
                } else {
                  if (p==a.length-1) { 
                    q = FileIO.create(fh); 
                  } else { 
                    q = DirIO.create(fh); 
                  }
                }
                if (!q) return false;
                if (fh.exists()) {
                  fhExPath = fh.path; 
                } else {
                  $.log('(!) sync.checkFileCfg() could not create: ' + a[p]);
                  return false;
                };
              }
            }
            
            return fh;
            
          } catch(e) {
              $.log('$.sync.checkFileCfg() exception: ' + e.message + ' @'+ e.lineNumber);
          }
        },
        
        getTxtUtf8: function(uri) { // 'chrome://'+$.APPNAME+'/content/tpl/presets.xml'
          var src = $.sync.getContents(uri);
          return $.sync.convertToCharset(src, "UTF-8");
        },
        
        getXmlDoc: function(arg) { // nsIFile or 'chrome://'+$.APPNAME+'/content/tpl/presets.xml'
          try {
            var src =  ('string'==typeof arg)? ($.sync.getContents(arg)) : ((arg instanceof Components.interfaces.nsIFile)? (FileIO.read(arg)) : null);
            if (!src) 
              return null;
            var u8 = $.sync.convertToCharset(src, "UTF-8");
            var doc = (new DOMParser()).parseFromString(u8, "application/xml");
            if ('parsererror'!=doc.documentElement.nodeName) 
              return doc;
            return null;
          } catch (e) {
            $.log('sync.getXmlDoc() exception: ' + e.message + ' @'+e.lineNumber);
            return null;
          }
        },
        
        // file is nsIFile, data is a string
        // You can also optionally pass a flags parameter here. It defaults to
        // FileUtils.MODE_WRONLY | FileUtils.MODE_CREATE | FileUtils.MODE_TRUNCATE;
        writeFile: function(file, data, fnBack) {
          try {
            var ostream = FileUtils.openSafeFileOutputStream(file);
            var converter = Components.classes["@mozilla.org/intl/scriptableunicodeconverter"].
                            createInstance(Components.interfaces.nsIScriptableUnicodeConverter);
            converter.charset = "UTF-8";
            var istream = converter.convertToInputStream(data);
            NetUtil.asyncCopy(istream, ostream, fnBack);
          } catch(e) {
            $.log('$.sync.writeFile() exception: ' + e.message + '@'+ e.lineNumber);
          }
        },            
        // sstream is nsIScriptableInputStream:
        // var str = sstream.read(4096), charset="UTF-8" 
        convertToCharset : function(str, charset) {
          var utf8Converter = Components.classes["@mozilla.org/intl/utf8converterservice;1"].getService(Components.interfaces.nsIUTF8ConverterService);
          var data = utf8Converter.convertURISpecToUTF8 (str, "UTF-8");    
          return data;
        },
        
        /*
        Subclasses of nsIURI, such as nsIURL , impose further structure on the URI.
        To create an nsIURI object, you should use nsIIOService.newURI() , like this:    
        */
        makeURI: function(aURL, aOriginCharset, aBaseURI) {
          var ioService = Components.classes["@mozilla.org/network/io-service;1"]
                        .getService(Components.interfaces.nsIIOService);
          return ioService.newURI(aURL, aOriginCharset, aBaseURI);
        },          
        /*
        @parameter (string) aURL - file://, http://, chrome://, resource:// 
        @returns (string) text file contents
        */
        getContents : function(aURL) { 
          var ioService=Components.classes["@mozilla.org/network/io-service;1"]
              .getService(Components.interfaces.nsIIOService);
          var scriptableStream=Components
              .classes["@mozilla.org/scriptableinputstream;1"]
              .getService(Components.interfaces.nsIScriptableInputStream);

          var channel=ioService.newChannel(aURL,null,null);
          var input=channel.open();
          scriptableStream.init(input);
          var str=scriptableStream.read(input.available());
          scriptableStream.close();
          input.close();
          return str;
        },        
        
        downloadFile: function(url, file, progressListener) { // url == 'http://server.tld/file.ext', |file| == nsIFile
          try {
            var fileURI = $.sync.makeURI(url, null, null);
            // create a persist
            var persist = Components.classes["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"]
                          .createInstance(Components.interfaces.nsIWebBrowserPersist);
            // with persist flags if desired See nsIWebBrowserPersist page for more PERSIST_FLAGS.
            const nsIWBP = Components.interfaces.nsIWebBrowserPersist;
            const flags = nsIWBP.PERSIST_FLAGS_REPLACE_EXISTING_FILES;
            persist.persistFlags = flags | nsIWBP.PERSIST_FLAGS_FROM_CACHE;
            if (progressListener) 
                persist.progressListener = progressListener;
            // do the save
            persist.saveURI(fileURI, null, null, null, "", file); 
            
            return true;
          } catch(e) {
            return e;
          }
        },
        
        // === section: Stylesheets ===========================================
        operateStylesheet: function(localURI, action){
          try {
            // $.log(' * sync.operateStylesheet('+localURI+', '+action+')');
            if (-1==['load','remove'].indexOf(String(action))) return;
            var sss = Components.classes["@mozilla.org/content/style-sheet-service;1"]
                                .getService(Components.interfaces.nsIStyleSheetService);
            var ios = Components.classes["@mozilla.org/network/io-service;1"]
                                .getService(Components.interfaces.nsIIOService);
            var uri = ios.newURI(localURI, null, null);
            switch(action) {
              case 'load' :
                sss.loadAndRegisterSheet(uri, sss.USER_SHEET);    
                break;  
              case 'remove':  
                if(sss.sheetRegistered(uri, sss.USER_SHEET))
                  sss.unregisterSheet(uri, sss.USER_SHEET);        
                break;
            }
            return true;
          } catch (e) {
            $.log('operateStylesheet() exception: ' + e.message + ' @ '+e.lineNumber);
            return false;
          }
        
        },
        // Note: loadAndRegisterSheet will load the stylesheet synchronously, so you should only call this method using local URIs.
        // localURI "chrome://myext/content/myext.css"
        loadStylesheet: function(localURI) {
          return $.sync.operateStylesheet(localURI, 'load');
        },        
        // If you wish to remove a stylesheet that you previously registered, simply use the unregisterSheet method.
        removeStylesheet: function(localURI) {
          return $.sync.operateStylesheet(localURI, 'remove');
        },        
        
        // === section: local resources =======================================
        getImageFromURL: function(url) {
          try {
            var ioserv = Components.classes["@mozilla.org/network/io-service;1"] 
                         .getService(Components.interfaces.nsIIOService); 
            var channel = ioserv.newChannel(url, 0, null); 
            var stream = channel.open(); 

            if (channel instanceof Components.interfaces.nsIHttpChannel && channel.responseStatus != 200) { 
              return ""; 
            }

            var bstream = Components.classes["@mozilla.org/binaryinputstream;1"] 
                          .createInstance(Components.interfaces.nsIBinaryInputStream); 
            bstream.setInputStream(stream); 
            
            var bArray = bstream.readByteArray(stream.available());
            bstream.close();
            stream.close();
            
            return bArray;

          } catch (e) {
            $.log('$.sync.getImageFromURL() exception: ' + e.message + ' @ '+e.lineNumber);
          } 
        },        
        
        readBinaryFile: function(file) {
          try {
            var fileStream = Components.classes['@mozilla.org/network/file-input-stream;1']
                             .createInstance(Components.interfaces.nsIFileInputStream);
            fileStream.init(file, 1, 0, false);
            var binaryStream = Components.classes['@mozilla.org/binaryinputstream;1']
                               .createInstance(Components.interfaces.nsIBinaryInputStream);
            binaryStream.setInputStream(fileStream);
            var bArray = binaryStream.readByteArray(fileStream.available());
            binaryStream.close();
            fileStream.close();
            
            return bArray;      
          } catch (e) {
            $.log('$.sync.readBinaryFile() exception: ' + e.message + ' @ '+e.lineNumber);
          } 
        },
        
        writeBinaryFile: function(file, bArray) {
          try {
            var fileStream = Components.classes['@mozilla.org/network/file-output-stream;1']
                             .createInstance(Components.interfaces.nsIFileOutputStream);
            fileStream.init(file, 2, 0x200, false);
            var binaryStream = Components.classes['@mozilla.org/binaryoutputstream;1']
                               .createInstance(Components.interfaces.nsIBinaryOutputStream);
            binaryStream.setOutputStream(fileStream);
            binaryStream.writeByteArray(bArray, bArray.length);
            binaryStream.close();
            fileStream.close();        
            return true;
          } catch (e) {
            $.log('$.sync.writeBinaryFile() exception: ' + e.message + ' @ '+e.lineNumber);
            return false;
          } 
        },
        
        imageToResource: function(remoteURL) {
          try {
            var re = /^(https?:\/\/)(.*)\.([a-z0-9]+)$/gi;
            if ('string'!=typeof remoteURL || !re.test(remoteURL)) 
              return '';
            var uPts = (/^(https?:\/\/)(.*)\.([a-z0-9]+)$/gi).exec(remoteURL);
            var extF = (uPts.length==4)? '.'+uPts[3] : '.dat';
            var localURL = 'resource://rambler_plugbar-cache/'+md5(remoteURL)+extF;
            var ioService = Components.classes["@mozilla.org/network/io-service;1"]
                .getService(Components.interfaces.nsIIOService);
            var channel = ioService.newChannel(localURL,null,null);
            var fileHandler = ioService.getProtocolHandler('file')
                              .QueryInterface(Components.interfaces.nsIFileProtocolHandler);
            var fhOut = fileHandler.getFileFromURLSpec(channel.URI.spec);
            // $.log(' * $.sync.imageToResource() remoteURL: '+remoteURL+'; localURL: '+localURL+'path: '+fhOut.path+'; fhOut.exists(): '+fhOut.exists());
            if (fhOut.exists() && fhOut.fileSize>0)
              return localURL;
              
            fhOut.create(fhOut.NORMAL_FILE_TYPE, 0777);
            var bArray = $.sync.getImageFromURL(remoteURL);
            if (!bArray.length) {
              $.log('$.sync.imageToResource(): bArray empty, halt. remoteURL: ' + remoteURL);
              fhOut.remove(); // removing an empty file
              return '';
            }
            $.log('$.sync.getImageFromURL('+remoteURL+') bArray ('+bArray.length+') '); 
            // $.log(bArray.map(function(aItem) {return aItem.toString(16);}).join(' ').toUpperCase());
            
            var written = $.sync.writeBinaryFile(fhOut, bArray);
            if (written) 
              return localURL;
            return '';
              
          } catch (e) {
            $.log('$.sync.imageToResource() exception: ' + e.message + ' @ '+e.lineNumber);
            return '';
          } 
        },
        
        // doc: scheme xml doc
        localizeImages: function(tDoc, customSelectors) {
          try {
            if (!tDoc) return;
            
            // custom selectors:
            var selectors = {
              imglist: 'informer>image',
              menuimg: 'informer>menus menuitem[image]'
            };
            if (customSelectors) { 
              for (var selector in selectors) {
                var customSelector = customSelectors[selector];
                if (customSelector)
                  selectors[selector] = customSelector;
              }
            }  

            var imgList = Zepto(selectors.imglist, tDoc);
            // $.log('$.sync.localizeImages() imgList.length:' + imgList.length+'; imgList[0].textContent:'+imgList[0].textContent);
            for (var i=0; i<imgList.length; i++) {
              var imgElm = imgList[i];
              var res = $.sync.imageToResource(imgElm.textContent);
              if (!res) 
                continue;

              while (imgElm.firstChild) 
                imgElm.removeChild(imgElm.firstChild);
              var cdata = imgElm.ownerDocument.createCDATASection(String(res));
              imgElm.appendChild(cdata);
            }
            
            var menuitems = Zepto(selectors.menuimg, tDoc);
            // $.log('$.sync.localizeImages() menuitems.length:' + menuitems.length); 
            for (var j=0; j<menuitems.length;j++) {
              var mni = menuitems[j];
              if (!mni.hasAttribute('image'))
                continue;
              var imgR = $.sync.imageToResource(mni.getAttribute('image'));
              if (imgR)
                mni.setAttribute('image', imgR);
            }
            
            return tDoc;
            
          } catch (e) {
            $.log('$.sync.localizeImages() exception: ' + e.message + ' @ '+e.lineNumber);
          } 
        },
        
        // === section: utilities  ============================================
        setRBarCookie: function(){
          $.util.cookie.setValue('rbar', $.APPVERSION + ($.util.isGhost()? ':invisible' : ''), {expires: 7*24*3600}); 
        },
        transformXML: function(aXML, aStylesheet) {
            if (!(aXML && aStylesheet))
                return null;
            
            var xsltProcessor = Components.classes["@mozilla.org/document-transformer;1?type=xslt"].createInstance(Components.interfaces.nsIXSLTProcessor);
            
            var result = null;
            
            try {
                xsltProcessor.importStylesheet(aStylesheet);
                var ownerDocument = document.implementation.createDocument("", "xsl-tmp", null);
                result = xsltProcessor.transformToFragment(aXML, ownerDocument);
            } catch (ex) {
                $.log("Can't transform XML. " + ex);
            }
            
            if (!result || (result.documentElement && result.documentElement.localName == "parsererror")) {
                $.log("Can't transform XML.");
                result = null;
            }
            
            return result;
        },    
        // NB! when 'guids' param containing only one item, animation will start on a webpage
        notifyGallery: function(/*<bool> uninstall, <Array> guids*/) {
          try {
            var uninstall = arguments[0] || false,
                guids = arguments[1] || $.scheme.getGuids();
            var func = uninstall? 'rambler_enable_widgets' : 'rambler_disable_widgets';
            var xprops = {};
            for (var y=0; y<guids.length; y++)
              xprops[y] = 'rw';
            guids.__exposedProps__ = xprops;
            // $.log(' * notifyGallery() func:'+func+'; guids: '+JSON.stringify(guids));
            
            var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
                               .getService(Components.interfaces.nsIWindowMediator);
            var winEnum = wm.getEnumerator("navigator:browser");
            while (winEnum.hasMoreElements()) {
              var aWin = winEnum.getNext();
              var tabbrowser = aWin.gBrowser;
              for (var i=0; i<tabbrowser.browsers.length; i++) {
                var browser = tabbrowser.getBrowserAtIndex(i);
                var jsWObj = browser? (browser && browser.contentWindow? browser.contentWindow.wrappedJSObject : null) : null;
                if (jsWObj[func]) {
                  jsWObj[func](guids);
                }
              }
            }
          } catch(e) {
            $.log('sync.notifyGallery() exception: '+e.message+' @ '+e.lineNumber);
          }
        },
        
        // === section: birthday =======================================
        retrieveBirthday: function(callback){
          // $.log('sync.retrieveBirthday()');
          var xhrArgs = {
            type     : 'GET', 
            url      : 'http://informers.rambler.ru/get_birthday/',
            data     : { version: 4 },
            headers  : $.util.getHttpHeaders('xml'),
            dataType : 'xml', 
            error    : (function(xhr, aCase, error) { 
              $.log('ajax://get_birthday/ error: '+error); 
            }),
            success  : (function(docR, aCase, xhr) { 
              // $.log('ajax://get_birthday/ success');
              var bdy = Zepto('result birthday', docR).text(),
                  bda = bdy.split('-'),
                  validDate = (3==bda.length? [bda[0], parseInt(bda[1],10)-1, bda[2]] : []);
              // $.loggo(validDate,'bdy: ' +bdy+'; validDate:'+validDate);
              if ('function' == typeof callback) 
                callback(validDate);
            })
          }; 
          // $.loggo(xhrArgs, 'sync.retrieveBirthday() ajax://get_birthday/ xhrArgs');
          var xhr = Zepto.ajax( xhrArgs ); 
        },
        storeBirthday: function(bday){
          // $.log('sync.storeBirthday() bday: '+bday);
          var xhrArgs = {
            type     : 'POST', 
            url      : 'http://informers.rambler.ru/set_birthday/',
            data     : { birthday: bday, version: 4 },
            headers  : $.util.getHttpHeaders('xml'),
            dataType : 'xml', 
            error    : (function(xhr, aCase, error) { 
              $.log('ajax://set_birthday/?birthday='+bday+' - error: '+error); 
            }),
            success  : (function(docR, aCase, xhr) { 
              // $.log('ajax://set_birthday/?birthday='+bday+' - success'); 
              $.scheme.updateWidget('53d7265c-9c1e-4ec1-99d8-b9cbc46b2140');
            })
          }; 
          // $.loggo(xhrArgs, 'sync.setBirthday() ajax://set_birthday/?birthday='+bday+'; xhrArgs');
          var xhr = Zepto.ajax( xhrArgs );        
        },
        
        // === section: geoid =======================================
        cookies: ('function'==typeof $.util.auth.pick && $.util.auth.pick()) || null,
        setupGeo: function(force) {
          try {
            var cookies = $.sync.cookies = !!force? $.util.auth.pick() : ($.sync.cookies || $.util.auth.pick());
            // $.loggo(cookies, ' * $.sync.setupGeo(); cookies. cookies.geoid={'+cookies.geoid+'} uri part:{'+(cookies.geoid? 'geoid/'+cookies.geoid+'/':'')+'}'); 
            var cfg = {
              url      : 'http://informers.rambler.ru/city/' + (cookies.geoid? 'geoid/'+cookies.geoid+'/':'') + '?version=4',
              headers  : $.util.getHttpHeaders('xml'),
              dataType : 'xml', 
              error    : function(xhr, s) { $.log('$.sync.setupGeo(), ajax.error(); xhr: ' + xhr) },
              success  : $.sync.setGeoPrefs
            };
            var xhr = Zepto.ajax( cfg );  
            setTimeout(function(){
              if (4 != xhr.readyState && 'function'==typeof xhr.abort){ xhr.abort(); }
            }, 2000);
            
          } catch (e) {
            $.log(' (!) $.sync.setupGeo() exception: ' + e.message + ' @ '+e.lineNumber);
          }
        },
        setGeoPrefs: function(doc, s, xhr) {
          try {
            // $.loggo(doc, ' * $.sync.setGeoPrefs() doc');
            var geoname = $.util.query('name', doc) || '',
                parents = $.util.query('parents', doc) || '',
                geoid = $.util.query('id', doc) || '';
                
            if (geoname && geoid) {
              var re = new RegExp('\\)\\s?\\(', 'g'),
                  georec = (geoname.textContent + (parents.textContent? ' (' + parents.textContent + ')' : '')).replace(re,', ');
              $.prefMgr.set('options.geoid.list', geoid.textContent);
              $.prefMgr.set('options.city.list',  georec);
              
              $.scheme.updateWidget('9d08646d-81cd-4076-9624-d6fc89b59735'); // weather
              $.scheme.updateWidget('b821ba5f-80fd-4e46-bea3-46e47e02ccbb'); // traffic
            }
            
          } catch (e) {
            $.log(' (!) $.sync.setGeoPrefs() exception: ' + e.message + ' @ '+e.lineNumber);
          }
        },        
        
        onPageLoad: function(event) {
          var doc = event.originalTarget;
          if (/^https?:\/\/[\w\.]+rambler\.ru\/gallery\/install_widget\//gi.test(doc.defaultView.location.href)) {
            $.log('$.sync.onPageLoad() add widget');
            $.scheme.addFetchedWidget(doc);
          } else if ( /^https?:\/\/[\w\.]+rambler\.ru\//gi.test(doc.defaultView.location.href)) {
            $.log('$.sync.onPageLoad() rambler page');
          }
        }
    };    
    
    $.sync = sync;
    
})( this['rambler_plugbar'] );
