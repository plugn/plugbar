/* ***************************************************************
 *   Zepto.ajax.moz.js
 *   (c) 2010, 2011 Thomas Fuchs
 *    (c) 2011 mozSafe patch  Max Dolgov 
 * **************************************************************/

(function($){
  var isObject = $.isObject,
      key;
  function empty() {};
  $.ajaxSettings = {
    type: 'GET',
    headers: {},
    nocache: false,
    dataType: 'any',
    beforeSend: empty,
    success: empty,
    error: empty,
    complete: empty,
    accepts: {
      script: 'text/javascript, application/javascript',
      json:   'application/json',
      xml:    'application/xml, text/xml',
      html:   'text/html',
      text:   'text/plain',
      any:    '*'+'/'+'*'
    },
    timeout: 0
  };
  $.ajax = function(options){
    options = options || {};
    var settings = $.extend({}, options);
    for (key in $.ajaxSettings) if (!settings[key]) settings[key] = $.ajaxSettings[key];

    if (!settings.url) settings.url = window.location.toString();
    if (settings.data && !settings.contentType) settings.contentType = 'application/x-www-form-urlencoded';
    if (isObject(settings.data)) settings.data = $.param(settings.data);

    if (settings.type.match(/get/i) && settings.data) {
      var queryString = settings.data;
      if (settings.url.match(/\?.*=/)) {
        queryString = '&' + queryString;
      } else if (queryString[0] != '?') {
        queryString = '?' + queryString;
      }
      settings.url += queryString;
    }

    var mime = settings.accepts[settings.dataType],
        xhr = new XMLHttpRequest();

    if (!settings.headers['X-Requested-With']) { settings.headers = $.extend({'X-Requested-With': 'XMLHttpRequest'}, settings.headers || {}) };
    if (!settings.headers['Accept'] && mime)   { settings.headers['Accept'] = mime; }
    
    xhr.onreadystatechange = function(){
      if (xhr.readyState == 4) {
        xhr.onreadystatechange = empty;
        var result, error = false;
        if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
          if (mime == 'application/json' && !(xhr.responseText == '')) {
            try { result = JSON.parse(xhr.responseText); }
            catch (e) { error = e; }
          }
          else if(settings.dataType == 'xml') { result = xhr.responseXML; } 
          else { result = xhr.responseText; }
          if (error) { settings.error(xhr, 'parsererror', error); }
          else { settings.success(result, 'success', xhr); }
        } else {
          error = true;
          settings.error(xhr, 'error');
        }
        settings.complete(xhr, error ? 'error' : 'success');
      }
    };

    xhr.open(settings.type, settings.url, true);
    if (('nocache' in settings) && settings.nocache) { xhr.channel.loadFlags |= Components.interfaces.nsIRequest.LOAD_BYPASS_CACHE; }

    if (settings.contentType) settings.headers['Content-Type'] = settings.contentType;
    for (name in settings.headers) xhr.setRequestHeader(name, settings.headers[name]);

    var sendRequest = function () {
      if (settings.beforeSend(xhr, settings) === false) {
        xhr.abort();
        return false;
      }
      xhr.send(settings.data);
    };

    if (settings.timeout > 0) {
      setTimeout((function () {
      if (settings.beforeSend(xhr, settings) === false) {
        xhr.onreadystatechange = empty;
        xhr.abort();
        return false;
      }
      xhr.send(settings.data);
    }), settings.timeout);
    } else if (sendRequest() === false) {
      return false;
    }

    return xhr;
  };

  $.get = function(url, success){ $.ajax({ url: url, success: success }) };

  $.post = function(url, data, success, dataType){
    if ($.isFunction(data)) dataType = dataType || success, success = data, data = null;
    $.ajax({ type: 'POST', url: url, data: data, success: success, dataType: dataType });
  };

  $.getJSON = function(url, success){ $.ajax({ url: url, success: success, dataType: 'json' }) };

  $.fn.load = function(url, success){
    if (!this.length) return this;
    var self = this, parts = url.split(/\s/), selector;
    if (parts.length > 1) url = parts[0], selector = parts[1];
    $.get(url, function(response){
      self.html(selector ?
        $(document.createElement('div')).html(response).find(selector).html()
        : response);
      success && success();
    });
    return this;
  };

  $.param = function(obj, v){
    var result = [], add = function(key, value){
      result.push(encodeURIComponent(v ? v + '[' + key + ']' : key)
        + '=' + encodeURIComponent(value));
      },
      isObjArray = $.isArray(obj);

    for(key in obj)
      if(isObject(obj[key]))
        result.push($.param(obj[key], (v ? v + '[' + key + ']' : key)));
      else
        add(isObjArray ? '' : key, obj[key]);

    return result.join('&').replace('%20', '+');
  };
})(Zepto);
