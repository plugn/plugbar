(function($){
  var sniff = {
    messenger : {
      listenRequest: function(callback) { // analogue of chrome.extension.onRequest.addListener
        return document.addEventListener("plugbar-query", function(event) {
          var node = event.target, doc = node.ownerDocument;

          return callback(node.getUserData("data"), doc, function(data) {
            if (!node.getUserData("callback")) {
              return doc.documentElement.removeChild(node);
            }
            node.setUserData("response", data, null);
            var listener = doc.createEvent("HTMLEvents");
            listener.initEvent("plugbar-response", true, false);
            return node.dispatchEvent(listener);
          });
        }, false, true);
      },
      callback: function(request, sender, callback) {
        if ('widget-install'==request.cmd && request.uri) {
          $.scheme.addWidget(request.uri);
        } else if ('widget-uninstall'==request.cmd && request.guid) {
          $.scheme.removeWidget(request.guid);
        }
        return callback(null);
      }
    },
    watcher: {
      observe: function(subject, topic, data) {
        if ('content-document-global-created' == topic && -1 !== String(subject.location.href).search($.config.galleryRE)) {
           subject.addEventListener('load',  function(event) {
                                      $.sync.notifyGallery();
                                    }, true);
        }
      },
      register: function(){
        var observerService = Components.classes["@mozilla.org/observer-service;1"]
                              .getService(Components.interfaces.nsIObserverService);
        observerService.addObserver($.util.sniff.watcher, 'content-document-global-created', false);
      },
      unregister: function(){
        var observerService = Components.classes["@mozilla.org/observer-service;1"]
                                .getService(Components.interfaces.nsIObserverService);
        observerService.removeObserver($.util.sniff.watcher, 'content-document-global-created');
      }      
    },
    cookieWatcher: {
      observe: function(subject, topic, data){
        try {
          if ('cookie-changed' != topic)
            return;
          if ('batch-deleted'==data) {
            var enumerator = subject.QueryInterface(Components.interfaces.nsIArray).enumerate();
            while (enumerator.hasMoreElements()) {
              var cookie = enumerator.getNext().QueryInterface(Components.interfaces.nsICookie2);
              if ('rsid'==cookie.name && '.rambler.ru'==cookie.host) {
                // $.log(' * rsid changed: (' + data + ')');
                $.scheme.updateWidget('e75874d9-90bf-4e60-a6eb-7304002f5fb8'); // update mail widget
                $.scheme.updateWidget('53d7265c-9c1e-4ec1-99d8-b9cbc46b2140'); // update horo widget
                return;
              }
            } 
            return;
          }
          if (-1 < ['cleared','reload'].indexOf(data)) { // affects all cookies 
            $.scheme.updateWidget('e75874d9-90bf-4e60-a6eb-7304002f5fb8'); // update mail widget
            $.scheme.updateWidget('53d7265c-9c1e-4ec1-99d8-b9cbc46b2140'); // update horo widget           
            return;
          }  
            
          if (['added','deleted','changed'].indexOf(data) < 0)
            return;          
          var aCookie = subject.QueryInterface(Components.interfaces.nsICookie2);
          if ('.rambler.ru'==aCookie.host) {
            if ('geoid'==aCookie.name) {
              $.log(' * geoid changed: (' + data + ')');
              $.sync.setupGeo(true);
            } else if ('rsid'==aCookie.name) {
              // $.log(' * rsid changed: (' + data + ')');
              $.scheme.updateWidget('e75874d9-90bf-4e60-a6eb-7304002f5fb8'); // update mail widget
              $.scheme.updateWidget('53d7265c-9c1e-4ec1-99d8-b9cbc46b2140'); // update horo widget           
              return;
            } 
          }
          
        } catch (e) {
          $.loggo(subject, 'cookieWatcher.observe() exception: '+e.message+' @ '+e.lineNumber+'. topic:{'+topic+'}, data:{'+data+'} ;;;; subject ');
        }
      },
      register: function(){
        var observerService = Components.classes["@mozilla.org/observer-service;1"]
                              .getService(Components.interfaces.nsIObserverService);
        observerService.addObserver($.util.sniff.cookieWatcher, 'cookie-changed', false);
      },
      unregister: function(){
        var observerService = Components.classes["@mozilla.org/observer-service;1"]
                                .getService(Components.interfaces.nsIObserverService);
        observerService.removeObserver($.util.sniff.cookieWatcher, 'cookie-changed');
      }
    },
    geoWatcher : new Watcher('autocomplete-did-enter-text', function(subject, topic, data) {
      if ('informers.rambler.ru/geosuggest/s/' != subject.searchParam)
        return; 
      var geoid = subject.controller.getCommentAt(subject.popup.selectedIndex);
      if (geoid)
        $.util.cookie.setValue('geoid', geoid, {expires: 800000000});
    }),
    appQuit: {
      observe: function(subject, topic, data) {
        try {
          // $.promptSvc.alert(null, 'appQuit.observe', 'beingUninstalled: ' + $.util.sniff.addonWatcher.beingUninstalled);
          if ($.util.sniff.addonWatcher.beingUninstalled)
            $.prefMgr.set('license.accepted', false);
        } catch (e) {
          $.log(' (!) appQuit.observe() exception:' + e.message + '@' + e.lineNumber);
        }
      }, 
      init: function() {
        this.watcher = new Watcher('quit-application', $.util.sniff.appQuit.observe);
        this.watcher.register();
      }
    },
    addonWatcher: {
      beingUninstalled: false,
      listener: {
        onUninstalling: function(addon) {
          if (addon.id == $.ADDONID) {
            $.util.sniff.addonWatcher.beingUninstalled = true;
            
          }
        },
        onOperationCancelled: function(addon) {
          if (addon.id == $.ADDONID) {
            $.util.sniff.addonWatcher.beingUninstalled = (addon.pendingOperations & AddonManager.PENDING_UNINSTALL) != 0;
          }
        }
      },
      init : function() {
        try {
          AddonManager.addAddonListener($.util.sniff.addonWatcher.listener);
        } catch (e) {
          $.log('sniff.addonWatcher.init() exception: '+e.message+' @ '+e.lineNumber);
        }
      }
      
    }
    
  };    
  $.util.sniff = sniff;
    
})( this['rambler_plugbar'] );