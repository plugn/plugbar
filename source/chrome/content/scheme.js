(function($){ 

    var scheme = {
        widgets : null,
        widgetsByGuid: null,
        cfgDoc: null,
        tmpDoc: null,
        tplStore: null,
         
        onLoad : function( doc ) {
          try {
            // $.log('scheme.onLoad() ' );
            if (!doc) return;
            
            $.sync.saveGUID(doc);
            $.scheme.saveConfig(doc);
            $.scheme.cfgDoc = doc;
            $.scheme.tmpDoc = null;            
      
            $.scheme.setup(doc);
            $.scheme.update();
            $.sync.initTimer();
          } catch(e) {
            $.log('scheme.onLoad() exception : ' + e.message + ' @ ' + e.lineNumber);
          }
        },
        
        initTemplates: function() {
          try {
            var tplDoc = $.scheme.tplDoc = $.sync.getXmlDoc('chrome://'+$.APPNAME+'/content/tpl/templates.xml');
            // $.loggo(tplDoc, 'tplDoc');
            var tpls = Zepto('[control]', tplDoc), 
                tplStore = $.scheme.tplStore = {};
            for (var i=0; i<tpls.length; i++){
              var tpl = tpls[i],
                  control = (tpl.getAttribute('control') || '');
              if(tpl && control && tpl.nodeName){
                if ('undefined' == typeof tplStore[control])
                  tplStore[control] = {}; 
                var jxtpl = tpl? $.util.jxt.dom2jxon( tpl ) : null;
                tplStore[control][tpl.nodeName] = (jxtpl? jxtpl.nodeValue : null);
              }
            }
            // $.loggo($.scheme.tplStore, '$.scheme.tplStore');
          } catch(e){
            $.log('scheme.initTemplates() exception : ' + e.message + ' @ ' + e.lineNumber);
          }
        },
        
        setup: function(doc) {
          try {
            // $.log('scheme.setup() ' );
            $.scheme.widgetsByGuid = {};
            $.scheme.widgets = [];
            var widgets = Zepto('scheme widget', doc);
            for (var i=0; i<widgets.length; i++){
              var wgt = widgets[i],
                  widgetObj = $.scheme.setupWidget(wgt);
              if (widgetObj && widgetObj.guid && 'true'!==widgetObj.deleted && !(widgetObj.guid in $.scheme.widgetsByGuid)) {
                $.scheme.widgets.push(widgetObj);
                $.scheme.widgetsByGuid[widgetObj.guid] = widgetObj;
              }    
            }
          } catch(e) {
            $.log('scheme.setup() exception : ' + e.message + ' @ ' + e.lineNumber);                
          }
        },
        setupWidget: function(wgt) {
          try {
            // hack: XSL-transform of cloned or modified Node results a single TextNode
            var idoc = document.implementation.createDocument(null, null, null);
            var ichd = wgt.cloneNode(true);
            idoc.appendChild(ichd);
            idoc = $.sync.localizeImages(idoc);
            
            var informers = Zepto('informer', idoc);
            if (!informers.length)
              return;
              
            var widgetObj = null,
                informer = informers[0],
                control = (wgt.getAttribute('control') || '');
            control = control && $.scheme.tplStore[control]? control : '';
            if ($.scheme.isControlModular(control)){
              Zepto('menus', informer).each(function(index, item){
                Zepto(item).remove();
              }); 
            }

            var infoSet =  $.util.api.parser(informer),
                reload = informer? (informer.getAttribute('reload') || '0') : '0',
                options = [],
                optionElms = Zepto('options > option', wgt);
            if (Array.isArray(optionElms)) {
              optionElms.forEach( function(item){
                var oData = {};
                ['label','preset','prefname'].forEach(function(attrName){
                    oData[attrName] = item.getAttribute(attrName) || '';
                })
                options.push(oData)    
              })
            }
         
            var src = Zepto(Zepto('informer', wgt).get(0)).attr('src') || '';
            var uri = src.replace(/#?\{[\w\d:;\._-]+\}/gi, function(str){
              var param = Zepto('params > param[name='+str.replace(/(^#?\{|\}$)/g,'')+']', wgt); 
              var pref = ''; 
              if (param.length) {
                var one = Zepto(param[0]), 
                    defVal = one.attr('default') || '', 
                    lookup = one.attr('lookup') || '';
                pref = '#{'+lookup + ':' + defVal+'}';
              }
              return pref;
            });

            var tpls = Zepto('template', wgt),
                tplPopups = Zepto('templatepopup[namespace="XHTML"]', wgt),
                xslPopups = Zepto('templatepopup[namespace="xslt"]', wgt),
                tplItems = Zepto('templateitem', wgt),
                tpl = tpls.length? tpls[0] : null,
                tplItem = tplItems.length? tplItems[0] : null,
                tplPopup = xslPopups.length? xslPopups[0] : (tplPopups.length? tplPopups[0] : null),
                tNS = tplPopup? String(tplPopup.getAttribute('namespace')).toUpperCase() : '',
                tplPopupNS = tNS? (['XUL', 'XHTML', 'XSLT'].indexOf(tNS)>-1? tNS : 'XUL') : 'XUL',
                jxpopup = ('XSLT' != tplPopupNS && tplPopup)? $.util.jxt.dom2jxon( tplPopup ) : null,
                jxtpl = tpl? $.util.jxt.dom2jxon( tpl ) : null,
                jxitem = tplItem? $.util.jxt.dom2jxon( tplItem ) : null;

            if (tpl || control) {
              widgetObj = { 
                guid: (wgt.getAttribute('guid') || ''),
                control: control,
                informer: (informer || null),
                reload: parseInt(reload, 10),
                nfoBody : (infoSet.length? infoSet[0] : null),
                visible: (wgt.getAttribute('visible') || 'true'),
                deleted: (wgt.getAttribute('deleted') || 'false'),
                options: options,
                lastUpdated: 0,
                dataURI: uri,
                template: (jxtpl? jxtpl.nodeValue : null), 
                templateitem: (jxitem? jxitem.nodeValue : null),
                templatepopup: ('XSLT'==tplPopupNS && tplPopup) ? tplPopup : (jxpopup? jxpopup.nodeValue : null), 
                popupns: tplPopupNS                 
              };
            }
            
            return widgetObj;
            
          } catch (e) {
            $.log('setupWidget() exception: ' + e.message + ' @ ' + e.lineNumber);
          }
        },
        isRssData : function(dataSet) {
          return ('isRss' in dataSet);
        },
        isControlExist: function(control) {
          return !!control && !!$.scheme.tplStore[String(control)];
        },
        isControlBasic: function(control) {
          return (-1 < String(control).search(/^(?:search|button|menu|menu-button|rss)$/gi) ); 
        }, 
        isControlModular: function(control) {
          return $.scheme.isControlExist(control) && !$.scheme.isControlBasic(control);
        },
        isUpdateable: function(guid) {
          var dtNow = Date.now(),
              w = $.scheme.widgetsByGuid[ guid ] || null,
              aUnit = $.scheme.isControlModular(w.control) && $.unit.items[w.control] || null;
          var r = ( w && 'true'!=w.deleted && w.guid && w.reload>0 && ((dtNow-w.lastUpdated)/1000+5 > w.reload) && 
                 (!!w.dataURI || aUnit && -1 < aUnit.publicFuncs.indexOf('update')) );
          // $.log(' * isUpdateable( '+guid+' ) control: '+w.control+'; unit: '+aUnit + '; r: '+ r);
          return r;
        },
        makeTplSet: function(wgt) {
          try {
            var tplSet = {};
            ['dataURI','template','templateitem','templatepopup','popupns'].forEach(function(element){
                tplSet[element] = wgt[element]; 
            });
            if (!wgt.template && wgt.control && $.scheme.tplStore && $.scheme.tplStore[wgt.control]) {
              var defTpl = $.scheme.tplStore[wgt.control];
              for (var tname in defTpl) 
                tplSet[tname] = defTpl[tname];
            }
            return tplSet;
          } catch(e) {
            $.log('makeTplSet() exception: '+e.message+' @ '+e.lineNumber);
          }
        },
        getTplSet: function( elm ) {
          try {
            var tpls = Zepto('template', elm),
                tplItems = Zepto('templateitem', elm),
                tpl = (tpls.length? tpls[0] : null),
                tplItem = (tplItems.length? tplItems[0] : null),
                jxtpl = tpl? $.util.jxt.dom2jxon( tpl ) : null,
                jxitem = tplItem? $.util.jxt.dom2jxon( tplItem ) : null,
                tplSet = {
                  template: (jxtpl && jxtpl.nodeValue) || null, 
                  templateitem: (jxitem && jxitem.nodeValue) || null 
                };
            return tplSet;
          } catch (e) {
            $.log('getTplSet() exception: '+e.message+' @ '+e.lineNumber);
          }
        }, 
        updateWidget: function(guid) {
          try {
            if (!$.scheme.widgetsByGuid || !$.scheme.widgetsByGuid[guid] || 'true'==$.scheme.widgetsByGuid[guid].deleted) return;
            // $.log('scheme.updateWidget('+guid+')');
            var wList = Zepto('[guid="'+guid+'"]', $.appNode);
            if (!wList.length) {
                $.log('[guid="'+guid+'"] empty! calling scheme.update()');            
                return $.scheme.update(); 
            }
            var wgt = $.scheme.widgetsByGuid[guid]; 
            var dataSet = wgt.nfoBody? [wgt.nfoBody] : ($.util.api.parser( wgt.informer ));
            if (wgt.control && $.scheme.isControlModular(wgt.control)) 
              $.unit.cmd(wgt.control, 'update');
            else if (wgt.dataURI)
              $.scheme.fetch(wgt.dataURI, wgt.guid);
            else
              $.scheme.createWidget(dataSet, wgt.guid, wgt.informer);
          } catch(e) {
            $.log('scheme.updateWidget('+guid+') exception : ' + e.message + ' @ ' + e.lineNumber);
          }
        },
        update: function() {
          try {
            if (!$.scheme.widgets || !$.scheme.widgets.length) 
              return;
            // $.log('scheme.update()');
            var widgets = $.scheme.widgets;
            if (!widgets.length) 
              return;
            var wList = Zepto('[plugtype]', $.appNode);
            for (var i=0; i < wList.length; i++)
              (wList[i].parentNode).removeChild(wList[i]);
            for (var i=0; i<widgets.length; i++)
              $.scheme.runWidget(widgets[i]);
          } catch(e) {
            $.log('scheme.update() exception : ' + e.message + ' @ ' + e.lineNumber);
          }
        },
        
        runWidget: function(wgt) {
          try {
            if ('object'!= typeof wgt || 'true'==wgt.deleted || !wgt.guid) 
              return;
            // $.log('scheme.runWidget '+wgt.guid);  
            
            var theSpring = document.getElementById($.APPNAME+'-spring'),
                hostWgt = document.createElement('toolbaritem'),
                refElm = ('always-right'==wgt.visible? theSpring.nextSibling : theSpring);
            hostWgt.setAttribute('guid', wgt.guid);    
            hostWgt.setAttribute('visible', wgt.visible);
            hostWgt.setAttribute('plugtype', 'host');
            (theSpring.parentNode).insertBefore(hostWgt, refElm);
              
            var dataSet = wgt.nfoBody? [wgt.nfoBody] : ($.util.api.parser( wgt.informer ));
            $.scheme.createWidget(dataSet, wgt.guid, wgt.informer);
            
            if ($.scheme.isControlModular(wgt.control))
              $.unit.init(wgt.control, wgt.guid);
            else if (wgt.dataURI) 
              $.scheme.fetch(wgt.dataURI, wgt.guid);
            
              
          } catch(e) {
            $.log('scheme.runWidget() exception: ' + e.message + ' @ ' + e.lineNumber);
          }
        },
        
        createWidget: function(dataSet, guid, dataXML) {
          try {
              var wgt = $.scheme.widgetsByGuid[guid];
              var xchKey, 
                  tplItemFake = false;
              var tplSet = $.scheme.makeTplSet(wgt);
              if (Array.isArray(dataSet) && dataSet.length>1 && !tplSet.templateitem) {
                tplSet.templateitem = $.util.unlink(tplSet.template);
                tplSet.template = "#{_}";
                tplItemFake = true;
              }
              var isRss = $.scheme.isRssData(dataSet),
                  data = isRss? dataSet : (tplSet.templateitem? (tplItemFake? {list:dataSet} : dataSet[0]) : dataSet[0]);
              if (isRss && guid) {
                data.image = (wgt && wgt.nfoBody)? (wgt.nfoBody.image || '') : '';
                data.text = (wgt && wgt.nfoBody)? (wgt.nfoBody.text || '') : '';
                data.tooltip = (wgt && wgt.nfoBody)? (wgt.nfoBody.tooltip || data.tooltip || '') : (data.tooltip || '');
              }
              
              if (tplSet['templatepopup'] && 'XSLT'==tplSet['popupns'] && (xchKey=$.getCfg('xchWProp', null))) {
                var oParser = new DOMParser();
                var srcXsl = tplSet.templatepopup.textContent || null,
                    docXsl = srcXsl? oParser.parseFromString(srcXsl, "text/xml") : null,
                    docXml = dataXML instanceof Node? dataXML : null;
                if (!docXsl || !docXml) {
                  $.log('XSLT: (!docXsl || !docXml)');
                  return;
                }
                if('object' !== typeof $[xchKey])
                  $[xchKey] = {};
                var xR = $.sync.transformXML(docXml, docXsl);
                if (xR){
                  $[xchKey][guid || 'defValue'] = {
                    docFrag  : xR,
                    docTitle : data.title || data.text || (wgt.nfoBody?(wgt.nfoBody.title || wgt.nfoBody.text) : '')
                  }                
                }
              } else if (tplSet.templatepopup && (xchKey=$.getCfg('xchWProp', null))) {
                if('object' !== typeof $[xchKey])
                  $[xchKey] = {};
                    
                $[xchKey][guid || 'defValue'] = {
                  docFrag  : $.util.jxt.render({'template':tplSet.templatepopup}, data, 'XHTML'),
                  docTitle : data.title || data.text || (wgt.nfoBody?(wgt.nfoBody.title || wgt.nfoBody.text) : '')
                }
              } 
              var dF = $.util.jxt.render(tplSet, data);
              if (data.menus) {
                Zepto('toolbarbutton', dF).append(data.menus);
              }
              
              var hosts = guid? Zepto('[guid="'+guid+'"]', $.appNode) : [];
              var host = hosts.length? hosts[0] : null;
              var aParent = host.parentNode || null;
              var nextHost = hosts[hosts.length-1].nextSibling;
              var aVisible = host.getAttribute('visible');
              var visible = (-1 < ['false','true','always','always-right'].indexOf(aVisible))? aVisible : 'true';
              var dfC = dF.childNodes; 
              
              if (host && host.getAttribute('plugtype')=='host') {
                for (var f=0; f < dfC.length; f++) {
                  dfC[f].setAttribute('plugtype', 'widget');
                  dfC[f].setAttribute('guid', guid);
                  dfC[f].setAttribute('visible', visible);
                  dfC[f].setAttribute('hidden', (('false'==visible)? 'true' : 'false'));
                }
                aParent.insertBefore(dF, nextHost);
                for (var i=0; i < hosts.length; i++)
                  aParent && aParent.removeChild(hosts[i]); 
                
              } else {
              
                for (var g=0; g < Math.max(hosts.length, dfC.length); g++) {
                  if(hosts[g] && dfC[g]) {
                    if (dfC[g].hasAttributes()) {
                      for (var iLength=0; iLength < dfC[g].attributes.length; iLength++) {
                        var iAttr = dfC[g].attributes.item(iLength);
                        hosts[g].setAttribute(iAttr.nodeName, iAttr.value);
                      }
                    }
                    while (hosts[g].firstChild) 
                      hosts[g].removeChild(hosts[g].firstChild); 
                      
                    if (dfC[g].hasChildNodes()) {
                      var iChild;
                      for (var iChildId = 0; iChildId < dfC[g].childNodes.length; iChildId++) {
                        iChild = dfC[g].childNodes.item(iChildId);
                        if (iChild) hosts[g].appendChild(iChild);
                      }
                    }    
                  } else if (dfC[g]) {
                    dfC[g].setAttribute('plugtype', 'widget');
                    dfC[g].setAttribute('guid', guid);
                    dfC[g].setAttribute('visible', visible);
                    dfC[g].setAttribute('hidden', (('false'==visible)? 'true' : 'false'));
                    
                    var injectElm = dfC[g].cloneNode(true);
                    aParent.insertBefore(injectElm, nextHost);
                    
                  } else if (hosts[g]) {
                    (hosts[g].parentNode).removeChild(hosts[g]);
                  }
                }
              }
              
          } catch(e) {
            $.log(' * createWidget() exception: ' + e.message + ' @ ' + e.lineNumber);
            // $.loggo(dataSet, ' excp, guid:{' + guid + '} ; dataSet');
            // $.loggo(tplSet, ' excp, guid:{' + guid + '} ; tplSet');
          }
        },
        addWidget: function(uri) {
          try {
            if (!$.util.xhr) {
              var cfg = $.util.getArgs({ 
                uri: uri, 
                format: 'xml',
                error: (function(){
                  $.log('scheme.addWidget() ajax error; uri: ' + uri);
                  $.util.xhr = null;
                }),
                success: (function(doc){
                  // $.log('scheme.addWidget() ajax success; uri: ' + aURI.spec);
                  $.scheme.addFetchedWidget(doc);
                  $.util.xhr = null;
                })
              });
              
              $.util.xhr = Zepto.ajax( cfg ); 
              setTimeout(function(){
                if ($.util.xhr && 4 != $.util.xhr.readyState && 'function'==typeof $.util.xhr.abort){ $.util.xhr.abort(); }
              }, 3000);
            }

          } catch(e) {
            $.log('addWidget() exception: '+e.message+' @ '+e.lineNumber);
          }
        },
        addFetchedWidget: function(doc) {
          try {
            var wScm = Zepto('scheme', doc),
                wSrc = Zepto('widget', doc);
            if (!wScm.length || !wSrc.length) return;
            
            var scm = wScm[0],
                wgt = wSrc[0],
                scmv = scm? (scm.getAttribute('version') || 0) : 0,
                guid = wgt? (wgt.getAttribute('guid') || null) : null;
            // $.log('scheme.addFetchedWidget() w.guid: ' + guid);
            if (!guid) return;

            scmv = parseInt(scmv, 10);
            var widgetObj = $.scheme.setupWidget(wgt);
            if (!widgetObj || scmv > $.config.schemeVersion) {
              var message = $.util.getMsg('widgetVersionExceeded', 'Widget version exceeded: application requires an update.');
              var title = $.util.getMsg('widgetAddTitle', 'Widget Setup');
              return $.promptSvc.alert(null, title, message);
            }

            var mk = false;
                cG = Zepto('[guid="'+guid+'"]', $.scheme.cfgDoc);
            if (cG.length) {
              mk = ('true' == (cG[0].getAttribute('deleted') || 'false'));
              for (var i=0; mk && (i < cG.length); i++)
                (cG[i].parentNode).removeChild(cG[i]);
            } else { mk = true; }
            
            if (!mk) {
              var message = $.util.getMsg('widgetAlreadyInstalled', 'You have this widget already installed');
              var title = $.util.getMsg('widgetAddTitle', 'Widget Setup');
              return $.promptSvc.alert(null, title, message);
            }
            
            var sM = Zepto('scheme', $.scheme.cfgDoc);
            if (sM.length)
              sM[0].appendChild(wgt); 
            
            $.scheme.setup($.scheme.cfgDoc);
            $.scheme.saveConfig();
            $.scheme.runWidget(widgetObj);
            $.sync.notifyGallery(false, [guid]); 
            
            if ($.util.isGhost()){
              $.appNode.collapsed = false;
              $.sync.setRBarCookie();
            }  
            
          } catch(e) {
            $.log('scheme.addFetchedWidget() exception: ' + e.message + ' @ ' + e.lineNumber);
          }  
        },
        moveWidget: function(actorGuid, targetGuid, where) {
          try {
            $.scheme.rearrangeWidgets(actorGuid, targetGuid, where, $.appNode);
            $.scheme.rearrangeWidgets(actorGuid, targetGuid, where, $.scheme.cfgDoc);
            
            $.scheme.setup($.scheme.cfgDoc);
            $.scheme.saveConfig();
          } catch(e) {
            $.log('scheme.moveWidget() exception: ' + e.message + ' @ ' + e.lineNumber);
          }  
        },
        removeWidget: function(guid) {
          try {
            $.log('scheme.removeWidget('+guid+')');
            var wList = Zepto('[guid="'+guid+'"]', $.appNode);
            for (var i=0; i < wList.length; i++)
              (wList[i].parentNode).removeChild(wList[i]);
                
            Zepto('[guid="'+guid+'"]', $.scheme.cfgDoc).forEach(function(item){
              item.setAttribute('deleted', 'true');
            });
              
            var wgt = $.scheme.widgetsByGuid[guid]; 
            if (wgt && wgt.control && $.scheme.isControlModular(wgt.control))
              $.unit.quit(wgt.control);
            
            $.scheme.setup($.scheme.cfgDoc);
            $.scheme.saveConfig();
            
            $.sync.notifyGallery(true, [guid]);
               
          } catch(e) {
            $.log('scheme.removeWidget('+guid+') exception : ' + e.message + ' @ ' + e.lineNumber);
          }
        },
        
        rearrangeWidgets: function(actorGuid, targetGuid, where, hostNode) {
            try {
              // $.log('rearrangeCfgDoc() actor: ' + actorGuid +' , target: ' + targetGuid + ', where: ' + where);
              where = ('string' == typeof where? (-1==['before','after'].indexOf(where)? 'before' : where ) : 'before' );
              
              var actorList = Zepto('[guid="'+actorGuid+'"]', hostNode),
                  targetList = Zepto('[guid="'+targetGuid+'"]', hostNode),
                  actorClones = [], 
                  lastInserted;
              if (!targetList.length || !actorList.length)
                return;
              
              actorList.forEach(function(item){
                actorClones.push(item.cloneNode(true));
              });
              var anchor = ('before' == where)? targetList[0] : (targetList[targetList.length-1]);
              actorClones.forEach(function(aClone){
                lastInserted = (anchor.parentNode).insertBefore(aClone, ('before'==where ? anchor : (lastInserted || anchor).nextSibling));
              });
              actorList.forEach(function(actor){
                actor.parentNode && actor.parentNode.removeChild(actor);
              });     
              
              return hostNode;
            
            } catch(e) {
              $.log(' * rearrangeCfgDoc() exception: ' + e.message + ' @ ' + e.lineNumber );
            }
            
        },
        
        fetch: function(dataURI, guid) {
            dataURI = dataURI.replace(/#?\{[\w\d:;\.,_-]+\}/gi, function(str){
                var prefPkg = str.replace(/(^#?\{|\}$)/g,'').split(':');
                var lookup = (prefPkg.length? prefPkg[0] : '');
                return (lookup? $.prefMgr.get(lookup, '') : (prefPkg[1] || ''));
            });
            var xhrArgs = {
                url: dataURI, 
                dataType: 'xml', 
                headers  : $.util.getHttpHeaders(), // 'xml'
                error    : function(xhr,s,err){
                    try {
                        var wgt = $.scheme.widgetsByGuid[guid] || null;
                        if (!wgt) return;
                        if (401 == xhr.status && xhr.responseXML) {
                            var dataSet = $.util.api.parser(xhr.responseXML);
                            if (!dataSet) return;
                            wgt.lastUpdated = Date.now();
                            $.scheme.createWidget(dataSet, guid, xhr.responseXML);
                        } else {
                            var dataDef = wgt.nfoBody? [wgt.nfoBody] : ($.util.api.parser( wgt.informer ));                        
                            $.scheme.createWidget(dataDef, guid, wgt.informer);
                        } 
                    } catch(e) {
                      $.log('scheme.fetch() xhr.error() exception: ' + e.message + ' @scheme.js:' +e.lineNumber +'; dataURI:'+dataURI + '; http-status: ' + xhr.status);
                    }
                },
                success : function(result,s,xhr){
                    try {
                        var wgt = $.scheme.widgetsByGuid[guid] || null;
                        if (!wgt) return;
                          
                        var r;
                        if (!xhr.responseXML){
                          r = (new DOMParser()).parseFromString(xhr.responseText, 'application/xml');
                          if ('parsererror' == r.documentElement.nodeName) 
                            return;
                        } else {
                          r = xhr.responseXML;
                        }  
                          
                        var rssE = Zepto('rss', r),
                            parser = (rssE && rssE.length)? 'rss' : 'parser';
                        if ('rss'==parser) {
                          var ioService = Components.classes['@mozilla.org/network/io-service;1']
                                                             .getService(Components.interfaces.nsIIOService);
                          var feedUrl = ioService.newURI(dataURI, null, null);
                          $.util.api.processFeed(xhr.responseText, feedUrl, wgt);
                          return;
                        }
                        var dataSet = $.util.api[ parser ]( r );
                        if (dataSet) {
                            wgt.lastUpdated = Date.now();
                            $.scheme.createWidget(dataSet, guid, r);
                        } else {
                            var dataDef = wgt.nfoBody? [wgt.nfoBody] : ($.util.api.parser( wgt.informer ));
                            $.scheme.createWidget(dataDef, guid, wgt.informer);
                        }
                    } catch(e) {
                      $.log('scheme.fetch() xhr.success() exception: ' + e.message + ' @ ' +e.lineNumber);
                    }
                }
            };
            
            Zepto.ajax( xhrArgs );
        
        },
        getWidgetElement: function(guid) {
          try {
            var wList = Zepto('[guid="'+guid+'"]', $.appNode);
            return (wList.length ? wList[wList.length-1] : null);
          } catch (e) {
            $.log(' (!) getWidgetElement() exception: ' + e.message + ' @ ' + e.lineNumber);
          }
        },
        getGuids: function() {
          try {
            var awg = [];
            for (var j=0; j<$.scheme.widgets.length; j++) {
              var wgt=$.scheme.widgets[j];
              if ('true'!=wgt.deleted && wgt.guid) 
                awg.push(wgt.guid);
            }
            return awg;
            
          } catch(e) {
            $.log('scheme.getGuids() exception: '+e.message+' @ '+e.lineNumber);
          }
        },
        saveConfig: function(doc) {
          try {
            var xmlDoc = doc || $.scheme.cfgDoc;
            var cfgTxt = $.xmlSerializer.serializeToString(xmlDoc); 
            cfgTxt = cfgTxt.replace(/[ ]+[\r\n]+/mg,'\n').replace(/[\r\n]+/mg,'\n').replace(/^\s+|\s+$/g, '');            
            var cfgFile = $.sync.checkFileCfg($.config.cfgLocalPath);
            $.sync.writeFile(cfgFile, cfgTxt);
            // $.log(' * scheme.saveConfig() ==================================================== \n' + cfgTxt);
          } catch(e) {
            $.log('scheme.saveConfig() exception: ' + e.message + ' @ ' +e.lineNumber);
          }
        }
        
    };
    
    $.scheme = scheme;
    
})( this['rambler_plugbar'] );
