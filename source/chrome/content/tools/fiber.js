/*
http://nin-jin.github.com/article/article_fiber/article_fiber.xml
*/

/*
let Confirm=
$fenix.FiberThread( function( message ){
    let choice= confirm( message )
    return choice
} )
Однако, FiberThread может принимать не только функцию, но и фабрику генераторов. Функция становится фабрикой генераторов, если в ней используется оператор yield.

let AskUser=
$fenix.FiberThread( function( message ){
    let choice= yield Confirm( message )
    alert( 'Your choice is ' + choice ) 
} )

*/

let AskUser=
$fenix.FiberThread( function( message ){
    try {
        var choice= yield Confirm( message )
    } catch( exception ){
        console.log( exception )
        choice= true
    }
    alert( 'Your choice is ' + choice ) 
} )

let Confirm=
$fenix.FiberThread( function( message ){

    let result= $fenix.FiberTrigger()

    formConfirm.elements.question.value= message
    formConfirm.elements.yes.onclick= result.done
    formConfirm.elements.no.onclick= result.done

    let[ event ]= yield result
    yield $fenix.FiberValue( event.target.value )

} )