var FB_APP_ID = 219468064849803,
    OAuthRedirectURI = 'https://www.facebook.com/connect/login_success.html',
    OAuthRedirectURITest = /facebook\.com\/connect\/login_success\.html/i,
    OAuthPermissions = [""
    , "manage_notifications"
    , "offline_access"
    , "read_insights"
    , "read_mailbox"
    , "read_requests"
    , "read_stream"
    , "user_activities"
    , "user_status"
    ].join(','),
    OAuthWindowFeatures = [
    "centerscreen",
    "resizable",
    "width=640",
    "height=480"
    ].join(",");

var fba = {
  logSvc : Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService),
  xmlSerializer: new XMLSerializer(),
  
  log: function(text) { fba.logSvc.logStringMessage(text); },
  loggo: function(obj, cmt, echotypes) {
    try {
      var is = [];
      var types = ['String', 'Number', 'Boolean', 'Error', 'Date', 'Function', 'RegExp', 'Object', 'Array', 
          'HTMLCollection', 'NodeList', 'DocumentFragment', 'Document', 'Node', 'Element', 'XULElement', 'Attr', 'CharacterData', 
          'ProcessingInstruction', 'DocumentType' ] ;
      for (var t=0; t<types.length; t++) 
        if ( obj instanceof window[ types[t] ] ) is.push( types[t] );
         
      var val = (-1 != ['Node', 'Document', 'DocumentFragment', 'Element', 'XULElement'].indexOf(is[is.length-1]) )?
      (fba.xmlSerializer.serializeToString(obj)) : JSON.stringify(obj);
      fba.logSvc.logStringMessage((cmt? cmt+' : ' : '') + val + (echotypes? '. * types: {'+is.join(',')+'}' : '') );
    } catch(e) { fba.log(' !!! ' + types[t] + ' : ' + e.message + '@'+e.lineNumber); }
  },
  __fn: function fba__fn() {
    try {
      this.log(' * ' + arguments.callee.name + '()');
    } catch (e) {
      this.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber + '.\nStack trace: ' + e.stack);
    }
  },
  
  _credentials: {},
  
  _setCredentials: function fba__setCredentials(credentials) {
    try {
      fba.log(' * ' + arguments.callee.name + '() access_token: {' + credentials.access_token + '}');
      fba.loggo(credentials, 'credentials');
      fba._credentials = credentials || {};
      if (fba.connected()) {
        fba.whatsnew();
      }  
    } catch (e) {
      this.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber);
    }
  },  
  
  init: function fba__init(){
    try {
      this.log(' * ' + arguments.callee.name + '() ');
      this.connect();
    } catch (e) {
      this.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber);
    }
  },  
  
  connect: function fba__connect() {
    try {
      this.log(' * ' + arguments.callee.name + '() encodeURIComponent(OAuthRedirectURI):'+encodeURIComponent(OAuthRedirectURI));
      
      var uri = 'https://www.facebook.com/dialog/oauth?client_id=' + FB_APP_ID + 
          '&redirect_uri=' + encodeURIComponent(OAuthRedirectURI) + '&scope=' + OAuthPermissions + '&response_type=token';

      var xhrArgs = {
          url      : uri,
          error    : (function(xhr, aCase, error){
            fba.log('fba login, xhr error() '+aCase+'; error: '+ error + '; xhr: ' + xhr);
            return fba.login();
          }),
          success  : (function(txr, aCase, xhr) {
            var uri = xhr.channel.URI.spec;  
            var url = fba.sync.makeURI(uri);
            if (OAuthRedirectURITest.test(url.spec)) {
              var ref = fba.utils.urlParams2obj(url.ref);
              if ("access_token" in ref) {
                return fba._setCredentials(ref);
              }
            }   
            return fba.login();
          })
      };    
      Zepto.ajax( xhrArgs ); 

    } catch (e)  {
      this.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber);
    }
  },
  
  connected: function fba__connnected() {
    return !!this._credentials.access_token;
  },  
  
  // me/notifications
  whatsnew: function fba__whatsnew() {
    try {
      fba.log(' * ' + arguments.callee.name + '()');
      if (!fba.connected()) {
        return fba.log('not connected.');
      }
      var uri = 'https://graph.facebook.com/me/notifications?method=GET&format=json&limit=3&include_read=true'+
                '&access_token='+fba._credentials.access_token;
                
      var xhrArgs = {
          url      : uri,
          error    : (function(xhr, aCase, error){
            fba.log('fba whatsnew, xhr error() '+aCase+'; error: '+ error + '; xhr: ' + xhr);
            return fba.login();
          }),
          success  : (function(txt, aCase, xhr) {
            fba.log('fba whatsnew CB txt: ' + txt);
            var jso = null;
            try { 
              jso = JSON.parse(txt);
            } catch (e) {
              fba.log(' (!) JSON.parse error ' + arguments.callee.name+'() : ' + e.message + ' @ ' + e.lineNumber + '.\nStack trace: ' + e.stack);
            }
            if (jso && 'object'== typeof jso) {
              if (jso.data && jso.data.length) {
                var item = jso.data[0];
                fba.tray.popup('От: ' + (item.from && item.from.name || ''), (item.title || 'неизвестное событие'));
              }  
            }
          })
      };    
      Zepto.ajax( xhrArgs );
      
    } catch (e) {
      fba.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber + '.\nStack trace: ' + e.stack);
    }
  },
  
  login: function fba_login() {
    try {
      var fn = arguments.callee.name;
      fba.log(' * ' + arguments.callee.name + '()');
      
      var callbacks = new fba.utils.CallbackObject(
        function () {
          fba.log(fn+'(): connected Ok');
          // fba.saveCredentials();
          // fba.openFacebookPage("main");
        },
        function (error) {
          fba.log("fba.connect(): not connected");
        }
      );
      
      fba.processLogin(callbacks);      
      
    } catch (e) {
      this.log(' (!) ' + arguments.callee.name+'() exception: ' + e.message + ' @ ' + e.lineNumber);
    }
  },
  
  processLogin: function fba__logUserIn(callbacks) {
    var $ = fba;
    var OAuthWindow = null;
    
    var oauthLocationListener = fba.utils.createLocationListener(
      function (callbacks, aWebprogress, aRequest, aLocation) {
        var spec = aLocation.spec;

        fba.log(spec);
        
        try {
          var url = aLocation.QueryInterface(Components.interfaces.nsIURL);
        } catch (e) {
          return;
        }
        
        if (OAuthRedirectURITest.test(spec)) {
          var param = fba.utils.urlParams2obj(url.query);
          
          if ("error" in param) {
            return callbacks.error(param); // TODO something
          }
          
          var ref = fba.utils.urlParams2obj(url.ref);
          
          if ("access_token" in ref) {
            return callbacks.success(ref);
          }
          
          callbacks.error("PARSE_ERROR");
        }
      }
    );
    
    var authCB = new fba.utils.CallbackObject(
      function (credentials) {
        OAuthWindow.close();
        fba._setCredentials(credentials);
        callbacks.success();
      },
      
      function (e) {
        OAuthWindow.close();
        callbacks.error(e);
      }
    );
    
    var oauthParams = {
      "client_id": FB_APP_ID, 
      "redirect_uri": OAuthRedirectURI, 
      "type": "user_agent",
      "display" : "popup",
      "scope": OAuthPermissions
    };
    
    var OAuthWindowArguments = {
      url: 'https://graph.facebook.com/oauth/authorize?' + fba.utils.obj2UrlParams(oauthParams),
      progressListener: new oauthLocationListener(authCB),
      notifyMask: Components.interfaces.nsIWebProgress.NOTIFY_LOCATION,				
      onclose: function () {
        callbacks.error("CANCELLED");
      }
    };
    
    OAuthWindow = fba.ui.openMicrobrowser(OAuthWindowFeatures, OAuthWindowArguments);
    // OAuthWindow = fba.wndOpen('https://graph.facebook.com/oauth/authorize?' + fba.utils.obj2UrlParams(oauthParams));
  }  

}    


    
