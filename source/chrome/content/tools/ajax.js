  // simple ajax example
  var cfg = {
    url      : $.util.camp.getAppEventURI('sethome'),
    nocache  : true, 
    dataType : 'xml', // needs for getting an XML Document as a result
    headers  : $.util.getHttpHeaders(), // 'xml' 
    error    : function(){},
    success  : function(r,s,xhr){
      $.log(' * notify() xhr.success()');
    }
  };
  var xhr = Zepto.ajax( cfg );  
  setTimeout(function(){
    if (4 != xhr.readyState && 'function'==typeof xhr.abort){ xhr.abort(); }
  }, 3000);     
  
  
  
  // example 2              
  var xhrArgs = {
    type     : 'POST', 
    url      : 'http://informers.rambler.ru/logout/',
    data     : { version  : 4 },
    headers  : $.util.getHttpHeaders('xml'),
    dataType : 'xml', 
    error    : (function(xhr, aCase, error){}),
    success  : (function(docR, aCase, xhr) {})
  }    
  Zepto.ajax( xhrArgs ); 
                 
           
            