var app = {
  logSvc : Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService),
  xmlSerializer: new XMLSerializer(),
  log: function(){
    try {
      var args = Array.prototype.slice.call(arguments);
      if (!args.length) 
        return;
      this.logSvc.logStringMessage((1 < args.length)? this.dumpObject.apply(this, args) : args[0]);
    } catch (e) {
      this.logSvc.logStringMessage(' (!) this.log() exception: ' + e.message + ' @' + e.lineNumber);
    }
  },
  bind: function(fn, context, params) {
    return function(){
      fn.apply(context, params);
    };
  },   
  
  utf8_to_b64 : function( str ) {
      return window.btoa(unescape(encodeURIComponent( str )));
  },
  b64_to_utf8: function( str ) {
      return decodeURIComponent(escape(window.atob( str )));
  },

  dumpObject: function app_dumpObject(obj, cmt, echotypes){
    try {
      var is = [];
      var types = ['String', 'Number', 'Boolean', 'Error', 'Date', 'Function', 'RegExp', 'Object', 'Array', 
        'HTMLDocument', 'HTMLCollection', 'NodeList', 'DocumentFragment', 'Document', 'Node', 'Element', 'XULElement', 'Attr', 'CharacterData', 
        'ProcessingInstruction', 'DocumentType' ];
      for (var t=0; t<types.length; t++) 
        if ( obj instanceof window[ types[t] ] ) is.push( types[t] );
      var val = (-1 != ['Node', 'Document', 'DocumentFragment', 'Element', 'XULElement'].indexOf(is[is.length-1]) )?
      (this.xmlSerializer.serializeToString(obj)) : JSON.stringify(obj);
      return (cmt? cmt+' :\n' : '') + val + (echotypes? '. * types: {'+is.join(',')+'}' : '');
    } catch(e) { return (' (!) '+arguments.callee.name+'() exception: ' +types[t]+ ' : ' +e.message+ '@' +e.lineNumber); }
  },
  copy: function(o){
    var copy = Object.create( Object.getPrototypeOf(o) );
    var propNames = Object.getOwnPropertyNames(o);
   
    propNames.forEach(function(name){
      var desc = Object.getOwnPropertyDescriptor(o, name);
      Object.defineProperty(copy, name, desc);
    });
   
    return copy;
  },  
  
  math: {
    dec2hex: function(dec) { return Number(dec).toString(16) },
    hex2dec: function(hex) { return parseInt(hex, 16); },
    
    // a>b => 1, a==b => 0, a<b => -1; a, b: '1.0.3', '0.9', etc.
    compareVersions: function(a, b) {
      var v1 = a.split('.')
        , v2 = b.split('.');
      for (var i = 0; i < Math.min(v1.length, v2.length); i++){
        var r = v1[i] - v2[i];
        if (0 !== r)
          return r > 0? 1 : -1;
      }
      return 0;
    }    
  },
  except: function(e, callerName){
    this.log(' (!) ' + callerName + '() exception: ' + e.message + ' @' + e.lineNumber + (e.stack? ('; stack: \n' + e.stack):'') ); 
  },
  attempt: function(fn, context, params, fname){
    try {
      if ('function'==typeof fn) 
        return fn.apply(context || {}, params || []);
    } catch(e) {
      this.except(e, '$.attempt() :: ' + (fn.name || fname || 'anonymous')+'() from '+(arguments.callee.caller.name || 'anonymous')+'()');
    }
  }
};