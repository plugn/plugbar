(function($){

    var JXT = {

        xmlNS : {
            'XUL'   : 'http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul',
            'XHTML' : 'http://www.w3.org/1999/xhtml'
        },
        keyNS : 'XUL', // 'XHTML'
        nodeAppend: function ( node, hostNode ) {
            try {
                var elms = node.documentElement.childNodes; // with textNodes, via DOM
                for (var i = 0; i < elms.length; i++) {
                    var item = elms[i];  
                    if ('#text' == item.nodeName) {  continue; }
                    hostNode.appendChild(hostNode.ownerDocument.importNode(elms[i], true));
                }
            } catch (e) {
                ramblerTpl.log( ' nodeAppend() exception: ' + e.message );
            }
        },
                
        parse : function(tplHtml, dataHash) {
            var html = tplHtml;
            for (var key in dataHash) {
                html = html.replace( (new RegExp('#\\{' + key + '\\}','g')), dataHash[key]);
            }
            return html;
        },                
        buildValue : function(sValue) {
            if (/^\s*$/.test(sValue)) { return(null); }
            if (/^(true|false)$/i.test(sValue)) { return(sValue.toLowerCase() === "true"); }
            if (isFinite(sValue)) { return(parseFloat(sValue)); }
            if (isFinite(Date.parse(sValue))) { return(new Date(sValue)); }
            return(sValue);
        },
        jxon2dom : function( jxon, ctx ) {
            try {
                if ('undefined' == typeof ctx || !ctx) 
                    ctx = document.createDocumentFragment(); //document.implementation.createDocument("", "", null);
                
                var objType, nodeName, nodeAttrs; 
                
                if ('string' == (typeof jxon).toLowerCase()) {
                    objType = 'string';
                } else if ('object' == (typeof jxon).toLowerCase()) {
                    objType = 'object';
                    if (jxon.constructor == Array) 
                        objType = 'array';
                }
                // ramblerTpl.log( ' objType : ' + objType);
                
                switch (objType) {
                case 'string':
                    // var rText = this.parse( jxon, this.data );
                    // var textNode = document.createTextNode( rText );
                    var textNode = document.createTextNode(jxon);
                    ctx.appendChild(textNode);                
                    
                    break;
                case 'object':
                    jxon.nodeValue = jxon.hasOwnProperty('nodeValue')? jxon.nodeValue : null;
                    for (var k in jxon) 
                        if ('nodeValue' != k) { 
                            nodeName  = k; 
                            nodeAttrs = jxon[ k ];
                        }
                    // var elm = document.createElement(nodeName);
                    var elm = document.createElementNS(this.xmlNS[this.keyNS], nodeName);
                    
                    for (var a in nodeAttrs) {
                        // var rAttr = this.parse( nodeAttrs[ a ], this.data );
                        var rAttr = nodeAttrs[a];
                        elm.setAttribute(a, rAttr);
                    }
                        
                    if (jxon.nodeValue) {
                        elm = ramblerTpl.JXT.jxon2dom(jxon.nodeValue, elm);
                    }
                        
                    ctx.appendChild( elm );
                    
                    break;
                case 'array':
                    var node;
                    for (var i=0; i<jxon.length; i++) {
                        node = jxon[ i ];
                        // ramblerTpl.log(' case array # '+i+' : ' + typeof node );
                        ctx = ramblerTpl.JXT.jxon2dom(jxon[ i ], ctx);
                    }
                    
                    break;            
                }
                
                return ctx;
                
            } catch (e) {
                ramblerTpl.log('jxon2dom() exception: ' + e.message);
            }    
            // ramblerTpl.log( ' ; ctx: ' + ctx + ' has: '+ ctx.hasOwnProperty('doctype') + ' = ' + ctx.doctype  );
        },


        dom2jxon: function(oXML) {
            try {
                // ramblerTpl.log('JXT.dom2jxon() nodeName : ' + oXML.nodeName + ' nodeType: ' + oXML.nodeType);
                var rElm = {}, sTxtContent = '';

                switch(oXML.nodeType) {
                
                case Node.DOCUMENT_NODE:
                case Node.DOCUMENT_FRAGMENT_NODE:
                    if (oXML.hasChildNodes()) {
                        var iXMLChild, oChild, oChilds = [], oValue;
                        for (var iChildId = 0; iChildId < oXML.childNodes.length; iChildId++) {
                            iXMLChild = oXML.childNodes.item(iChildId);
                            oChild = ramblerTpl.JXT.dom2jxon(iXMLChild);
                            if (null !== oChild) 
                                oChilds.push(oChild)
                        }
                        if (oChilds.length == 1) 
                            oValue = oChilds[0];
                        else if (oChilds.length > 0) 
                            oValue = oChilds;
                    }
                    return oValue;
                    
                    break;
                case Node.ELEMENT_NODE:
                
                    var attrs = {};
                    if (oXML.hasAttributes()) {
                        for (var nLength=0; nLength < oXML.attributes.length; nLength++) {
                            var iAttr = oXML.attributes.item(nLength);
                            var aKey = iAttr.nodeName.toLowerCase();
                            var aValue = ramblerTpl.JXT.buildValue(iAttr.nodeValue.replace(/^\s+|\s+$/g, ""));
                            attrs[aKey] = aValue;
                        }
                    }
                    // ramblerTpl.log('attributes: ' + JSON.stringify(attrs));
                    
                    rElm[oXML.nodeName] = attrs;
                    // rElm.nodeValue = null;
                    
                    if (oXML.hasChildNodes()) {
                        var iXMLChild, oChild, oChilds = [];
                        for (var iChildId = 0; iChildId < oXML.childNodes.length; iChildId++) {
                            iXMLChild = oXML.childNodes.item(iChildId);
                            oChild = ramblerTpl.JXT.dom2jxon(iXMLChild);
                            if (null !== oChild) 
                                oChilds.push(oChild)
                        }
                        if (oChilds.length == 1) 
                            rElm.nodeValue = oChilds[0];
                        else if (oChilds.length > 0) 
                            rElm.nodeValue = oChilds;
                    }
                    return rElm;
                    
                    break;
                case Node.TEXT_NODE: // 3
                    sTxtContent += oXML.nodeValue.replace(/^\s+|\s+$/g, ""); 
                    return ramblerTpl.JXT.buildValue(sTxtContent);
                
                    break;
                case Node.CDATA_SECTION_NODE: // 4
                    sTxtContent += oXML.nodeValue; 
                    return ramblerTpl.JXT.buildValue(sTxtContent);
                
                    break;
                default: 
                    return null;
                }
            } catch(e) {
                ramblerTpl.log('JXT.dom2jxon() exception : ' + e.message);
            }
        }
    }
    
    $.JXT = JXT;
    
})(ramblerTpl);
