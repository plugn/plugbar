(function($){
  var cookie = {
    removeCookie: function(name, host) {
      try {
        host = String(host || '.rambler.ru');
        if ('.' !== String(host).charAt(0)) 
          host = '.' + host;
        // $.log('cookie.remove() ' + name + '@' + host);
        var cookieManager = Components.classes["@mozilla.org/cookiemanager;1"].getService(Components.interfaces.nsICookieManager2);
        // var iter = cookieManager.enumerator;
        var iter = cookieManager.getCookiesFromHost(host);            
        while (iter.hasMoreElements()) {
          var cookie = iter.getNext();
          if (cookie instanceof Components.interfaces.nsICookie) {
            if (name===cookie.name && host.indexOf(cookie.host.toLowerCase()) != -1) {
              cookieManager.remove(cookie.host, cookie.name, cookie.path, cookie.blocked);
            }
          }
        }
        cookieManager.remove(cookie.host, cookie.name, cookie.path, false);
      
      } catch (e) {
        $.log('cookie.remove() exception + ' + e.message);
      }
    },      
    setCookie: function(name, value, props) {
        try {
            $.log(props, 'util.setCookie.setValue('+name+', '+ value+')');            
            if (!name || ''==name) { return; }
            props = props || {};
            var cookieCfg = {};
            cookieCfg[name] = (value?  value  : "");
            cookieCfg.domain = (props.domain? props.domain : ".rambler.ru");
            cookieCfg.path =   (props.path? props.path : "/");
            
            var exp = props.expires;
            
            if (typeof exp == "number" && exp) {
                var d = new Date();
                d.setTime(d.getTime() + exp*1000);
                exp = props.expires = d;
            }
            if (exp && exp.toUTCString) { props.expires = exp.toUTCString(); }
            if (props.expires) {
                cookieCfg.expires = props.expires;
            }
               
            var cookieURI = Components.classes["@mozilla.org/network/standard-url;1"].createInstance(Components.interfaces.nsIURI);
            cookieURI.spec = "http://" + cookieCfg.domain.replace(/^\./, "");
                
            var cookieStr = '';
            for (var pName in cookieCfg) {
                var pValue = cookieCfg[pName];
                if(pValue !== true){ 
                    cookieStr += (pName + "=" + pValue + ';'); 
                }
            }
            // $.log('util.cookie.setValue() cookieStr: ' + cookieStr);
            Components.classes["@mozilla.org/cookieService;1"]
                .getService(Components.interfaces.nsICookieService)
                .setCookieString(cookieURI, null, cookieStr, null); 
        
        } catch (e) {
            $.log('util.cookie.setValue() exception: ' + e.message);
        }
    },
    remove: function() {
      try {
        var name = $.cookie.f.cName.value,
            domain = $.cookie.f.cDomain.value;
        $.cookie.removeCookie(name, domain);
      } catch (e) {
        $.log('cookie.remove() exception: ' + e.message + ' @' + e.lineNumber);
      }
    },
    
    set: function() {
      try {
        var name = $.cookie.f.cName.value,
            domain = $.cookie.f.cDomain.value,
            value = $.cookie.f.cValue.value;
            // $.log('set() '+name+', '+value+','+domain);
        $.cookie.setCookie(name, value, {domain: domain, expires: 15*60} );
      } catch (e) {
        $.log('cookie.set() exception: ' + e.message + ' @' + e.lineNumber );
      }
    },
    
    set0: function cookie__set0() {
      var name = $.cookie.f.cName.value,
          domain = $.cookie.f.cDomain.value,
          value = $.cookie.f.cValue.value;
    },
    set1: function() {
      $.attempt(set0);
    }
  };
  
  $.cookie = cookie;
  

  $.init = function(){
      $.log(' * cookie.js app.init();');
      
      $.cookie.f = {
        cName: document.getElementById('cName'),
        cDomain: document.getElementById('cDomain'),
        cValue: document.getElementById('cValue')
      };  
      // Lenta.ru, 7Дней.ру
      var cval = '4f657db0-adea-4fd9-aa45-e63c147d8b53,c8bfb84b-ee9d-4955-8ecc-57c0d8d592c7';
      // 1fd86d66-b93d-47c1-b71a-f9afdc518cf6,b53d5b29-b303-4418-b98b-03e2973a953e,5edcf478-2d1d-4b51-b86e-74cc2f14d6fc
                 
      $.cookie.f.cValue.value = cval;
  };
  
  
})( app );

