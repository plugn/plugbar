var xsl = {
    logSvc : Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService),
    xmlSerializer: new XMLSerializer(),
    
    log: function(text) { xsl.logSvc.logStringMessage(text); },
    loggo: function(obj, cmt, echotypes) { 
        try {
            var is = [];
            var types = ['String', 'Number', 'Boolean', 'Error', 'Date', 'Function', 'RegExp', 'Object', 'Array', 
                'HTMLCollection', 'NodeList', 'DocumentFragment', 'Document', 'Node', 'Element', 'XULElement', 'Attr', 'CharacterData', 
                'ProcessingInstruction', 'DocumentType' ] ;
            for (var t=0; t<types.length; t++) 
               if ( obj instanceof window[ types[t] ] ) is.push( types[t] );
               
            var val = (-1 != ['Node', 'Document', 'DocumentFragment', 'Element', 'XULElement'].indexOf(is[is.length-1]) )?
            (xsl.xmlSerializer.serializeToString(obj)) : JSON.stringify(obj);
            xsl.logSvc.logStringMessage((cmt? cmt+' : ' : '') + val + (echotypes? '. * types: {'+is.join(',')+'}' : '') );                
        } catch(e) { xsl.log(' !!! ' + types[t] + ' : ' + e.message + '@'+e.lineNumber); }
    },
    
    transform: function(){
        try {
            this.log('transform()');
            var inXsl  = document.getElementById('rmbXslInput'),
                inXml  = document.getElementById('rmbXmlInput'),
                elmOut = document.getElementById('rmbTplOutput');
            
            var oParser = new DOMParser();
            var docXsl = inXsl.value? oParser.parseFromString(inXsl.value, "text/xml") : '-1',
                docXml = inXml.value? oParser.parseFromString(inXml.value, "text/xml") : '-1';
                
            let r = xsl._transformXML(docXml, docXsl);
            
            elmOut.value = xsl.xmlSerializer.serializeToString(r);
            
        } catch (e) {
            this.log('transform() exception: ' + e.message);
        }
    },
    
    _transformXML: function(aXML, aStylesheet) {
        if (!(aXML && aStylesheet))
            return null;
        
        let xsltProcessor = Components.classes["@mozilla.org/document-transformer;1?type=xslt"].createInstance(Components.interfaces.nsIXSLTProcessor);
        
        let result = null;
        
        try {
            xsltProcessor.importStylesheet(aStylesheet);
            let ownerDocument = document.implementation.createDocument("", "xsl-tmp", null);
            result = xsltProcessor.transformToFragment(aXML, ownerDocument);
        } catch (ex) {
            this.log("Can't transform XML. " + ex);
        }
        
        if (!result || (result.documentElement && result.documentElement.localName == "parsererror")) {
            this.log("Can't transform XML.");
            result = null;
        }
        
        return result;
    }   



    
    
    
}    
    
