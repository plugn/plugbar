var ramblerTpl = {
    logSvc : Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService),
    log    : function(text) { ramblerTpl.logSvc.logStringMessage(text); },
    
    onLoad : function () {
        this.log('ramblerTpl onLoad()');
    },
    
    render: function( dir ) {
        try {
            this.log('render() ');
            var elmIn  = document.getElementById('rmbTplInput');
            var elmOut = document.getElementById('rmbTplOutput');
            var xin = elmIn.value;
            if ('json2xml' == dir) {
                var jxon = JSON.parse(xin);
                var rx =  ramblerTpl.JXT.jxon2dom( jxon ) ;
                var xj = (new XMLSerializer()).serializeToString(rx);
                elmOut.value = xj;
            } else {
                var parser = new DOMParser();
                var doc = parser.parseFromString(xin, "text/xml");            
                var xj =  ramblerTpl.JXT.dom2jxon( doc ) ;
                elmOut.value = JSON.stringify( xj );
            }
            
        } catch (e) {
            elmOut.value = e.message + ' \n\n Keep in mind, JS comments is not allowed in JSON source';
            this.log('render() exception: ' + e.message);
        }
    }
}

