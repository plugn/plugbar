var EXPORTED_SYMBOLS = ['Log'];
var Log = {
  logSvc : Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService),
  xmlSerializer: new XMLSerializer(),
  log: function(){
    try {
      var args = Array.prototype.slice.call(arguments);
      if (!args.length) 
        return;
      this.logSvc.logStringMessage((1 < args.length)? this.dumpObject.apply(this, args) : args[0]);
    } catch (e) {
      this.logSvc.logStringMessage(' (!) this.log() exception: ' + e.message + ' @' + e.lineNumber);
    }
  },
  dumpObject: function(obj, cmt, echotypes) {
    try {
      var is = [];
      var types = ['String', 'Number', 'Boolean', 'Error', 'Date', 'Function', 'RegExp', 'Object', 'Array', 
        'HTMLDocument', 'HTMLCollection', 'NodeList', 'DocumentFragment', 'Document', 'Node', 'Element', 'XULElement', 'Attr', 'CharacterData', 
        'ProcessingInstruction', 'DocumentType' ];
      for (var t=0; t<types.length; t++) 
        if ( obj instanceof window[ types[t] ] ) is.push( types[t] );
      var val = (-1 != ['Node', 'Document', 'DocumentFragment', 'Element', 'XULElement'].indexOf(is[is.length-1]) )?
      (this.xmlSerializer.serializeToString(obj)) : JSON.stringify(obj);
      return (cmt? cmt+' :\n' : '') + val + (echotypes? '. * types: {'+is.join(',')+'}' : '');
    } catch(e) { return (' (!) '+arguments.callee.name+'() exception: ' +types[t]+ ' : ' +e.message+ '@' +e.lineNumber); }
  }  
}