function animate(stepfunc, duration, callBack, func, steptime) {
  duration = duration || 500;
  func = func || function (n) {
    return n; //(-Math.cos(n*Math.PI)/2) + 0.5;
  };
  if (typeof stepfunc == 'function') {
    var start = +new Date;
    steptime = steptime || 20; // period
    return new function (duration, func, stepfunc, callBack) {
      var now = 0,
        progress,
        timeout = setTimeout(function() {
          now = new Date - start;
          progress = Math.min(now / duration, 1);
          stepfunc(func(progress));
          if (progress < 1) {
            timeout = setTimeout(arguments.callee, steptime);
          } else {
            timeout = null;
            callBack && callBack();
          }
        }, steptime);
      this.stop = function() {
        timeout && clearTimeout(timeout);
        timeout = null;
      }
    }(duration, func, stepfunc, callBack)
  }
}


var myAnim,
  el = document.getElementById('mydiv'),
  cur_b = el.offsetTop,
  new_b = 0;
  
myAnim = animate(function (prog) {
  el.style.top = (cur_b + (new_b–cur_b) * prog) + 'px';
}, 1000);