<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" />
  <xsl:template match="/results/result">
    <head><style type="text/css">
      html,body {height:100%;}
      html {overflow:hidden;_overflow:none;}
      body {width:100%;margin:0;padding:0;font-family:"Arial","Helvetica",sans-serif;font-size:1em;color:#333;zoom:1;}
      a {color:#05c;text-decoration:none; cursor:pointer;}
      a:hover {text-decoration:underline;}
      .b-head {height:43px;position:relative;z-index:2;}
      .b-nav {height:42px;overflow:hidden;background:#e9e9e9 url(http://informers.rambler.ru/static/informers/horo/4/widget/bg__graynoise.gif);border-bottom:1px solid #f7f7f7;padding:0 15px;margin:0;}
      .b-nav-decoration {width:100%;height:5px;display:block;overflow:hidden;background: url(http://informers.rambler.ru/static/informers/horo/4/widget/shadow__horo.png) repeat-x 0 0;border-top:1px solid #c2c2c2;padding:0;margin:0 0 12px;position:absolute;top:100%;left:0;z-index:3;/* ^ */_background-image:url(http://informers.rambler.ru/static/informers/horo/4/widget/shadow__horo.gif);}
      .b-nav-item {float:left;height:24px;display:block;white-space:nowrap;padding:0 0 0 14px;margin:9px 0 0 0;outline:none;font-size:13px;line-height:23px;/* _ */_width:10px;*display:inline;zoom:1;}
      .b-nav-item_right {float:right;}
      .b-nav-item__inner {height:24px;display:block;padding:0 14px 0 0;margin:0;}
      .b-nav-item_current {background:#dcdcdc url(http://informers.rambler.ru/static/informers/horo/4/widget/button__bg.gif) no-repeat 0 0;color:#333}
      .b-nav-item_current:hover {text-decoration:none;}
      .b-nav-item_current .b-nav-item__inner {background:url(http://informers.rambler.ru/static/informers/horo/4/widget/button__bg.gif) no-repeat 100% -26px;}
      .b-content {width:100%;overflow:auto;position:absolute;top:44px;left:0;z-index:1;zoom:1; height: 288px; padding: 0 0 18px;}
      .b-content__inner {padding:10px 0 5px;}
      .b-sign-name {font-size:1.5em;line-height:1.5;padding:0;margin:0 28px 3px;}
      .b-sign-dates {font-size:0.7em;font-weight:normal;color:#888;line-height:1.5;padding:0;margin:0 28px 6px;}
      .b-sign-horo {font-size:0.8em;line-height:1.4;padding:0;margin:0 28px 28px;}
    </style></head>
    <body>
        <div class="b-head">
            <div class="b-nav">
                <a>
                    <xsl:attribute name="href"><xsl:value-of select="url"/></xsl:attribute>
                    <xsl:attribute name="class">b-nav-item b-nav-item_current</xsl:attribute>
                    <span class="b-nav-item__inner">Гороскоп на сегодня</span>
                </a>
                 <a>
                    <xsl:attribute name="href"><xsl:value-of select="horo_link_next"/></xsl:attribute>
                    <xsl:attribute name="class">b-nav-item</xsl:attribute>
                    <span class="b-nav-item__inner">На завтра</span>
                </a>
                 <a>
                    <xsl:attribute name="href"><xsl:value-of select="horo_link_other"/></xsl:attribute>
                    <xsl:attribute name="class">b-nav-item b-nav-item_right</xsl:attribute>
                    <span class="b-nav-item__inner">Другие знаки</span>
                </a>
            </div>
            <i class="b-nav-decoration"></i>
        </div>
        <div class="b-content">
            <div class="b-content__inner">
                <h2 class="b-sign-name"><xsl:value-of select="horo_title"/></h2>
                <h6 class="b-sign-dates"><xsl:value-of select="horo_subtitle"/></h6>
                <p class="b-sign-horo"><xsl:value-of select="horo_text"/></p>
            </div>
        </div>
    </body>
  </xsl:template>
  <xsl:template match="informer">
    <head><style type="text/css">
        html,body {height:100%;}
        html {overflow:hidden;_overflow:none;}
        body {width:100%;margin:0;padding:0;font-family:"Arial","Helvetica",sans-serif;font-size:1em;color:#333;zoom:1;}
        a {color:#05c;text-decoration:none; cursor:pointer;}
        a:hover {text-decoration:underline;}
        .b-head {height:43px;position:relative;z-index:2;}
        .b-nav {height:42px;overflow:hidden;background:#e9e9e9 url(http://informers.rambler.ru/static/informers/horo/4/widget/bg__graynoise.gif);border-bottom:1px solid #f7f7f7;padding:0 15px;margin:0;}
        .b-nav-decoration {width:100%;height:5px;display:block;overflow:hidden;background: url(http://informers.rambler.ru/static/informers/horo/4/widget/shadow__horo.png) repeat-x 0 0;border-top:1px solid #c2c2c2;padding:0;margin:0 0 12px;position:absolute;top:100%;left:0;z-index:3;/* ^ */_background-image:url(http://informers.rambler.ru/static/informers/horo/4/widget/shadow__horo.gif);}
        .b-nav-item {float:left;height:24px;display:block;white-space:nowrap;padding:0 0 0 14px;margin:9px 0 0 0;outline:none;font-size:13px;line-height:23px;/* _ */_width:10px;*display:inline;zoom:1;}
        .b-nav-item_right {float:right;}
        .b-nav-item__inner {height:24px;display:block;padding:0 14px 0 0;margin:0;}
        .b-nav-item_current {background:#dcdcdc url(http://informers.rambler.ru/static/informers/horo/4/widget/button__bg.gif) no-repeat 0 0;color:#333}
        .b-nav-item_current:hover {text-decoration:none;}
        .b-nav-item_current .b-nav-item__inner {background:url(http://informers.rambler.ru/static/informers/horo/4/widget/button__bg.gif) no-repeat 100% -26px;}
        .b-content {width:100%;overflow:auto;position:absolute;top:44px;left:0;z-index:1;zoom:1; height: 288px; padding: 0 0 18px;}
        .b-content__inner {padding:10px 0 5px;}
        .b-sign-name {font-size:1.5em;line-height:1.5;padding:0;margin:0 28px 3px;}
        .b-sign-dates {font-size:0.7em;font-weight:normal;color:#888;line-height:1.5;padding:0;margin:0 28px 6px;}
        .b-sign-horo {font-size:0.8em;line-height:1.4;padding:0;margin:0 28px 28px;}
    </style></head>
    <body>
        <div class="b-head">
            <div class="b-nav">
                <a href="http://horoscopes.rambler.ru/" class="b-nav-item b-nav-item_current"><span class="b-nav-item__inner">Гороскоп на сегодня</span></a>
            </div>
            <i class="b-nav-decoration"></i>
        </div>
        <div class="b-content"><div class="b-content__inner"><h2 class="b-sign-name">Идет получение данных</h2></div></div>
    </body>				
  </xsl:template>
</xsl:stylesheet>
      