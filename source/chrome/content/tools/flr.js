var flr = {
    logSvc : Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService),
    xmlSerializer: new XMLSerializer(),
    
    log: function(text) { flr.logSvc.logStringMessage(text); },
    loggo: function(obj, cmt, echotypes) { 
        try {
            var is = [];
            var types = ['String', 'Number', 'Boolean', 'Error', 'Date', 'Function', 'RegExp', 'Object', 'Array', 
                'HTMLCollection', 'NodeList', 'DocumentFragment', 'Document', 'Node', 'Element', 'XULElement', 'Attr', 'CharacterData', 
                'ProcessingInstruction', 'DocumentType' ] ;
            for (var t=0; t<types.length; t++) 
               if ( obj instanceof window[ types[t] ] ) is.push( types[t] );
               
            var val = (-1 != ['Node', 'Document', 'DocumentFragment', 'Element', 'XULElement'].indexOf(is[is.length-1]) )?
            (flr.xmlSerializer.serializeToString(obj)) : JSON.stringify(obj);
            flr.logSvc.logStringMessage((cmt? cmt+' : ' : '') + val + (echotypes? '. * types: {'+is.join(',')+'}' : '') );                
        } catch(e) { flr.log(' !!! ' + types[t] + ' : ' + e.message + '@'+e.lineNumber); }
    },
    
    transform: function(){
        try {
            this.log('transform()');
            var elmSrc = document.getElementById('rmbSource'),
                elmXcl  = document.getElementById('rmbExclude'),
                elmRsl  = document.getElementById('rmbResult');
            var txtSrc = elmSrc.value || '',
                txtXcl = elmXcl.value || '';
            
var dts = Date.now();

            let r = flr.filter(txtSrc, txtXcl);
            
            elmRsl.value = (Array.isArray(r))? r.join('\n') : 'not an Array: {{'+r+'}}';
            
this.log('========================\n time used: '+(Date.now()-dts));
            
        } catch (e) {
            this.log('transform() exception: ' + e.message);
        }
    },
    
    filter: function(src, xcl) {
      try {
        var r = '',
            sList = src.replace(/[\r\n]+/mg,'\n').split('\n'),
            xList = xcl.replace(/[\r\n]+/mg,'\n').split('\n')
        this.loggo(sList, 'sList ['+sList.length+']');
        this.loggo(xList, 'xList ['+xList.length+']');
/*        
        var f = sList
          .map(function(item){
            var [k,v] = item.split('=');
            return k || null;
          })
          .filter(function(item){
            return !!item;
          });        
*/          
        var f = sList          
          .map(function(item){
            var o={},
                [k,v] = item.split('=');
            o[k]=v;
            return k? o : null;
          })
          .filter(function(item){
            return !!item;
          });           
          
        this.loggo(f, 'f ['+f.length+']');  
        
        if (!Array.isArray(f)) 
          return null;
        
        var g = f
          .filter(function(item){
            return (-1 == xList.indexOf( Object.keys(item)[0] ))
          })
          .map(function(item){
            var key = String(Object.keys(item)[0]);
            return key + '=' + item[key];
          });
        
        this.loggo(g, 'g ['+g.length+']');  
          
        return g;
        
      } catch (e) {
        this.log('filter() exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    },
    
    _transformXML: function(aXML, aStylesheet) {
        if (!(aXML && aStylesheet))
            return null;
        
        let xsltProcessor = Components.classes["@mozilla.org/document-transformer;1?type=xslt"].createInstance(Components.interfaces.nsIXSLTProcessor);
        
        let result = null;
        
        try {
            xsltProcessor.importStylesheet(aStylesheet);
            let ownerDocument = document.implementation.createDocument("", "xsl-tmp", null);
            result = xsltProcessor.transformToFragment(aXML, ownerDocument);
        } catch (ex) {
            this.log("Can't transform XML. " + ex);
        }
        
        if (!result || (result.documentElement && result.documentElement.localName == "parsererror")) {
            this.log("Can't transform XML.");
            result = null;
        }
        
        return result;
    }   



    
    
    
}    
    
