(function($){

  function KeyCaster(srcNode, evtType){
    this.reset();
    this.setSourceNode(srcNode);
    this.setEventType(evtType);
  };
  
  KeyCaster.prototype = {
    EVENT_SCOPE: 'KeyboardEvent',
    defProps: {
      evtType: 'keypress',
      srcNode: null,
      evtData: []
    },
    reset: function(){
      var propNames = Object.keys(this.defProps);
      for (var j=0; j < propNames.length; j++)
        this[ propNames[j] ] = this.defProps[ propNames[j] ];
    },
    setSourceNode: function(srcNode){
      this.srcNode = srcNode || null;
    },
    setEventType: function(type) {
      this.evtType = type || 'keypress';
    },
    isReady: function() {
      return !!(this.evtType && this.srcNode);
    },
    clear: function() {
      this.evtData = [];
    },
    load: function(data){
      this.clear();
      this.append(data);
    },
    append: function(data){
      for (var i=0; i<data.length; i++){
        var item = data[i];
        if ('string' == typeof item) {
          this.evtData.push({'char':item})
        } else if (('object'==typeof item) && item) {
          if (!item.key && !item.char)
            continue;
          this.evtData.push(item);  
        }
      }
    },
    play: function(what){
      $.log(this, 'keyCaster.play()');
      if (!this.isReady()) {
        $.log(this, 'keyCaster.play() instance is not ready to play ');
        return false;
      }  
      if (what) 
        this.load(what);
        
      for (var p=0; p < this.evtData.length; p++){
        var q = this.evtData[p];
        q && this.sendEvent(q);
      }
    },
    sendEvent: function(q){
      try {
        var event = document.createEvent(this.EVENT_SCOPE);
        event.initKeyEvent(this.evtType, true, true, null, 
          (q.ctrl? 1:0), (q.alt? 1:0), (q.shift? 1:0), (q.meta? 1:0),  
          (q.key || 0), (q.char? q.char.charCodeAt(0) : 0) );
        this.srcNode.dispatchEvent(event);
      } catch (e) {
        $.log(' (!) KeyCaster.sendEvent() exception: ' + e.message + '@' + e.lineNumber );
      }
    }
  }
  
  $.keyListener = function(event){
    $.log(' {event} type:'+event.type+', keyCode: '+event.keyCode+', charCode:'+event.charCode+
          ', ctrlKey:'+event.ctrlKey+', altKey: '+event.altKey+', shiftKey:'+event.shiftKey+', meta:'+event.metaKey);
  };
  $.castText = function(){
    var srcNode = document.getElementById('cDomain')
      , kybd = new KeyCaster(srcNode, 'keypress')
      , text = document.getElementById('cName').value;
    kybd.play(text);
  }
  $.castKey = function(){
    var srcNode = document.getElementById('cDomain')
      , kybd = new KeyCaster(srcNode, 'keypress');
      
    srcNode.focus();  
    kybd.play([ {key:9} ]);
  }

  $.init = function(){
    $.log(' * evtd: app.init() ');
    window.addEventListener('keypress', $.keyListener)
  };
  
})( app );