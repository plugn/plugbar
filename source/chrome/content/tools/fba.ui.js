(function(scope){

  var WM = Components.classes["@mozilla.org/appshell/window-mediator;1"].getService(Components.interfaces.nsIWindowMediator);
  
  var ui = {
		MICROBROWSER_PATH: 'chrome://rambler_plugbar/content/tools/microbrowser/microbrowser.xul',
		MICROBROWSER_WINDOW_NAME: "facebook:browser",

		openMicrobrowser: function (features, args) {
			var window = scope.ui.getTopBrowserWindow(),
				browserWindow,
				inject = function () {
					window.close = function () {
						var evt = document.createEvent("Event");
						evt.initEvent("CloseMicrobrowser", true, false);
						document.documentElement.dispatchEvent(evt);
					}
				},
				
				defaultArgs = {
					inject: function (document) {
						var location = document.location;
						if (location.toString().indexOf("http") !== 0) return;
						var script = document.createElement("script");
						script.innerHTML = inject.toSource() + "()";
						document.addEventListener("CloseMicrobrowser", function () {
							browserWindow.close();
						}, false, true);
						document.documentElement.appendChild(script);
					}
				};
			
			scope.utils.mix(args, defaultArgs);
			browserWindow = window.openDialog(scope.ui.MICROBROWSER_PATH, scope.ui.MICROBROWSER_WINDOW_NAME, features, args);
			return browserWindow;
		},    
    
    getTopBrowserWindow: function fba__ui__getTopBrowserWindow() {
      return WM.getMostRecentWindow('navigator:browser');    
    }
  };
    
  scope.ui = ui;
  
 })( fba ); 
