<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" encoding="utf-8" version="1.0" indent = "yes" />
  <xsl:template match="/">
    <results><result>
      <menus>
        <xsl:for-each select="rss/channel/item">
          <menuitem>
            <xsl:attribute name="text">
              <xsl:value-of select="title" />
            </xsl:attribute>
            <xsl:attribute name="url">
              <xsl:value-of select="link" />
            </xsl:attribute>
          </menuitem>
          <xsl:if test="position()=last()">
            <menuitem>
              <xsl:attribute name="text">-</xsl:attribute>
              <xsl:attribute name="url">-</xsl:attribute>
            </menuitem>
            <menuitem>
              <xsl:attribute name="text">Обновить</xsl:attribute>
              <xsl:attribute name="url">force</xsl:attribute>
            </menuitem>
          </xsl:if>
        </xsl:for-each>
      </menus>
    </result></results>
  </xsl:template>
</xsl:stylesheet>