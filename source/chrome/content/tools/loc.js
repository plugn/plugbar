Components.utils.import('resource://rambler_plugbar-mod/jsuri.min.js');

var loc = {
    logSvc : Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService),
    xmlSerializer: new XMLSerializer(),
    
    log: function(text) { loc.logSvc.logStringMessage(text); },
    loggo: function(obj, cmt, echotypes) { 
        try {
            var is = [];
            var types = ['String', 'Number', 'Boolean', 'Error', 'Date', 'Function', 'RegExp', 'Object', 'Array', 
                'HTMLCollection', 'NodeList', 'DocumentFragment', 'Document', 'Node', 'Element', 'XULElement', 'Attr', 'CharacterData', 
                'ProcessingInstruction', 'DocumentType' ] ;
            for (var t=0; t<types.length; t++) 
               if ( obj instanceof window[ types[t] ] ) is.push( types[t] );
               
            var val = (-1 != ['Node', 'Document', 'DocumentFragment', 'Element', 'XULElement'].indexOf(is[is.length-1]) )?
            (loc.xmlSerializer.serializeToString(obj)) : JSON.stringify(obj);
            loc.logSvc.logStringMessage((cmt? cmt+' : ' : '') + val + (echotypes? '. * types: {'+is.join(',')+'}' : '') );                
        } catch(e) { loc.log(' !!! ' + types[t] + ' : ' + e.message + '@'+e.lineNumber); }
    },
    init: function() {
      loc.log(' mod loc init()');
      loc.txl = document.getElementById('txl');
      loc.txl.value = 'https://domain.tld:50/path/to/file.ext?chars&p=1&q=2&amp;r=3&0;#hash01:abc#def#g:10&098?a=1'
    },
    
    /*
    Subclasses of nsIURI, such as nsIURL , impose further structure on the URI.
    To create an nsIURI object, you should use nsIIOService.newURI() , like this:    
    */
    makeURI: function(aURL, aOriginCharset, aBaseURI) {
      try {
        var ioService = Components.classes["@mozilla.org/network/io-service;1"]
                      .getService(Components.interfaces.nsIIOService);
        return ioService.newURI(aURL, aOriginCharset || null, aBaseURI || null);
      } catch (e) {
        loc.log('makeURI('+aURL+') exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    },     
        
    // 'https://domain.tld:50/path/to/file.ext?chars&p=1&q=2&amp;r=3&0;#hash01:abc#def#g:10&098?a=1'
    
    render2: function() {
      var txl = loc.txl;
      var str = 'utm_source=r07&utm_medium=distribution&utm_content=e08&utm_campaign=a11&=we&d=&=';
      var f = str.split('&')
        .map(function(item){
          var o={},
              [k,v] = item.split('=');
          o[k]=v;
          return k? o : null;
        })
        .filter(function(item){
          return !!item;
        });
        
      loc.log(' * render2() txl.value: '+txl.value + '; f: '+JSON.stringify(f));
      
      var uri = new jsUri(txl.value);
      var lh = [
        "protocol", 
        "host", 
        "port", 
        "path", 
        "query", 
        "anchor"
      ];        
      
      function logURI(uri, lh){
        return lh.map(function(item){
          return '\t#'+item+': '+ ('function'== typeof uri[item]? uri[item]() : '""');
        }).join('\n');
      }
      
      loc.log(' * initial uri:\n'+logURI(uri, lh) + ' ;\n uri: '+uri.toString());      
      
      var uri2 = new jsUri(txl.value);
      for (var j=0; j<f.length; j++) {
        for (var jkey in f[j]) {
          uri2.addQueryParam(jkey, f[j][jkey] )
        } 
      }
      
      loc.log(' * final uri:\n'+logURI(uri2, lh) + ' ;\n uri: '+uri2.toString());      
      
      
      // loc.log(' * render2() * '+txl.value+' ::: ' + JSON.stringify(uriObj));
    },
    
    
    render: function() {
      var txl = loc.txl;
      loc.log('render txl.value: '+txl.value);
      
      var uri = loc.makeURI(txl.value, null, null);
      loc.log('nsIURI: '  + uri);
      var lh = ['spec', 'path', 'asciiHost', 'asciiSpec', 'host', 'hostPort', 'port', 'prePath', 'ref', 'scheme', 'pathname', 'search'];
      var rl = lh.map(function(item){
        return '\t#'+item+': '+ ('undefined'==typeof uri[item]? "undefined" : (uri[item]?uri[item]:'""'));
      });
      loc.log(' * uri:\n'+rl.join('\n'));
      loc.log('uri.resolve()'+uri.resolve(''));
      // loc.log('nsIURI.spec: '  + uri.spec+' \n nsIURI.path: '  + uri.path + '; \n ' );
    }
    
    
}