    var scheme = {
        widgets : null,
        widgetsByGuid: null,
        cfgDoc: null,
        tmpDoc: null,
        tplStore: null,
        onLoad : function( doc ) {
            $.scheme.setup(doc);
            $.scheme.update();
            $.sync.initTimer();
        },
        initTemplates: function() {
        },
        setup: function(doc) {
          widgetObj = $.scheme.setupWidget(wgt);
        },
        setupWidget: function(wgt) {
          if (tpl || control) {
            widgetObj = { 
              guid: (wgt.getAttribute('guid') || ''),
              control: control,
              informer: (informer || null),
              reload: parseInt(reload, 10),
              nfoBody : (infoSet.length? infoSet[0] : null),
              visible: (wgt.getAttribute('visible') || 'true'),
              deleted: (wgt.getAttribute('deleted') || 'false'),
              options: options,
              lastUpdated: 0,
              dataURI: uri,
              template: (jxtpl? jxtpl.nodeValue : null), 
              templateitem: (jxitem? jxitem.nodeValue : null),
              templatepopup: ('XSLT'==tplPopupNS && tplPopup) ? tplPopup : (jxpopup? jxpopup.nodeValue : null), 
              popupns: tplPopupNS                 
            };
          }        
          return widgetObj;
        },
        isRssData : function(dataSet) {
        },
        isControlExist: function(control) {
        },
        isControlBasic: function(control) {
        },        
        isControlModular: function(control) {
        },        
        makeTplSet: function(wgt) {
          return tplSet;
        },
        getTplSet: function( elm ) {
          return tplSet;
        }, 
        updateWidget: function(guid) {
          if (!wList.length) { // '[guid="'+guid+'"] empty!
            return $.scheme.update(); 
          }
          if (wgt.dataURI)
            $.scheme.fetch(wgt.dataURI, wgt.guid);
          else
            $.scheme.createWidget(dataSet, wgt.guid, wgt.informer);
        },
        update: function() {
          var wList = Zepto('[plugtype]', $.appNode);
          for (var i=0; i < wList.length; i++)
            (wList[i].parentNode).removeChild(wList[i]);
          for (var i=0; i<$.scheme.widgets.length; i++)
            $.scheme.runWidget(widgets[i]);
        },
        
        runWidget: function(wgt) {
          var theSpring = document.getElementById($.APPNAME+'-spring'),
              hostWgt = document.createElement('toolbaritem'),
              refElm = ('always-right'==wgt.visible? theSpring.nextSibling : theSpring);
          hostWgt.setAttribute('guid', wgt.guid);    
          hostWgt.setAttribute('visible', wgt.visible);
          hostWgt.setAttribute('plugtype', 'host');
          (theSpring.parentNode).insertBefore(hostWgt, refElm);
          
          var dataSet = wgt.nfoBody? [wgt.nfoBody] : ($.util.api.parser( wgt.informer ));
          $.scheme.createWidget(dataSet, wgt.guid, wgt.informer);
          if (wgt.dataURI)
            $.scheme.fetch(wgt.dataURI, wgt.guid);
        },
        
        createWidget: function(dataSet, guid, dataXML) {
          var wgt = $.scheme.widgetsByGuid[guid];
          var data = isRss? dataSet : (tplSet.templateitem? (tplItemFake? {list:dataSet} : dataSet[0]) : dataSet[0]);
          if (isRss && guid) {
            data.image = (wgt && wgt.nfoBody)? (wgt.nfoBody.image || '') : '';
            data.text = (wgt && wgt.nfoBody)? (wgt.nfoBody.text || '') : '';
            data.tooltip = (wgt && wgt.nfoBody)? (wgt.nfoBody.tooltip || data.tooltip || '') : (data.tooltip || '');
          }
          
          if (tplSet['templatepopup'] && 'XSLT'==tplSet['popupns'] && (xchKey=$.getCfg('xchWProp', null))) {
            var xR = $.sync.transformXML(docXml, docXsl);
            $[xchKey][guid || 'defValue'] = {
              docFrag  : xR,
              docTitle : data.title || data.text || (wgt.nfoBody?(wgt.nfoBody.title || wgt.nfoBody.text) : '')
            }                
          } else if (tplSet.templatepopup && (xchKey=$.getCfg('xchWProp', null))) {
            if('object' !== typeof $[xchKey])
              $[xchKey] = {};
            $[xchKey][guid || 'defValue'] = {
              docFrag  : $.util.jxt.render({'template':tplSet.templatepopup}, data, 'XHTML'),
              docTitle : data.title || data.text || (wgt.nfoBody?(wgt.nfoBody.title || wgt.nfoBody.text) : '')
            }
          } 
          var dF = $.util.jxt.render(tplSet, data);
          if (data.menus) {
            Zepto('toolbarbutton', dF).append(data.menus);
          }
          
          var hosts = guid? Zepto('[guid="'+guid+'"]', $.appNode) : [];
          var host = hosts.length? hosts[0] : null;
          var aParent = host.parentNode || null;
          var nextHost = hosts[hosts.length-1].nextSibling;
          var aVisible = host.getAttribute('visible');
          var visible = (-1 < ['false','true','always','always-right'].indexOf(aVisible))? aVisible : 'true';
          var dfC = dF.childNodes; 
          
          if (host && host.getAttribute('plugtype')=='host') {
            for (var f=0; f < dfC.length; f++) {
              dfC[f].setAttribute('plugtype', 'widget');
              dfC[f].setAttribute('guid', guid);
              dfC[f].setAttribute('visible', visible);
              dfC[f].setAttribute('hidden', (('false'==visible)? 'true' : 'false'));
            }
            aParent.insertBefore(dF, nextHost);
            for (var i=0; i < hosts.length; i++)
              aParent && aParent.removeChild(hosts[i]); 
            
          } else {
          
            for (var g=0; g < Math.max(hosts.length, dfC.length); g++) {
              if(hosts[g] && dfC[g]) {
                if (dfC[g].hasAttributes()) {
                  for (var iLength=0; iLength < dfC[g].attributes.length; iLength++) {
                    var iAttr = dfC[g].attributes.item(iLength);
                    hosts[g].setAttribute(iAttr.nodeName, iAttr.value);
                  }
                }
                while (hosts[g].firstChild) 
                  hosts[g].removeChild(hosts[g].firstChild); 
                  
                if (dfC[g].hasChildNodes()) {
                  var iChild;
                  for (var iChildId = 0; iChildId < dfC[g].childNodes.length; iChildId++) {
                    iChild = dfC[g].childNodes.item(iChildId);
                    if (iChild) hosts[g].appendChild(iChild);
                  }
                }    
              } else if (dfC[g]) {
                dfC[g].setAttribute('plugtype', 'widget');
                dfC[g].setAttribute('guid', guid);
                dfC[g].setAttribute('visible', visible);
                dfC[g].setAttribute('hidden', (('false'==visible)? 'true' : 'false'));
                
                var injectElm = dfC[g].cloneNode(true);
                aParent.insertBefore(injectElm, nextHost);
                
              } else if (hosts[g]) {
                (hosts[g].parentNode).removeChild(hosts[g]);
              }
            }
          }
        },
        addWidget: function(uri) {
          var cfg = $.util.getArgs({ 
            uri: uri, 
            success: (function(doc){
              $.scheme.addFetchedWidget(doc);
            })
          });
          $.util.xhr = Zepto.ajax( cfg ); 
        },
        addFetchedWidget: function(doc) {
          var scm = Zepto('scheme', doc)[0],
              wgt = Zepto('widget', doc)[0],
              scmv = scm.getAttribute('version') || 0,
              guid = wgt.getAttribute('guid') || null,
              sM = Zepto('scheme', $.scheme.cfgDoc);
          if (sM.length)
            sM[0].appendChild(wgt);
          
          $.scheme.setup($.scheme.cfgDoc);
          $.scheme.saveConfig();
          $.scheme.runWidget(widgetObj);
          $.sync.notifyGallery(false, [guid]); 
        },
        moveWidget: function(actorGuid, targetGuid, where) {
          $.scheme.rearrangeWidgets(actorGuid, targetGuid, where, $.appNode);
          $.scheme.rearrangeWidgets(actorGuid, targetGuid, where, $.scheme.cfgDoc);
          
          $.scheme.setup($.scheme.cfgDoc);
          $.scheme.saveConfig();
        },
        removeWidget: function(guid) {
            var wList = Zepto('[guid="'+guid+'"]', $.appNode);
            for (var i=0; i < wList.length; i++)
                (wList[i].parentNode).removeChild(wList[i]);
            var wgtx = Zepto('[guid="'+guid+'"]', $.scheme.cfgDoc); 
            if(wgtx.length)
              wgtx[0].setAttribute('deleted', 'true'); 
            
            $.scheme.setup($.scheme.cfgDoc);
            $.scheme.saveConfig();
            
            $.sync.notifyGallery(true, [guid]);
        },
        
        rearrangeWidgets: function(actorGuid, targetGuid, where, hostNode) {
          return hostNode;
        },
        
        fetch: function(dataURI, guid) {
            dataURI = dataURI.replace(/#?\{[\w\d:;\.,_-]+\}/gi, function(str){
                var prefPkg = str.replace(/(^#?\{|\}$)/g,'').split(':');
                var lookup = (prefPkg.length? prefPkg[0] : '');
                return (lookup? $.prefMgr.get(lookup, '') : (prefPkg[1] || ''));
            });
            var xhrArgs = {
                url: dataURI, 
                dataType: 'xml', 
                headers  : $.util.getHttpHeaders('xml'),
                error    : function(xhr,s,err){
                    try {
                        var wgt = $.scheme.widgetsByGuid[guid] || null;
                        if (!wgt) return;
                        if (401 == xhr.status && xhr.responseXML) {
                            var dataSet = $.util.api.parser(xhr.responseXML);
                            if (!dataSet) return;
                            wgt.lastUpdated = Date.now();
                            $.scheme.createWidget(dataSet, guid, xhr.responseXML);
                        } else {
                            var dataDef = wgt.nfoBody? [wgt.nfoBody] : ($.util.api.parser( wgt.informer ));                        
                            $.scheme.createWidget(dataDef, guid, wgt.informer);
                        } 
                    } catch(e) {
                      $.log('scheme.fetch() xhr.error() exception: ' + e.message + ' @scheme.js:' +e.lineNumber +'; dataURI:'+dataURI + '; http-status: ' + xhr.status);
                    }
                },
                success : function(r,s,xhr){
                    try {
                        var wgt = $.scheme.widgetsByGuid[guid] || null;
                        if (!wgt) return;
                        var rssE = Zepto('rss', r); 
                        var parser = (rssE && rssE.length)? 'rss' : 'parser';
                        if ('rss'==parser) {
                          var ioService = Components.classes['@mozilla.org/network/io-service;1']
                                                             .getService(Components.interfaces.nsIIOService);
                          var feedUrl = ioService.newURI(dataURI, null, null);
                          $.util.api.processFeed(xhr.responseText, feedUrl, wgt);
                          return;
                        }
                        var dataSet = $.util.api[ parser ]( r );
                        if (dataSet) {
                            wgt.lastUpdated = Date.now();
                            $.scheme.createWidget(dataSet, guid, r);
                        } else {
                            var dataDef = wgt.nfoBody? [wgt.nfoBody] : ($.util.api.parser( wgt.informer ));
                            $.scheme.createWidget(dataDef, guid, wgt.informer);
                        }
                    } catch(e) {
                      $.log('scheme.fetch() xhr.success() exception: ' + e.message + ' @ ' +e.lineNumber);
                    }
                }
            };
            
            Zepto.ajax( xhrArgs );
        
        },
        getWidgetElement: function(guid) {
        },        
        getGuids: function() {
          var awg = [];
          for (var j=0; j<$.scheme.widgets.length; j++) {
            var wgt=$.scheme.widgets[j];
            if ('true'!=wgt.deleted && wgt.guid) 
              awg.push(wgt.guid);
          }
          return awg;
        },
        saveConfig: function(doc) {
        }
        
    };