(function($){

    var exports = {
        /*  https://developer.mozilla.org/en/Code_snippets/Tabbed_browser  */
        enumBrowsers: function() {
            var num = gBrowser.browsers.length;
            for (var i = 0; i < num; i++) {
              var b = gBrowser.getBrowserAtIndex(i);
              try {
                dump(b.currentURI.spec); // dump URLs of all open tabs to console
              } catch(e) {
                Components.utils.reportError(e);
              }
            }
        },
    
        initObserver: function() {
            // $.log('initObserver() * * * ');
            $.Observer = function(){ 
                this.register();
            };
            
            $.Observer.prototype = {
                observe: function(subject, topic, data) {
                    var id = subject.controller.getCommentAt(subject.popup.selectedIndex);
                    $.log(' * subject.controller.getValueAt('+subject.popup.selectedIndex+') : '+subject.controller.getValueAt(subject.popup.selectedIndex)+'; id:'+id);
                },
                register: function() {
                    var observerService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);
                    observerService.addObserver(this, "autocomplete-did-enter-text", false);
                },
                unregister: function() {
                    var observerService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);
                    observerService.removeObserver(this, "autocomplete-did-enter-text");
                }
            };    
            if (!this.observer) 
                this.observer = new $.Observer();
        },    
        quitSmartSuggest : function() {
            // $.log('quitSmartSuggest()');
            if (this.observer) 
                this.observer.unregister();
        },
        initSmartSuggest : function() {
            // $.log('initSmartSuggest()');
            this.initObserver();

            try {
                var jD = [{
                    value:'Genoa',
                    comment: '500578'
                }, {
                    value:'Радио Рок', 
                    comment: '102,5 FM'
                }, {
                    value:'ЯЗЬ FM', 
                    comment: '936 FM'
                }, {
                    value:'Радио Куропатка FM',
                    comment: '37,54898 F, 55.568786 M'
                }, {
                    value:'Helsinki', 
                    comment: '224687'
                }, {
                    value:'Helsingborg', 
                    comment: '534003'
                }, {
                    value:'Helsinore', 
                    comment: '795792'
                }, {
                    value:'Copenhagen', 
                    comment: '153468'
                }, {
                    value:'Odense', 
                    comment: '795134'
                }, {
                    value:'Moscow', 
                    comment: '495499'
                }, {
                    value:'Arhus', 
                    comment: '359786'
                }];
                
                var ku = document.getElementById('kunde') || null;
                if (ku) {
                    ku.setAttribute('autocompletesearchparam', JSON.stringify(jD));
                    // $.log(' attr: ' + ku.getAttribute('autocompletesearchparam'));
                }
            } catch (e) {
                $.log(' initSmartSuggest() exception: ' + e.message);
            }  
        },
        playStream : function() {
          function loadedMetadata() {
            channels          = audio.mozChannels;
            rate              = audio.mozSampleRate;
            frameBufferLength = audio.mozFrameBufferLength;
          } 

          function audioAvailable(event) {
            var frameBuffer = event.frameBuffer;
            var t = event.time;
                    
            var text = "Samples at: " + t + "\n"
            text += frameBuffer[0] + "  " + frameBuffer[1]
            raw.textContent = text;
          }
          
          var raw = document.getElementById('audio-raw');
          var audio = document.getElementById('audio-element'); 
          audio.addEventListener('MozAudioAvailable', audioAvailable, false); 
          audio.addEventListener('loadedmetadata', loadedMetadata, false);    
        },    
        playTone : function() {
            this.log('$.playTone()');
            var output = new Audio();
            output.mozSetup(1, 44100);
            var samples = new Float32Array(22050);
            var len = samples.length;

            for (var i = 0; i < samples.length ; i++) {
                samples[i] = Math.sin( i / 20 );
            }
            output.mozWriteAudio(samples);
        },    
        // path = 'ProfD/brand_toolbar.cfg.xml'
        checkFileCfg: function(path) {
            if ('string' != typeof path || !path) return false;
            var fh = null, 
                fhExPath = '';
            // $.log(' checkFileCfg() path: ' + path);
            
            var a = path.replace(/\/+$/g, '').split('/'); 
            if (''==a[0] && a.length>1) {
                a.shift(0);
                a[0] = '/'+a[0];
            }
            for (var p=0; p<a.length; p++) {
                if (0==p) {
                    var q = true;
                    if (p==a.length-1) {
                        fh = FileIO.open(a[p]);
                        if (!fh.exists()) { q = FileIO.create(fh); }
                    } else {
                        fh = DirIO.get(a[p]);
                        if (!fh.exists()) { q = DirIO.create(fh); }
                    }    
                    if (!q) return false;
// $.loggo(fh, 'open : nsIFile '+a[p]);
                    if (fh.exists()) { 
                        fhExPath = fh.path; 
                        continue; 
                    } else return false;
                }
                if (fh instanceof Components.interfaces.nsIFile) {
                    var q = true;
                    fh.append(a[p]);
                    if (fh.exists()) { 
                        fhExPath = fh.path; 
                    } else {
                        if (p==a.length-1) { 
                            q = FileIO.create(fh); 
                        } else { 
                            q = DirIO.create(fh); 
                        }
                    }
                    if (!q) return false;
                    if (fh.exists()) {
                        fhExPath = fh.path; 
                    } else {
                        $.log(' Could not create: ' + a[p]);
                        return false;
                    };
                }
// $.log(' fhExPath: {'+fhExPath+'}');

            }
            
            return fh;
        },
        
        writeFileCfg: function() {
            // var fnBack = (function(status)
            var fnBack0 = (function(pipeInputStream, status, aRequest){ 
              var $ = this;
              $.log('fnBack() status: ' + status);
              if (!Components.isSuccessCode(status)) {
                  $.log(' * !Components.isSuccessCode status: ' + status);
                  return;
              }
            }).bind($);
            
            var fnBack = (function(aInputStream, status, aRequest){
              var $ = this;
              $.log('fnBack() status: ' + status);
              if (!Components.isSuccessCode(status)) {
                  $.log(' * !Components.isSuccessCode status: ' + status);
                  return;
              }
              var count = aInputStream.available(),
                  aOptions = {
                      charset:'UTF-8',
                      replacement:''
                  };

              var txt = NetUtil.readInputStreamToString(aInputStream, count, aOptions);
              $.log('fnBack: ' + txt);
            }).bind($);
            
            var aURI = NetUtil.newURI($.config.schemeUri);
            NetUtil.asyncFetch(aURI, fnBack);
        },
        
        // url = 'file:///C:/temp/test.txt'; 
        URI2localPath: function(url) {
          try {
            var ioService = Components.classes['@mozilla.org/network/io-service;1']
                            .getService(Components.interfaces.nsIIOService);
            var fileHandler = ioService.getProtocolHandler('file')
                              .QueryInterface(Components.interfaces.nsIFileProtocolHandler);
            var file = fileHandler.getFileFromURLSpec(url);
            var path = file.path;
            return path; // "C:\temp\temp.txt"
          } catch (e) {
            $.log('$.cpf() exception: ' + e.message);
          } 
        },
        
        // This function is for fetching an image file from a URL. 
        // Accepts a URL and returns the file. 
        // Returns empty if the file is not found (with an 404 error for instance). 
        // Tried with .jpg, .ico, .gif (even .html).
        getImageFromURL: function(url) {
          try {
            var ioserv = Components.classes["@mozilla.org/network/io-service;1"] 
                         .getService(Components.interfaces.nsIIOService); 
            var channel = ioserv.newChannel(url, 0, null); 
            var stream = channel.open(); 

            if (channel instanceof Components.interfaces.nsIHttpChannel && channel.responseStatus != 200) { 
              return ""; 
            }

            var bstream = Components.classes["@mozilla.org/binaryinputstream;1"] 
                          .createInstance(Components.interfaces.nsIBinaryInputStream); 
            bstream.setInputStream(stream); 
            
            var bArray = bstream.readByteArray(stream.available());
            bstream.close();
            stream.close();
            
            return bArray;

          } catch (e) {
            $.log('$.getImageFromURL() exception: ' + e.message + ' @ '+e.lineNumber);
          } 
        },        
        
        readBinaryFile: function(file) {
          try {
            var fileStream = Components.classes['@mozilla.org/network/file-input-stream;1']
                             .createInstance(Components.interfaces.nsIFileInputStream);
            fileStream.init(file, 1, 0, false);
            var binaryStream = Components.classes['@mozilla.org/binaryinputstream;1']
                               .createInstance(Components.interfaces.nsIBinaryInputStream);
            binaryStream.setInputStream(fileStream);
            var bArray = binaryStream.readByteArray(fileStream.available());
            binaryStream.close();
            fileStream.close();
            
            return bArray;      
          } catch (e) {
            $.log('$.readBinaryFile() exception: ' + e.message + ' @ '+e.lineNumber);
          } 
        },
        
        writeBinaryFile: function(file, bArray) {
          try {
            var fileStream = Components.classes['@mozilla.org/network/file-output-stream;1']
                             .createInstance(Components.interfaces.nsIFileOutputStream);
            fileStream.init(file, 2, 0x200, false);
            var binaryStream = Components.classes['@mozilla.org/binaryoutputstream;1']
                               .createInstance(Components.interfaces.nsIBinaryOutputStream);
            binaryStream.setOutputStream(fileStream);
            binaryStream.writeByteArray(bArray, bArray.length);
            binaryStream.close();
            fileStream.close();        
            return true;
          } catch (e) {
            $.log('$.writeBinaryFile() exception: ' + e.message + ' @ '+e.lineNumber);
            return false;
          } 
        },
        
        imageToResource: function(remoteURL) {
          try {
            var re = /^(https?:\/\/)(.*)\.([a-z0-9]+)$/gi;
            if ('string'!=typeof remoteURL || !re.test(remoteURL)) 
              return '';
            var uPts = (/^(https?:\/\/)(.*)\.([a-z0-9]+)$/gi).exec(remoteURL);
            var extF = (uPts.length==4)? '.'+uPts[3] : '.dat';
            var localURL = 'resource://rambler_plugbar-cache/'+md5(remoteURL)+extF;
            var ioService = Components.classes["@mozilla.org/network/io-service;1"]
                .getService(Components.interfaces.nsIIOService);
            var channel = ioService.newChannel(localURL,null,null);
            var fileHandler = ioService.getProtocolHandler('file')
                              .QueryInterface(Components.interfaces.nsIFileProtocolHandler);
            var fhOut = fileHandler.getFileFromURLSpec(channel.URI.spec);
            $.log(' * imageToResource() remoteURL: '+remoteURL+'; localURL: '+localURL+'path: '+fhOut.path+'; fhOut.exists(): '+fhOut.exists());
            if (fhOut.exists())
              return localURL;
              
            fhOut.create(fhOut.NORMAL_FILE_TYPE, 0777);
            var bArray = $.getImageFromURL(remoteURL);
            if (!bArray.length) {
              $.log('imageToResource(): bArray empty, halt. remoteURL: ' + remoteURL);
              return '';
            }
            $.log('getImageFromURL('+remoteURL+') bArray ('+bArray.length+') '); 
            // $.log(bArray.map(function(aItem) {return aItem.toString(16);}).join(' ').toUpperCase());
            
            var written = $.writeBinaryFile(fhOut, bArray);
            if (written) 
              return localURL;
            return '';
              
          } catch (e) {
            $.log('$.imageToResource() exception: ' + e.message + ' @ '+e.lineNumber);
            return '';
          } 
        },
        
        // doc: scheme xml doc
        localizeImages: function(tDoc) {
          try {
            if (!tDoc) return;

            var imgList = Zepto('widget>informer>image', tDoc);
            $.log('$.localizeImages() imgList.length:' + imgList.length+'; imgList[0].textContent:'+imgList[0].textContent);
            for (var i=0; i<imgList.length; i++) {
              var imgElm = imgList[i];
              var res = $.imageToResource(imgElm.textContent);
              if (!res) 
                continue;
              
              while (imgElm.firstChild) 
                imgElm.removeChild(imgElm.firstChild);
              var cdata = tDoc.createCDATASection(String(res));
              imgElm.appendChild(cdata);
            }
            
            var menuitems = Zepto('widget>informer>menus menuitem[image]', tDoc);
            $.log('menuitems.length:' + menuitems.length); 
            // $.log('; menuitems[0].textContent:'+menuitems[0].textContent+'; menuitems[0].firstChild.nodeName:'+menuitems[0].firstChild.nodeName);
                  
            for (var j=0; j<menuitems.length;j++) {
              var mni = menuitems[j];
              if (!mni.hasAttribute('image'))
                continue;
              var imgR = $.imageToResource(mni.getAttribute('image'));
              if (imgR)
                mni.setAttribute('image', imgR);
            }
            
            return tDoc;
            
          } catch (e) {
            $.log('$.localizeImages() exception: ' + e.message + ' @ '+e.lineNumber);
          } 
        },

        cpf: function() {
          $.log('cpf image: '+ $.imageToResource('http://informers.rambler.ru/static/bar/4/currency.png'));
        }        
        
        
    };    
    
    
    
    
    for (var key in exports)
        $[key] = exports[key];
    
})( this['rambler_plugbar'] );
