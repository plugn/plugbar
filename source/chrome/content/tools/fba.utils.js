(function(scope){

  var utils = {
		createLocationListener: function (changeListener) {
			var progressListener = function (callbacks) {
				var $ = this, empty = function (){};
			
				$.QueryInterface = function (aIID) {
					if (aIID.equals(Components.interfaces.nsIWebProgressListener) ||
						aIID.equals(Components.interfaces.nsISupportsWeakReference) ||
						aIID.equals(Components.interfaces.nsISupports)) {
							return this;
					} else {
						throw Components.results.NS_NOINTERFACE;
					}
				}	
				
				$.onLocationChange = function (aWebProgress, aRequest, aLocation) {
					return changeListener.call(this, callbacks, aWebProgress, aRequest, aLocation);
				}
				$.onProgressChange = empty;
				$.onSecurityChange = empty;
				$.onStateChange = empty;
				$.onStatusChange = empty;
			};
			return progressListener;
		},
		
		md5: function (str) {
			function toHexString(charCode) {
				return ("0" + charCode.toString(16)).slice(-2);
			}
			var converter = Components.classes["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Components.interfaces.nsIScriptableUnicodeConverter);
			converter.charset = "UTF-8";
			var t = {};
			var data = converter.convertToByteArray(str, t);
			var ch = Components.classes["@mozilla.org/security/hash;1"].createInstance(Components.interfaces.nsICryptoHash);
				ch.init(ch.MD5);
				ch.update(data, data.length);
			var hash = ch.finish(false);
			var s = [];
			for (var i = 0, l = hash.length; i < l; ++i) {
				s.push(toHexString(hash.charCodeAt(i)));
			}
			
			return s.join("");
		},
		
		CallbackObject: function (success, error) {
			var $ = this,
          empty = function () {};
			
			$.success = function () {
				try {
					return success.apply($, arguments);
				} catch(e) {
					scope.log("callback exception\n" + e.stack);
					this.error();
				}
			}
			$.error = error || empty;
		},
		
		CallbackWatcher: function (abort) {
			this.abort = abort || function () {};
		},
		
		obj2UrlParams: function (obj, encode) {
			var value = "",
				res = [],
				noencode = (encode === false);
			
			var fixedEncodeURIComponent = function (str) {
				return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A');
			}
			
			for (var key in obj) {
				if (obj.hasOwnProperty(key)) {
					value = noencode ? obj[key] : fixedEncodeURIComponent(obj[key]);
					key = noencode ? key : fixedEncodeURIComponent(key);
					res.push(key + "=" + value);
				}
			}
			return res.join("&");
		},
		
		urlParams2obj: function (urlParams, decode) {
			var res = {},
				nodecode = (decode === false);
			
			if (urlParams.length == 0) {
				return res;
			}

			var fixedDecodeURIComponent = function (str) {
				return decodeURIComponent(str.replace(/\+/g, '%20'));
			}
			
			var params = urlParams.split("&");
			for (var i = 0, l = params.length; i < l; ++i) {
				var [key, value] = params[i].split("=");
				key = nodecode ? key : fixedDecodeURIComponent(key);
				res[key] = nodecode ? value : fixedDecodeURIComponent(value);
			}
			return res;
		},
		
		mix: function F(a, b) {
			for (var i in b) {
				if (!b.hasOwnProperty(i)) {
					continue;
				}
				
				var data = b[i],
					mixin;
				
				if (i in a) {
					switch (typeof data) {
						case "object":
							mixin = F(a[i], data);
							break;
					
						default: 
							mixin = b[i];
					}
					
					a[i] = mixin;
				} else {
					a[i] = data;
				}
			}
			
			return a;
		}
		
	}
  
  var sync = {
  
    /*
    Subclasses of nsIURI, such as nsIURL , impose further structure on the URI.
    To create an nsIURI object, you should use nsIIOService.newURI() , like this:    
    */
    makeURI: function(aURL, aOriginCharset, aBaseURI) {
      try {
        var ioService = Components.classes["@mozilla.org/network/io-service;1"]
                      .getService(Components.interfaces.nsIIOService);
        return ioService.newURI(aURL, aOriginCharset || null, aBaseURI || null);
      } catch (e) {
        scope.log('fba.sync.makeURI('+aURL+') exception: ' + e.message + ' @ ' + e.lineNumber);
      }
    }
  }
  
  var tray = {
    image: 'chrome://rambler_plugbar/skin/settings-logo.png',
    popup: function trayPopup(title, text, image) {
      scope.log(' (!) ' + arguments.callee.name+'()');
      try {
        image = image || tray.image || null;
        Components.classes['@mozilla.org/alerts-service;1'].
                  getService(Components.interfaces.nsIAlertsService).
                  showAlertNotification(image, title, text, false, '', null);
      } catch(e) {
        // prevents runtime error on platforms that don't implement nsIAlertsService
        // If you need to display a comparable alert on a platform that doesn't support nsIAlertsService, you can do this:
        image = image || tray.image || null;
        var win = Components.classes['@mozilla.org/embedcomp/window-watcher;1'].
                            getService(Components.interfaces.nsIWindowWatcher).
                            openWindow(null, 'chrome://global/content/alerts/alert.xul',
                                        '_blank', 'chrome,titlebar=no,popup=yes', null);
        win.arguments = [image, title, text, false, ''];        
      }
    }
  }  
    
  scope.utils = utils;  
  scope.sync = sync;
  scope.tray = tray;
  
  
 })( fba ); 
