(function($){
  $.tray = {
    image: 'chrome://rambler_plugbar/skin/settings-logo.png',
    popup: function trayPopup(title, text, image) {
      $.log(' * ' + arguments.callee.name+'('+title+','+text+')');
      try {
        image = image || $.tray.image || null;
        Components.classes['@mozilla.org/alerts-service;1'].
                  getService(Components.interfaces.nsIAlertsService).
                  showAlertNotification(image, title, text, false, '', null);
      } catch(e) {
        // prevents runtime error on platforms that don't implement nsIAlertsService
        // If you need to display a comparable alert on a platform that doesn't support nsIAlertsService, you can do this:
        image = image || this.image || null;
        var win = Components.classes['@mozilla.org/embedcomp/window-watcher;1'].
                            getService(Components.interfaces.nsIWindowWatcher).
                            openWindow(null, 'chrome://global/content/alerts/alert.xul',
                                        '_blank', 'chrome,titlebar=no,popup=yes', null);
        win.arguments = [image, title, text, false, ''];        
      }
    }
  }
})( app );

(function($){
  var queue = {
    timer: null,
    period: 1500,
    data: [],
    index: 0,
    handler: function(){},
    context: {},
    
    create: function(data, period, handler, context){
      this.data = Array.isArray(data) && data || [];
      this.period = !isNaN(period)? period : this.period;
      this.handler = 'function' == typeof handler? handler : function(){};
      this.context = context || {};
      this.stepNext = this.fnBind(this.setTimer, this);
      return this.copy(this);
    },
    reset: function() {
      this.stop();
      this.period = 1500;
      this.data = [];
      this.index = 0;
      this.handler = function(){};
      this.context = {};
    },
    play: function(data, period, handler, context){
      this.step();
    },
    stop: function(){
      if (this.timer)  
        clearTimeout(this.timer);
      this.timer = null;
    },
    setTimer: function(){
      if (this.timer)
        clearTimeout(this.timer);
      this.timer = setTimeout(this.fnBind(this.step, this), this.period);
    },
    step: function(){
      var params = this.data[this.index++] || null;
      if (!params)
        return this.stop();
        
      $.log(params, 'step()');
      this.handler.apply(this.context, params);
      
      if (this.data[this.index]){
        this.setTimer.apply(this);
      } else {
        this.stop();      
      }  
    },
    fnBind: function(fn, context, params){
      return function(){
        fn.apply(context, params);
      };
    },
    copy: function(o){
      var copy = Object.create( Object.getPrototypeOf(o) );
      var propNames = Object.getOwnPropertyNames(o);
     
      propNames.forEach(function(name){
        var desc = Object.getOwnPropertyDescriptor(o, name);
        Object.defineProperty(copy, name, desc);
      });
     
      return copy;
    }      
  };
  

  $.init = function(){
    $.log(' * queue.js app.init();');
    // $.tray.popup('dooooo','almdoodle');
    var data = [
      ['one','by U2'],
      ['two', 'by Spin Doctors'],
      ['Three', 'by 3 Musketeers'],
      ['thirty','30 seconds to Mars'],
      ['5','Five']
    ];
    var player = $.queue.create(data, 500, $.tray.popup, {});
    player.play();
  };
  
  $.queue = queue;
  
})( app );

