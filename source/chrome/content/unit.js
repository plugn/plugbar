(function($){
    var unit = {
      items: {},
      init: function unit__init(unitID, wgtGuid){
        try {
          $.log(' * unit.init( ' + unitID + ', ' + wgtGuid + ' )'); 
          
          if (unitID in $.unit.items)
            return $.unit.items[unitID];
          
          var item = unit.load(unitID);
          if (item) {
            var api = {
              Zepto: Zepto,
              Queue: Queue,
              Database: Database,
              log: $.log,
              util: $.util,
              prefMgr: $.prefMgr,
              sync: $.sync,
              createWidget: $.scheme.createWidget,
              getWidgetElement: $.scheme.getWidgetElement,
              getWidget: (function(guid) { 
                return $.scheme.widgetsByGuid[guid]; 
              }),
              makeURI: $.sync.makeURI,

              require: unit.require
            }
            $.unit.items[unitID] = item.init(api, wgtGuid);
            
            // $.log($.unit.items[unitID], ' * unit.init('+unitID+', '+wgtGuid+') item ');
          }
          
        } catch (e) {
          return $.log(' (!) ' + arguments.callee.name + '() exception: ' + e.message+' @ ' + e.lineNumber + '.\nStack trace: ' + e.stack);
        }
      },
      quit: function unit__quit(unitID){
        $.log(' * unit.quit( ' + unitID + ' )');
        if (unitID in $.unit.items)
          delete $.unit.items[unitID];
      },
      
      load: function unit__load(unitID, aScope) {
        try {
          var scope = aScope || {};
          var url = 'chrome://' + $.APPNAME + '/content/units/'+unitID+'.js'
          Components.utils.import(url, scope); 
          // $.log(' * Unit.load(url: ' + url + ')'); // +' scope.keys: '+Object.keys(scope)+'; ');
          
          return scope.unit;
          
        } catch (e) {
          $.log(' (!) ' + arguments.callee.name + '() exception: ' + e.message + ' @ ' + e.lineNumber + '.\nStack trace: ' + e.stack);
        }
      },
      
      require: function unit__require(alias, scope) {
        try {
          if (!scope || 'object' !== typeof scope)
            return;
          var part = $.unit.load(alias);
          if ('function' == typeof part)
            part(scope);
          else if (part && 'object' == typeof part)
            scope[alias] = part;  
          else   
            return false;
            
          return true;  
          
        } catch (e) {
          $.log(' (!) ' + arguments.callee.name + '() exception: ' + e.message + ' @ ' + e.lineNumber + '.\nStack trace: ' + e.stack);
        }
      },
      cmd: function unit__cmd(unitID, unitFn) {
        try {
          // $.log(' * ' + arguments.callee.name +'('+unitID+', '+unitFn+')');
          var aUnit = $.unit.items[unitID] || null,
              aFn = ( aUnit && 'function' == typeof aUnit[unitFn] && Array.isArray(aUnit.publicFuncs) && 
                      -1 < aUnit.publicFuncs.indexOf(unitFn) && aUnit[unitFn] ) || null;
          if (aFn) 
            aFn();
            
        } catch (e) {
          $.log(' (!) ' + arguments.callee.name + '() exception: ' + e.message + ' @ ' + e.lineNumber + '.\nStack trace: ' + e.stack);
        }
      }
    }
    
    $.unit = unit;
    
})( this['rambler_plugbar'] );