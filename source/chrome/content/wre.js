/**
 * Widget Runtime Environment
**/
(function($){

    var wre = {
      wndOpen: function(cfg, guid) {
          // $.log('wndOpen() guid:'+guid);
          var xchKey = $.getCfg('xchWProp', null);
          if (('html' !== cfg.type) && (!xchKey || !$[xchKey] || !$[xchKey][guid]))
            return $.log('wndOpen() fail');
           
          if (guid)
            $.util.camp.notify('usage', guid);
          if ('html' !== cfg.type) 
            $[xchKey]['xchCurrent'] = guid;
            
          var o = cfg = (('object' == typeof cfg)? cfg : {});
          var w = (o.w? o.w : 415), h = (o.h?o.h:300), left = (o.left?o.left:200), top = (o.top?o.top:200);
          var b = (cfg.btnId? Zepto('#'+cfg.btnId)[0] : null);
          if (b && b.boxObject) {
              var bo = b.boxObject;
              left=bo.x;
              top=bo.y + bo.height + 26;
          }
          
          var options;
          if('html' == cfg.type && cfg.uri) {
              options = 'titlebar=yes,resizable=no,scrollbars=no,toolbar=no,centerscreen,width='+w+',height='+h;
              $.informerWnd = window.openDialog(cfg.uri, 'informer', options);
          } else if ('dialog'==cfg.type){
              options = 'chrome,titlebar,toolbar,centerscreen,dialog=yes';
              $.informerWnd = window.openDialog('chrome://'+$.APPNAME+'/content/informer.xul', 'informer', options);
          } else {
              options = "chrome,modal,titlebar=no,left="+left+",top="+top+",width="+w+",height="+h;
              $.informerWnd = window.open('chrome://'+$.APPNAME+'/content/informer.xul', "informer", options); 
          }
          if ($.informerWnd && 'function'== typeof cfg.onload)
              $.informerWnd.addEventListener("load",   cfg.onload.bind($.informerWnd), false );
      },
      runAction: function(action,target){
        try {
          if (action in $.wre.actions && 'function'==typeof $.wre.actions[action])
            return $.wre.actions[action](target);
        } catch(e) {
          $.log('wre.runAction() exception: ' + e.message + ' @ ' + e.lineNumber);
        }
      },
      actions: {
        'fb.login': function(target) {
          $.log('action:fb.login');
          $.unit.cmd('facebook', 'login');
        },
        'fb.logout': function(target) {
          $.log('action:fb.logout');
          $.unit.cmd('facebook', 'logout');
        },
        'fb.update': function(target) {
          $.log('action:fb.update');
          $.unit.cmd('facebook', 'update');
        },
        'reload': function(target) {
          if (!target) 
            return;
          var wgts = Zepto(target).parents('[plugtype=widget]');
          if (wgts.length){
            var guid = wgts[0].getAttribute('guid') || '';
            if (guid)
              $.scheme.updateWidget(guid);
          }
        },
        'login': function(target) {
          $.config.prefPane = 'settings';
          window.openDialog('chrome://'+$.APPNAME+'/content/prefs.xul', '&appname;-options', 'chrome,titlebar,toolbar,centerscreen,dialog=yes,resizable=yes');
        },
        'config': function(target) {
          $.config.prefPane = 'widgets';
          window.openDialog('chrome://'+$.APPNAME+'/content/prefs.xul', '&appname;-options', 'chrome,titlebar,toolbar,centerscreen,dialog=yes,resizable=yes');
        },
        'about': function(target) {
          var xchKey = $.getCfg('xchWProp', null);
          if (!xchKey) return;
          if('object' !== typeof $[xchKey])
            $[xchKey] = {};
          
          if (!$[xchKey].about) {
            var aboutDoc = $.sync.getXmlDoc('chrome://'+$.APPNAME+'/content/tpl/about/about.xml');
            var tplSet = $.scheme.getTplSet(aboutDoc),
                abO = $.util.jxt.render(tplSet, $.config.appData, 'XHTML');
            if (abO) {
              $[xchKey].about = {
                docFrag  : abO,
                docCSS   : 'chrome://'+$.APPNAME+'/content/tpl/about/about.css',
                docTitle : 'О программе'
              }
            }
          }   
          $.wre.wndOpen({type:'dialog', w:520, h:320}, 'about');
        }
      }
    };
    
    $.wre = wre;
    
})( this['rambler_plugbar'] );