(function($){ 

    var apiHelper = {
        factory : function ( mode ) {
            var modes = { menus: 'nfoMenu' };
            if (-1 == Object.keys(modes).indexOf(mode)) return null; 
            var klass = this.classes[ modes[mode] ];
            var helper = klass.construct;
            helper.prototype = klass.proto;
            helper.prototype.render = this.modes[ mode ].render;
            
            // $.log(' helper.prototype: ' + helper.prototype + ' ; typeof helper.prototype.render: ' + typeof helper.prototype.render );
            return ('function' == typeof helper? helper : null);
        },
        classes : {
            nfoMenu : {
                construct: function( oXML ) {
                    try {
                        if ('object' != typeof oXML || !oXML.nodeType || !oXML.nodeName) 
                            throw new Error(' (!) '+arguments.callee+'(); invalid constructor argument: '+oXML );
                        this.oXML = oXML;
                        this.elmTop = null;
                        this.elmEdge = null;
                        this.render();
                    } catch (e) {
                        $.log(' (!) '+arguments.callee+'() exception: ' + e.message);
                    }
                },
                proto: {
                    getTopElm: function() {
                        return this.elmTop;
                    },
                    getEdgeElm: function() {
                        return this.elmEdge;
                    }
                }
            }
        },    
        modes: {
            menus: {
                render: function() {
                    try {
                        if ('menus' == this.oXML.nodeName) {
                            var menupopup = this.elmTop = this.elmEdge = document.createElement('menupopup');
                        }                            
                        if ('menuseparator' == this.oXML.nodeName) {
                            var menuseparator = this.elmTop = this.elmEdge = document.createElement('menuseparator');
                        }                            
                        if ('menuitem' == this.oXML.nodeName) {
                            var menuitem = this.elmTop = this.elmEdge = document.createElement('menuitem');
                            if (this.oXML.hasAttribute('text')) 
                                menuitem.setAttribute('label', this.oXML.getAttribute('text'));
                            if (this.oXML.hasAttribute('image')) {
                                menuitem.setAttribute('style', "list-style-image: url('"+this.oXML.getAttribute('image').replace(/^\/\/(content|locale|skin)/mg, 'chrome://'+$.APPNAME+'/$1')+"');" );
                                menuitem.setAttribute('class', "menuitem-iconic" );
                            } else {
                                menuitem.setAttribute('class', "menuitem-iconic bookmark-page");
                            } 
                            if (this.oXML.hasAttribute('action')) {
                                menuitem.setAttribute('action', this.oXML.getAttribute('action'));
                                menuitem.setAttribute('oncommand', "event.stopPropagation();"+$.APPNAME+".wre.runAction(this.getAttribute('action'),this)");
                            } else if (this.oXML.hasAttribute('url')) {
                                menuitem.setAttribute('targetURI', this.oXML.getAttribute('url'));
                                menuitem.setAttribute('oncommand', $.APPNAME+".util.loadURL(this.getAttribute('targetURI'), event);event.stopPropagation();" );
                            }    
                        }
                        if ('menu' == this.oXML.nodeName) {
                            var menu = this.elmTop = document.createElement('menu');
                            if (this.oXML.hasAttribute('text')) 
                                menu.setAttribute('label', this.oXML.getAttribute('text'));
                            
                            if (this.oXML.hasAttribute('image')) {
                                menu.setAttribute('class', 'menu-iconic');
                                menu.setAttribute('style', "list-style-image:url('"+this.oXML.getAttribute('image')+"');");
                            } else {
                                menu.setAttribute('content', 'true');
                                menu.setAttribute('class','menu-iconic bookmark-folder');
                            }
                            
                            var menupopup = this.elmEdge = document.createElement('menupopup');
                            menu.appendChild(menupopup);
                        }
                    } catch (e) {
                        $.log('apiHelper.modes.menus.render() exception: ' + e.message);
                    }        
                }            
            }
        }
    };
    
    $.util.apiHelper = apiHelper;
    
})( this['rambler_plugbar'] );
